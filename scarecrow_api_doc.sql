/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : www.scarecrow.top:3306
 Source Schema         : scarecrow_api_doc

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 17/03/2021 21:25:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sc_api_doc
-- ----------------------------
DROP TABLE IF EXISTS `sc_api_doc`;
CREATE TABLE `sc_api_doc`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '接口名称',
  `url` varchar(2048) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '接口地址',
  `request_method` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '请求方式',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态 1正常 9删除',
  `create_time` int(10) NULL DEFAULT 0 COMMENT '添加时间',
  `update_time` int(10) NULL DEFAULT 0 COMMENT '编辑时间',
  `param` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '参数',
  `result` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '返回结果',
  `category_id` int(11) NULL DEFAULT NULL COMMENT '分类ID',
  `project_id` int(11) NULL DEFAULT NULL COMMENT '项目ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户是否拥有全局项目权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sc_api_doc_category
-- ----------------------------
DROP TABLE IF EXISTS `sc_api_doc_category`;
CREATE TABLE `sc_api_doc_category`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `project_id` int(11) NOT NULL COMMENT '项目名称',
  `state` tinyint(1) NULL DEFAULT NULL COMMENT '分类状态 1:正常 9删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '接口文档分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sc_api_doc_project
-- ----------------------------
DROP TABLE IF EXISTS `sc_api_doc_project`;
CREATE TABLE `sc_api_doc_project`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项目名称',
  `state` tinyint(1) NULL DEFAULT NULL COMMENT '状态 1正常 9删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '接口文档项目表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sc_do_handle_logs
-- ----------------------------
DROP TABLE IF EXISTS `sc_do_handle_logs`;
CREATE TABLE `sc_do_handle_logs`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '操作者ID',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型：d：删除  i：插入  u：修改',
  `method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作具体方法',
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '操作数据',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作备注',
  `route` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求API',
  `ip` bigint(20) NULL DEFAULT NULL COMMENT '请求者IP',
  `created_at` bigint(20) NULL DEFAULT NULL COMMENT '创建时间',
  `src` tinyint(255) NULL DEFAULT 0 COMMENT '日志来源 0：PC端  1：APP',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65608 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sc_system_api
-- ----------------------------
DROP TABLE IF EXISTS `sc_system_api`;
CREATE TABLE `sc_system_api`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) NULL DEFAULT 0 COMMENT '菜单ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '名称',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
  `url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '路径',
  `create_time` bigint(13) NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` bigint(13) NULL DEFAULT 0 COMMENT '修改时间',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '数据状态 1 正常 9 删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单功能表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sc_system_api
-- ----------------------------
INSERT INTO `sc_system_api` VALUES (3, 1, '获取用户信息', '', '/login/get_user_info', 1614310100, 1614310100, 1);
INSERT INTO `sc_system_api` VALUES (4, 1, '更新用户TOKEN', '', '/login/update_token', 1614310162, 1614310162, 1);
INSERT INTO `sc_system_api` VALUES (5, 1, '退出登录', '', '/login/login_out', 1614310193, 1614310193, 1);
INSERT INTO `sc_system_api` VALUES (6, 1, '获取当前环境信息', '', '/api/system/getsysteminfo', 1614310491, 1614310491, 1);
INSERT INTO `sc_system_api` VALUES (7, 1, '获取用户菜单', '', '/login/getusermenulist', 1615532617, 1615532617, 1);
INSERT INTO `sc_system_api` VALUES (8, 1, '获取所有可以查看项目', '', '/api/projectmanage/getallcanviewproject', 1615532767, 1615532767, 1);
INSERT INTO `sc_system_api` VALUES (9, 1, '获取所有可以编辑项目', '', '/api/projectmanage/getallcaneditproject', 1615532797, 1615532797, 1);
INSERT INTO `sc_system_api` VALUES (10, 1, '创建分类', '', '/api/projectmanage/createcategory', 1615532812, 1615532812, 1);
INSERT INTO `sc_system_api` VALUES (11, 1, '编辑分类', '', '/api/projectmanage/updatecategory', 1615532824, 1615532824, 1);
INSERT INTO `sc_system_api` VALUES (12, 1, '删除分类', '', '/api/projectmanage/deletecategory', 1615532833, 1615532833, 1);
INSERT INTO `sc_system_api` VALUES (13, 1, '获取所有分类', '', '/api/projectmanage/getallcategory', 1615532843, 1615532843, 1);
INSERT INTO `sc_system_api` VALUES (14, 1, '创建接口', '', '/api/apidoc/createapi', 1615532958, 1615532958, 1);
INSERT INTO `sc_system_api` VALUES (15, 1, '编辑接口', '', '/api/apidoc/editapi', 1615532968, 1615532968, 1);
INSERT INTO `sc_system_api` VALUES (16, 1, '获取接口文档列表分页', '', '/api/apidoc/getapidoclistpage', 1615532980, 1615532980, 1);
INSERT INTO `sc_system_api` VALUES (17, 1, '获取接口文档列表', '', '/api/apidoc/getapidoclist', 1615532988, 1615532988, 1);
INSERT INTO `sc_system_api` VALUES (18, 1, '获取接口文档详情', '', '/api/apidoc/getapidocinfo', 1615532997, 1615532997, 1);
INSERT INTO `sc_system_api` VALUES (19, 1, '获取分类详情', '', '/api/projectmanage/getcategoryinfo', 1615532767, 1615532767, 1);
INSERT INTO `sc_system_api` VALUES (20, 1, '删除接口', '', '/api/apidoc/deleteapi', 1615532968, 1615532968, 1);
INSERT INTO `sc_system_api` VALUES (21, 1, '导出文档', '', '/api/apidoc/exportapidoc', 1615532968, 1615532968, 1);

-- ----------------------------
-- Table structure for sc_system_authority
-- ----------------------------
DROP TABLE IF EXISTS `sc_system_authority`;
CREATE TABLE `sc_system_authority`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
  `sort` int(20) NULL DEFAULT 0 COMMENT '排序',
  `create_time` bigint(13) NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` bigint(13) NULL DEFAULT 0 COMMENT '修改时间',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '数据状态 1 正常 9 删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sc_system_authority
-- ----------------------------
INSERT INTO `sc_system_authority` VALUES (1, '所有员工基础权限', '', 0, 1611407838, 1611408033, 1);

-- ----------------------------
-- Table structure for sc_system_authority_menu
-- ----------------------------
DROP TABLE IF EXISTS `sc_system_authority_menu`;
CREATE TABLE `sc_system_authority_menu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `authority_id` int(10) NULL DEFAULT 0,
  `menu_id` int(10) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sc_system_authority_menu
-- ----------------------------
INSERT INTO `sc_system_authority_menu` VALUES (1, 1, 1);

-- ----------------------------
-- Table structure for sc_system_authority_menu_function
-- ----------------------------
DROP TABLE IF EXISTS `sc_system_authority_menu_function`;
CREATE TABLE `sc_system_authority_menu_function`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `authority_id` int(10) NULL DEFAULT 0,
  `menu_id` int(10) NULL DEFAULT 0,
  `function_id` int(10) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限与菜单功能关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sc_system_authority_menu_function
-- ----------------------------
INSERT INTO `sc_system_authority_menu_function` VALUES (1, 1, 2, 1);

-- ----------------------------
-- Table structure for sc_system_function
-- ----------------------------
DROP TABLE IF EXISTS `sc_system_function`;
CREATE TABLE `sc_system_function`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) NULL DEFAULT 0 COMMENT '菜单ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `key_val` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'keyVal',
  `create_time` bigint(13) NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` bigint(13) NULL DEFAULT 0 COMMENT '修改时间',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '数据状态 1 正常 9 删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单功能表' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sc_system_menu
-- ----------------------------
DROP TABLE IF EXISTS `sc_system_menu`;
CREATE TABLE `sc_system_menu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_type` int(10) NULL DEFAULT 1 COMMENT '菜单所属平台 1:PC端',
  `parent_id` int(10) NULL DEFAULT 0 COMMENT '父级菜单Id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
  `key_val` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'keyVal',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路径',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '图标',
  `include_str` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '自定义信息字符串',
  `sort` int(255) NULL DEFAULT 0 COMMENT '排序',
  `create_time` bigint(13) NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` bigint(13) NULL DEFAULT 0 COMMENT '修改时间',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '数据状态 1 正常 9 删除',
  `is_can_distribution` tinyint(1) NULL DEFAULT 1 COMMENT '是否允许分配 0：否 1：是',
  `depth` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '深度',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sc_system_menu
-- ----------------------------
INSERT INTO `sc_system_menu` VALUES (1, 1, 0, '所有员工基础菜单', '所有员工基础菜单', '', '', '', '', 1, 1611400220, 1611401487, 1, 1, '|1|');

-- ----------------------------
-- Table structure for sc_system_role
-- ----------------------------
DROP TABLE IF EXISTS `sc_system_role`;
CREATE TABLE `sc_system_role`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
  `sort` int(20) NULL DEFAULT 0 COMMENT '排序',
  `create_time` bigint(13) NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` bigint(13) NULL DEFAULT 0 COMMENT '修改时间',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '数据状态 1 正常 9 删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sc_system_role
-- ----------------------------
INSERT INTO `sc_system_role` VALUES (1, '所有员工基础角色', '', 0, 1611408216, 1611408333, 1);

-- ----------------------------
-- Table structure for sc_system_role_authority
-- ----------------------------
DROP TABLE IF EXISTS `sc_system_role_authority`;
CREATE TABLE `sc_system_role_authority`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NULL DEFAULT 0,
  `authority_id` int(10) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sc_system_role_authority
-- ----------------------------
INSERT INTO `sc_system_role_authority` VALUES (1, 1, 1);

-- ----------------------------
-- Table structure for sc_system_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sc_system_role_user`;
CREATE TABLE `sc_system_role_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL DEFAULT 0,
  `user_id` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sc_token_cache
-- ----------------------------
DROP TABLE IF EXISTS `sc_token_cache`;
CREATE TABLE `sc_token_cache`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `token` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户令牌',
  `expire_time` bigint(11) NULL DEFAULT 0 COMMENT '过期时间',
  `update_time` bigint(11) NULL DEFAULT 0 COMMENT '更新时间',
  `call_num` int(11) NULL DEFAULT 0 COMMENT '令牌剩余调用次数',
  `is_invalid` tinyint(1) NULL DEFAULT 0 COMMENT '是否失效 0：否 1：是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 193 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户令牌缓存表' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sc_user
-- ----------------------------
DROP TABLE IF EXISTS `sc_user`;
CREATE TABLE `sc_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nike_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户登录账号',
  `password` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户密码',
  `is_supper_admin` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否是超级管理员 0：否 1：是',
  `state` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态 1:正常 2:停用 9:删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户列表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sc_user
-- ----------------------------
INSERT INTO `sc_user` VALUES (1, 'Scarecrow', '18888888888', '57a51c12792a0aa880faa8889da409ac', 1, 1);

-- ----------------------------
-- Table structure for sc_user_all_project_auth
-- ----------------------------
DROP TABLE IF EXISTS `sc_user_all_project_auth`;
CREATE TABLE `sc_user_all_project_auth`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `write` tinyint(1) NOT NULL DEFAULT 0 COMMENT '可写 0:否  1:是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户是否拥有全局项目权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sc_user_project_auth
-- ----------------------------
DROP TABLE IF EXISTS `sc_user_project_auth`;
CREATE TABLE `sc_user_project_auth`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `project_id` int(11) NOT NULL COMMENT '项目ID',
  `write` tinyint(1) NOT NULL DEFAULT 0 COMMENT '可写 0：否 1：是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户绑定某个项目权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sc_user_read_api_doc
-- ----------------------------
DROP TABLE IF EXISTS `sc_user_read_api_doc`;
CREATE TABLE `sc_user_read_api_doc`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `api_doc_id` int(11) NOT NULL COMMENT '接口ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户可以查看的接口列表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
