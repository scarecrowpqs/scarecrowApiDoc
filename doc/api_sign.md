# API签名文档
## 返回说明
    当验签或鉴权失败时会返回此接口值
    code:
        -1:代表用户验签失败
        -2:代表用户token失效
        XXX:与HTTP CODE一致
        
## APP鉴权说明:
    1、必须包含sign(签名)，randstr（随机字符串）字段，timestamp(时间戳不带毫秒),platform(平台字段 且值只能为  （移动端：h5 ）、（安卓端：android ）、(苹果端：ios)、(PC端))没有这些字段自动判定为失败
    2、鉴权规则：
        1、所有传输的参数必须为字符串，无论KEY还是VALUE都必须是字符串，鉴于这个原因凡是有参数需要传多维数组的都请使用json进行序列化后进行传参
        2、第一步：将所有值为非空值的参数值进行MD5计算 计算方式: md5(key_value)
        3、第二步：将所有计算后的值进行字符串排序，顺序为升序
        4、第三步：将所有的参数组合成 key=md5(key_value)&的格式，前后两边&移除
        5、第四步：在第三步的结果上前后都加上  bs:(cijz0Yyx@Tk6"C*Jo>!P2v8npZm]~&G%^<1A  然后进行MD5计算得到sign值
        6、第四步，将sign值加入到请求地址中，带着访问即可。

## 步骤示例:
    例如请求参数为 http://host/api?name=稻草人&age=20&sex=1&randstr=888888&token=16815254531798dc21ee979d1d9c6675&love=
    
    下面演示如何求得sign：
    1、现将所有的参数中的空值移除，所以需要进行签名的是
    name=稻草人  
    age=20  
    sex=1 
    randstr=888888 
    token=16815254531798dc21ee979d1d9c6675
    2、对所有参数的值进行md5计算:
      'name' => string '44a68a15e30e82ecf26d8c84c54f75f4' (length=32)
      'age' => string '5e405aff94d877a92403372539f64495' (length=32)
      'sex' => string '76c4b3831cd907a90c92259cf5723e33' (length=32)
      'randstr' => string 'dc24549e731fdf294e3bfb17a659eada' (length=32)
      'token' => string 'a2109d5b82ffe492eb3e9d0f81e34b1e' (length=32)
    3、对值进行字符串升序排序：
      'name' => string '44a68a15e30e82ecf26d8c84c54f75f4' (length=32)
      'age' => string '5e405aff94d877a92403372539f64495' (length=32)
      'sex' => string '76c4b3831cd907a90c92259cf5723e33' (length=32)
      'token' => string 'a2109d5b82ffe492eb3e9d0f81e34b1e' (length=32)
      'randstr' => string 'dc24549e731fdf294e3bfb17a659eada' (length=32)
    
    
    4、将排好序的参数拼成字符串
    name=44a68a15e30e82ecf26d8c84c54f75f4&age=5e405aff94d877a92403372539f64495&sex=76c4b3831cd907a90c92259cf5723e33&token=a2109d5b82ffe492eb3e9d0f81e34b1e&randstr=dc24549e731fdf294e3bfb17a659eada
    
    5、在4的字符串前后加上bs:(cijz0Yyx@Tk6"C*Jo>!P2v8npZm]~&G%^<1A字符串并进行MD5计算
    Md5(bs:(cijz0Yyx@Tk6"C*Jo>!P2v8npZm]~&G%^<1Aname=44a68a15e30e82ecf26d8c84c54f75f4&age=5e405aff94d877a92403372539f64495&sex=76c4b3831cd907a90c92259cf5723e33&token=a2109d5b82ffe492eb3e9d0f81e34b1e&randstr=dc24549e731fdf294e3bfb17a659eadabs:(cijz0Yyx@Tk6"C*Jo>!P2v8npZm]~&G%^<1A
    ) 得到值 f000d3059346589f3ed536cb3daf3228
    
    
    最后将sign=f000d3059346589f3ed536cb3daf3228 加入到请求的路径中得到
    http://host/api?name=稻草人&age=20&sex=1&randstr=888888&token=16815254531798dc21ee979d1d9c6675&love=&sign=f000d3059346589f3ed536cb3daf3228
    这样就完成了签名