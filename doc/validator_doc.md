# 表单验证器使用文档
## 自定义扩展规则
    若要使用自定义扩展需要将  bootstrap/app.php中的$app->register(\App\Providers\ScarecrowValidatorProvider::class); 注释去掉
    在App/Rules目录中创建一个 xxxRule.php规则文件，注意命名规则为：规则名字+Rule.php
    规则文件中 类名必须与文件名一致 且必须继承BaseRule.php文件，否则该规则会始终返回验证失败
    然后完善规则文件必要函数即可
    
## 使用方式
    1、可以根据官方文档的使用方式使用
    2、可以自定义一个表单验证类，支持统一管理字段、验证规则
    以SystemAuth类为例
        SystemAuth.class
        class SystemAuth extends Validate
        {
            // 验证规则
            protected $rule = [
        		'id'	=>	'required|integer',
                'name' => 'required',
        		'roleId'	=>	'required|integer',
        		'roleName'	=>	'required',
        		'userId'	=>	'required|integer'
            ];
        
            protected $message = [
        		'id.required' => '权限ID不能为空',
        		'id.integer' => '权限ID必须为数字',
                'name.required' => '权限名称不能为空',
        		'roleId.required' => '角色ID不能为空',
        		'roleId.integer' => '角色ID必须为数字',
        		'roleName.required' => '角色名称不能为空',
        		'userId.required' => '用户ID不能为空',
        		'userId.integer' => '用户ID必须为数字',
            ];
        
            protected $scene = [
            	//创建权限
            	'createAuth'	=>	['name'],
        		//编辑权限
        		'editAuth'		=>	['id', 'name'],
        		//删除权限
        		'deleteAuth'	=>	['id'],
        		//创建角色
        		'createRole'	=>	['roleName'],
        		//编辑角色
        		'editRole'		=>	['roleId', 'roleName'],
        		//删除角色
        		'deleteRole'	=>	['roleId'],
        		//用户绑定角色
        		'userBindRole'	=>	['userId']
        	];
        }
    
    调用的方式:
        $allData = [];//验证的字段及值
        $validateObj = new SystemAuth();
        if (!$validateObj->scene('createAuth')->check($allData)) {
            return ApiReturn('NO', $validateObj->getError());
        }