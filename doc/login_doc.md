#用户、API管理文档

## 全局API请求、响应基础格式
    RouteType:GET/POST
    Header:SC-API-TOKEN=TOKEN  需要鉴权的API接口HEADER需要携带此KEY
    Request:
        sign:String 签名(需签名的接口必带)
        randstr:String 随机字符串(需签名的接口必带)
        platform:String 平台类型 pc/h5/ios/android(需签名的接口必带)
        ... 其他API请求需要携带的参数
    Return:
        status:String YES/NO   状态
        code:Int    响应码 除特殊自定义码以外，其他均兼容Http响应码
        info:String 响应描述
        data:Mixd   任意类型值
    
    以下的接口描述中自动对应上面格式,返回值如果有Data则只会展示Data数据结构这里定义ReturnData为返回数据
    参数除了上面标注需要的字段以外的才会在文档请求字段中展示

#### 用户登录
    Route:/login
    Request:
        userName:String 用户登录名
        userPwd:String  用户密码
    ReturnData:
    {
       //用户ID
       "uid": 1,
       //用户TOKEN
       "token": "eyJpdiI6IkVRd3F0TTYrVmtLR3krbTByMzlSMlE9PSIsInZhbHVlIjoiM0s2YUtjQkNVZi85WWV1bzl5TFNUVTZCalZGaGpvMHRGMys5b0kzUDJIMEcxYVExaHprZXJEa1B0cEo3M1pabiIsIm1hYyI6IjY0NTI5MWQyOTVkYTJmMTUzOTgxMDE5OWU0NWUzMTY5Mzk0ZDNiZTkzOTk1ZDk2MTJjOTU5NzQyY2JmZmQzYzIifQ==",
       //TOKEN失效时间
       "expire_time": 1611456491,
       //TOKEN最后更新时间
       "update_time": 1611452891,
       //TOKEN剩余调用次数
       "call_num": 2000,
       //TOKEN是否失效 1:是  0:否
       "is_invalid": 0
    }

#### 更新用户TOKEN
    Route:/login/update_token
    Request:
    ReturnData:
    {
        //用户TOKEN
        "token": "eyJpdiI6ImlVVHBkSmgxVjhOek1RWk9lUVpqQnc9PSIsInZhbHVlIjoiWjhHUFBET0VReXpkekdXTlk2dHRzZ3F1ZERaTFR3RDZlYlNpSnJPbTJmMEhjYW9iWVRTbVRuQTJGZHJWZURucCIsIm1hYyI6ImJhMjU4OWUzNmEyMGMyNWQwZWI3NDJjODVkYzIzODg3NmYwZjcyMmU1MTlhOTU0Njg0YmZmYTc4YTU5MzI0MGMifQ==",
        //TOKEN失效时间
        "expire_time": 1611456668,
        //TOKEN最后更新时间
        "update_time": 1611453068,
        //TOKEN剩余调用次数
        "call_num": 2000,
        //TOKEN是否失效 1:是  0:否
        "is_invalid": 0
    }
    
#### 退出登录
    Route:/login/login_out
    Request:
    ReturnData:
    

#### 获取当前用户信息
    Route:/login/get_user_info
    Request:
    ReturnData:
    {
        //用户ID
        "id": 1,
        //用户昵称
        "nike_name": "Scarecrow",
        //用户登录账号
        "username": "18888888888",
        //用户当前Token信息 字段与用户登录返回一致
        "tokenInfo": {
            "uid": 1,
            "token": "41cdad5fb3ef7bb1bc2419e334ef52c5",
            "expire_time": 1611456863,
            "update_time": 1611453275,
            "call_num": 1999,
            "is_invalid": 0
        },
        //用户权限列表
        "authList": []
    }