## 接口文档
    所有鉴权的文档都需要在header带上SC-API-TOKEN参数，参数值为token令牌
    所有验签的接口都需要签名，具体签名算法见签名文档
    所有接口返回都是具体格式，如下:
        {
            status:"YES",//状态值 YES/NO
            info:"结果处理描述",
            data:{},//具体的结果
            code:200//具体的响应CODE，除了框架特殊CODE以外其他均以HTTP CODE一致
        }
    下面所有接口描述中的returnData都表示对象中的data字段


    
### 用户登录
    api:/login
    type:POST
    isSign:true //是否需要签名
    isToken:false //是否需要token
    data:
        userName:string:用户名
        userPwd:string:用户密码
    return:{
               "uid": 1,//用户ID
               "token": "eyJpdiI6InNCQksyM1NRdE1RTlVISThEYk8veVE9PSIsInZhbHVlIjoiNDlvL0IyZ3JOeG4xY3dZTDFmVldSaW1YTHh4Z296OWhiVjQrcmliZ2ZWYTFIUFpDdkNBYjdLSWFVU05pQ2J2aSIsIm1hYyI6IjM5MDdiN2ViOGRjNjI3NDc5YzNlZDYwYjI2ZGYyMThkMTg0OTNmMTNjMTVjM2FkNDNiMTdiZmJmNzYzM2NiYjUifQ==",//token
               "expire_time": 1614323020,//过期时间
               "update_time": 1614319420,//最后更新时间
               "call_num": 2000,//token剩余调用此时
               "is_invalid": 0//是否已过期 1：是  0:否
           }
        
        
### 更新用户TOKEN
    api:/login/update_token
    type:POST
    isSign:true
    isToken:true
    data:{},
    return:{
       "token": "eyJpdiI6InNCQksyM1NRdE1RTlVISThEYk8veVE9PSIsInZhbHVlIjoiNDlvL0IyZ3JOeG4xY3dZTDFmVldSaW1YTHh4Z296OWhiVjQrcmliZ2ZWYTFIUFpDdkNBYjdLSWFVU05pQ2J2aSIsIm1hYyI6IjM5MDdiN2ViOGRjNjI3NDc5YzNlZDYwYjI2ZGYyMThkMTg0OTNmMTNjMTVjM2FkNDNiMTdiZmJmNzYzM2NiYjUifQ==",//token
       "expire_time": 1614323020,//过期时间
       "update_time": 1614319420,//最后更新时间
       "call_num": 2000,//token剩余调用此时
       "is_invalid": 0//是否已过期 1：是  0:否
    }
    
    
### 退出登录
    api:/login/login_out
    type:POST
    isSign:true
    isToken:true
    data:{},
    returnData:{}
### 获取当前用户信息
    api:/login/get_user_info
    type:POST
    isSign:true
    isToken:true
    data:{},
    returnData:{
       "id": 1,//用户ID
       "nike_name": "Scarecrow",//用户昵称
       "username": "18888888888",//用户登录账号
       "is_supper_admin": 1,//是否是超级管理员
       "tokenInfo": {//token相关数据
           "uid": 1,//用户ID
           "token": "9f9774d3aae7760cb89215ba56227bb9",//用户真实TOKEN
           "expire_time": 1614323020,//当前token过期时间
           "update_time": 1614320663,//当前token最后修改时间
           "call_num": 1995,//当前token剩余令牌调用次数
           "is_invalid": 0//是否已过期令牌
       }
   }
### 首页-获取当前环境信息
    api:/api/system/getsysteminfo
    type:POST
    isSign:true
    isToken:true
    data:{},
    returnData:{
       "userName": "18888888888",//用户名称
       "nickName": "Scarecrow",//用户昵称
       "userNum": 3,//当前系统用户数
       "projectNum": 0,//项目数
       "apiNum": 0//接口数
   }
   
### 系统设置-创建用户
    api:/api/user/createuser
    type:POST
    isSign:true
    isToken:true
    data:{
        userName:string:用户名,
        nickName:string:用户昵称,
        password:string:用户密码
    },
    returnData:{}
    
    
### 系统设置-更新用户密码
    api:/api/user/updateuserpwd
    type:POST
    isSign:true
    isToken:true
    data:{
        userId:int:用户ID,
        password:string:用户密码
    },
    returnData:{}

### 系统设置-更新用户自己密码
    api:/api/user/updatemypwd
    type:POST
    isSign:true
    isToken:true
    data:{
        oldPwd:string:用户旧密码,
        newPwd:string:用户新密码
    },
    returnData:{}
### 系统设置-修改用户状态
    api:/api/user/stoporstartuser
    type:POST
    isSign:true
    isToken:true
    data:{
        userId:int:用户ID
    },
    returnData:{}
### 系统设置-删除用户
    api:/api/user/deleteuser
    type:POST
    isSign:true
    isToken:true
    data:{
        userId:int:用户ID
    },
    returnData:{}
    
    
    
### 系统设置-获取用户列表
    api:/api/user/getuserlist
    type:POST
    isSign:true
    isToken:true
    data:{
        page:int:页码数
        limit:int:当前页数据条数,
        searchContent:string:搜索字段
    },
    returnData:{
       "total": 1,//总数
       "limit": 10,//当前页数据条数
       "page": 1,//页码数
       "list": [//数据列表
            {
                "id": 1,//用户ID
                "nike_name": "Scarecrow",//用户昵称
                "username": "18888888888",//用户账号
                "state": 1//用户状态
            }
       ]
   }
   
### 系统设置-获取所有用户列表
    api:/api/user/getalluserlist
    type:POST
    isSign:true
    isToken:true
    data:{},
    returnData:{
       {
           "id": 1,//用户ID
           "nike_name": "Scarecrow",//用户昵称
           "username": "18888888888",//用户账号
       }
   }
   
### 系统设置-获取所有项目
    api:/api/projectauth/getallproject
    type:POST
    isSign:true
    isToken:true
    data:{},
    returnData:[
       {
           "id": 1,//项目ID
           "name": "互动平台"//项目名称
       }
   ]
      
### 系统设置-获取用户当前项目权限
    api:/api/projectauth/getuserprojectauth
    type:POST
    isSign:true
    isToken:true
    data:{
        userId:int:用户ID,
        projectId:int:项目ID
    },
    returnData:{
           "isAllProjectManage": 0,
           "isAllProjectManageWrite": 0,
           "isProjectManage": 0,
           "isProjectManageWrite": 0,
           "projectAllCanSeeApiList": []
       }
             
             
### 系统设置-获取当前项目所有接口列表
    api:/api/projectauth/getprojectallapilist
    type:POST
    isSign:true
    isToken:true
    data:{
        projectId:int:项目ID
    },
    returnData:{
           "id": '接口ID',
           "url": '接口URL',
           "title": '接口名称',
           "name": "分类名称",
       }
### 系统设置-设置用户当前项目权限
    api:/api/projectauth/setuserprojectauth
    type:POST
    isSign:true
    isToken:true
    data:{
        userId:int:用户ID,
        projectId:int:项目ID,
        isAllProjectManage:int:是否有所有项目查看权限,
        isAllProjectManageWrite:int:是否有所有项目编辑权限,
        isProjectManage:int:是否有当前项目查看权限,
        isProjectManageWrite:int:是有有当前项目编辑权限,
        projectAllCanSeeApiList:string:当前项目可查看用户ID列表，多个id之间用逗号隔开
    },
    returnData:[]
### 系统设置-创建项目
    api:/api/projectmanage/createproject
    type:POST
    isSign:true
    isToken:true
    data:{
        projectName:string:项目名称
    },
    returnData:[]
### 系统设置-编辑项目
    api:/api/projectmanage/editproject
    type:POST
    isSign:true
    isToken:true
    data:{
        projectId:int:项目ID
        projectName:string:项目名称
    },
    returnData:[]
    
### 系统设置-改变项目状态
    api:/api/projectmanage/changeprojectstate
    type:POST
    isSign:true
    isToken:true
    data:{
        projectId:int:项目ID
    },
    returnData:[]
        
### 系统设置-获取所有可以编辑项目
    api:/api/projectmanage/getallcaneditproject
    type:POST
    isSign:true
    isToken:true
    data:{
        projectName:String:项目名称(模糊搜索)
    },
    returnData:[
       {
           "id": 1,//项目ID
           "name": "互动平台",//项目名称
           "state": 1//项目状态
       }
   ]
   
### 系统设置-获取项目信息
    api:/api/projectmanage/getprojectinfo
    type:POST
    isSign:true
    isToken:true
    data:{
        projectId:int:项目ID
    },
    returnData:{
          "id": 1,//项目ID
          "name": "互动平台",//项目名称
          "state": 1//项目状态
      }
   
        
### 系统设置-获取所有可以查看项目
    api:/api/projectmanage/getallcanviewproject
    type:POST
    isSign:true
    isToken:true
    data:{},
    returnData:[
       {
           "id": 1,//项目ID
           "name": "互动平台",//项目名称
           "state": 1//项目状态
       }
   ]
       
### 系统设置-删除项目
    api:/api/projectmanage/deleteproject
    type:POST
    isSign:true
    isToken:true
    data:{
        projectId:int:项目ID
    },
    returnData:[]
    
### 系统设置-创建分类
    api:/api/projectmanage/createcategory
    type:POST
    isSign:true
    isToken:true
    data:{
        categoryName:string:分类名称,
        projectId:int:项目ID,
    },
    returnData:[]
        
### 系统设置-编辑分类
    api:/api/projectmanage/updatecategory
    type:POST
    isSign:true
    isToken:true
    data:{
        categoryName:string:分类名称,
        categoryId:int:分类ID,
    },
    returnData:[]
            
### 系统设置-删除分类
    api:/api/projectmanage/deletecategory
    type:POST
    isSign:true
    isToken:true
    data:{
        categoryId:int:分类ID,
    },
    returnData:[]
      
### 系统设置-获取所有分类
    api:/api/projectmanage/getallcategory
    type:POST
    isSign:true
    isToken:true
    data:{
        projectId:int:项目ID,
        categoryName:String:分类名称(模糊搜索)
    },
    returnData:[
       {
           "id": 1,//分类ID
           "name": "测试分类",//分类名称
           "project_id": 1,//项目ID
           "state": 1//分类状态
       }
   ]
     
### 系统设置-获取分类信息
    api:/api/projectmanage/getcategoryinfo
    type:POST
    isSign:true
    isToken:true
    data:{
        projectId:int:项目ID,
        categoryId:int:分类ID,
    },
    returnData:{
           "id": 1,//分类ID
           "name": "测试分类",//分类名称
           "project_id": 1,//项目ID
           "state": 1//分类状态
      }
          
### 文档设置-创建接口
    api:/api/apidoc/createapi
    type:POST
    isSign:true
    isToken:true
    data:{
        title:string:接口名称,
        url:string:接口地址,
        requestMethod:string:接口请求方法,
        result:string:接口返回结果,
        categoryId:int:分类ID,
        projectId:int:项目ID,
        paramList:string:参数数组 JSON串 [{paramName: "userPwd", paramType: "string", paramMust: "1", paramDesc: "用户密码"}]
    },
    returnData:[]
             
### 文档设置-编辑接口
    api:/api/apidoc/editapi
    type:POST
    isSign:true
    isToken:true
    data:{
        title:string:接口名称,
        url:string:接口地址,
        requestMethod:string:接口请求方法,
        result:string:接口返回结果,
        categoryId:int:分类ID,
        projectId:int:项目ID,
        apiId:int:项目ID,
        param_title:array:参数名,
        param_type:array:参数类型,
        param_must:array:参数是否必须,
        param_desc:array:参数描述,
    },
    returnData:[]
         
### 文档设置-获取接口文档列表分页
    api:/api/apidoc/getapidoclistpage
    type:POST
    isSign:true
    isToken:true
    data:{
        projectId:int:项目ID,
        categoryId:int:分类ID,
        limit:int:当前页数据条数,
        page:int:当前页码数,
    },
    returnData:{
           "total": 1,//数据总条数
           "limit": 10,//当前页数据条数
           "page": 1,//当前页码数
           "list": [//数据列表
               {
                   "id": 1,//文档ID
                   "title": "1",//接口名称
                   "url": "1",//接口地址
                   "request_method": "GET",//接口请求方法
                   "status": 1,//接口状态
                   "create_time": 0,//创建时间戳
                   "update_time": 0,//最后更新时间戳
                   "param": "{}",//参数列表
                   "result": "1",//返回接口
                   "category_id": 1,//分类ID
                   "project_id": 1,//项目ID
               }
           ]
       }
                
### 文档设置-获取接口文档列表
    api:/api/apidoc/getapidoclist
    type:POST
    isSign:true
    isToken:true
    data:{
        projectId:int:项目ID,
        categoryId:int:分类ID,
        apiName:string:接口名称
    },
    returnData:[//数据列表
          {
              "id": 1,//文档ID
              "title": "1",//接口名称
              "url": "1",//接口地址
              "request_method": "GET",//接口请求方法
              "status": 1,//接口状态
              "create_time": 0,//创建时间戳
              "update_time": 0,//最后更新时间戳
              "param": "{}",//参数列表
              "result": "1",//返回接口
              "category_id": 1,//分类ID
              "project_id": 1,//项目ID
          }
      ]
                       
### 文档设置-获取接口文档详情
    api:/api/apidoc/getapidocinfo
    type:POST
    isSign:true
    isToken:true
    data:{
        apiId:int:文档ID,
    },
    returnData:{
          "id": 1,//文档ID
          "title": "1",//接口名称
          "url": "1",//接口地址
          "request_method": "GET",//接口请求方法
          "status": 1,//接口状态
          "create_time": 0,//创建时间戳
          "update_time": 0,//最后更新时间戳
          "param": "{}",//参数列表
          "result": "1",//返回接口
          "category_id": 1,//分类ID
          "project_id": 1,//项目ID
      }

### 文档设置-删除接口
    api:/api/apidoc/deleteapi
    type:POST
    isSign:true
    isToken:true
    data:{
        apiId:int:文档ID,
    },
    returnData:{}