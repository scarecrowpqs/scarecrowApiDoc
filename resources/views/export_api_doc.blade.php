<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>导出{{$projectInfo['projectName']}}接口</title>
    <style>
        table{
            border-collapse:collapse;
            width: 100%;
        }
        tr td{border:1px solid #D5D3D3;}
    </style>
</head>
<body>
<p style="text-align: center;">{{$projectInfo['projectName']}} 接口文档</p>
<div style="width: 880px;margin: auto;">
    <?php foreach ($projectInfo['categoryList'] as $category) { ?>
        <p style="font-weight: bold;color: #0000FF;font-size: 20px;">{{$category['categoryName']}}:</p>
        <?php foreach ($category['apiList'] as $api) { ?>
        <table>
            <tr>
                <td colspan="2" style="text-align: center;">接口名称:</td>
                <td colspan="6" style="padding-left: 10px;">{{$api['title']}}</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">接口方法:</td>
                <td colspan="6" style="padding-left: 10px;">{{$api['request_method']}}</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">接口地址:</td>
                <td colspan="6" style="padding-left: 10px;">{{$api['url']}}</td>
            </tr>
            <tr>
                <td colspan="8" style="text-align: center;">接口参数</td>
            </tr>
            <tr>
                <td colspan="2"  style="text-align: center;width: 25%;">参数名</td>
                <td colspan="2"  style="text-align: center;width: 25%;">类型</td>
                <td colspan="2"  style="text-align: center;width: 25%;">必传</td>
                <td colspan="2"  style="text-align: center;width: 25%;">描述</td>
            </tr>

            <?php foreach ($api['paramArr'] as $paramObj) { ?>
            <tr>
                <td colspan="2"  style="text-align: center;width: 25%;">{{$paramObj['title']}}</td>
                <td colspan="2"  style="text-align: center;width: 25%;">{{$paramObj['type']}}</td>
                <td colspan="2"  style="text-align: center;width: 25%;">{{$paramObj['must'] ? "是" : "否"}}</td>
                <td colspan="2"  style="text-align: center;width: 25%;">{{$paramObj['desc']}}</td>
            </tr>
            <?php } ?>

            <tr>
                <td colspan="8" style="text-align: center;"> &nbsp;</td>
            </tr>
            <tr>
                <td colspan="8" style="text-align: center;">返回值</td>
            </tr>
            <tr>
                <td colspan="8"><div style="min-height: 100px;padding: 15px 10px;">{!! $api['result'] !!}</div></td>
            </tr>
        </table>
        <br>
    <?php }} ?>
</div>
</body>
</html>