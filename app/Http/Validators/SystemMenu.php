<?php

namespace App\Http\Validators;

/**
 * 菜单模块验证器
 * Class SystemMenu
 * @package app\back\validate
 */
class SystemMenu extends Validate
{
    // 验证规则
    protected $rule = [
        'parentId' => 'required|integer',
        'name' => 'required',
		'id'	=>	'required|integer',
		'status'=>	'required|in:1,9',
		'sort'	=>	'required|integer',
		'functionId'	=>	'required|integer',
		'functionName'	=>	'required',
		'keyVal'		=>	'required',
		'apiRouteId'	=>	'required|integer',
		'apiRouteName'	=>	'required',
		'apiRouteUrl'	=>	'required'
    ];

    //错误信息
    protected $message = [
        'parentId.required' => '父级ID不能为空',
        'parentId.integer' => '父级ID必须为整数字',
        'name.required' => '菜单名称不能为空',
		'id.require' => '菜单ID不能为空',
		'id.integer' => '菜单ID必须为数字',
		'status.required' => '菜单状态值不能为空',
		'status.in' => '菜单状态值不在范围内',
		'sort.required' => '排序不能为空',
		'sort.integer' => '排序必须为数字',
		'functionId.required' => '功能ID不能为空',
		'functionId.integer' => '功能ID必须为数字',
		'functionName.required'	=>	'功能名称不能为空',
		'keyVal.required'		=>	'关键字不能为空',
		'apiRouteId.required' => 'API的ID不能为空',
		'apiRouteId.integer' => 'API的ID必须为数字',
		'apiRouteName.required'	=>	'API名称不能为空',
		'apiRouteUrl.required'	=>	'API路由不能为空'
    ];

    //场景
    protected $scene = [
    	//创建菜单
    	'create'	=>	['parentId', 'name', 'sort'],
		//编辑菜单
		'edit'		=>	['id','name','parentId','sort'],
		//删除菜单
		'delete'	=>	['id'],
		//创建功能
		'createFunction'	=>	['id','functionName','keyVal'],
		//编辑功能
		'editFunction'		=>	['functionId', 'functionName', 'keyVal'],
		//删除功能
		'deleteFunction'	=>	['functionId'],
		//创建API路由
		'createApiRoute'	=>	['id','apiRouteName', 'apiRouteUrl'],
		//编辑API路由
		'editApiRoute'	=>	['apiRouteId', 'apiRouteName', 'apiRouteUrl'],
		//删除API路由
		'deleteApiRoute'	=>	['apiRouteId'],
	];
}