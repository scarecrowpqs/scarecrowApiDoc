<?php

namespace App\Http\Validators;

/**
 * 项目、分类验证
 * Class ProjectManageValidators
 * @package App\Http\Validators
 */
class ProjectManageValidators extends Validate{
	// 验证规则
	protected $rule = [
		'projectName' => 'required',
		'categoryName' => 'required',
		'projectId'   => 'required|integer',
		'categoryId'   => 'required|integer',
	];

	//错误信息
	protected $message = [
		'projectName.required' => '项目名不能为空',
		'projectName.alpha_dash' => '项目名可能具有字母、数字、破折号(-)以及下划线(_)',
		'categoryName.required' => '分类名不能为空',
		'categoryName.alpha_dash' => '分类名可能具有字母、数字、破折号(-)以及下划线(_)',
		'projectId.required' => '项目ID不能为空',
		'projectId.integer' => '项目ID只能为数字',
		'categoryId.required' => '分类ID不能为空',
		'categoryId.integer' => '分类ID只能为数字',
	];

	//场景
	protected $scene = [
		//创建项目
		'createProject'	=>	['projectName'],
		//编辑项目
		'updateProject'	=>	['projectName','projectId'],
        //改变项目状态
		'changeProject'	=>	['projectId'],
        //删除项目
		'deleteProject'	=>	['projectId'],
		//创建分类
		'createCategory'	=>	['categoryName','projectId'],
		//编辑分类
		'updateCategory'	=>	['categoryName','categoryId'],
        //删除分类
		'deleteCategory'	=>	['categoryId'],
	];
}