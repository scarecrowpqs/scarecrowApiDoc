<?php

namespace App\Http\Validators;

/**
 * Api验证
 * Class ProjectManageValidators
 * @package App\Http\Validators
 */
class ApiValidators extends Validate{
	// 验证规则
	protected $rule = [
		'title' => 'required',
		'url' => 'required',
		'apiId'   => 'required|integer',
		'projectId'	=>	'required|integer',
		'categoryId'	=>	'required|integer'
	];

	//错误信息
	protected $message = [
		'title.required' => '接口名称不能为空',
		'title.alpha_dash' => '接口名称可能具有字母、数字、破折号(-)以及下划线(_)',
		'url.required' => '路由地址不能为空',
		'apiId.required' => '分类ID不能为空',
		'projectId.required' => '项目ID不能为空',
		'categoryId.required' => '分类ID不能为空',
		'apiId.integer' => '分类ID只能为数字',
		'projectId.integer' => '项目ID只能为数字',
		'categoryId.integer' => '分类ID只能为数字',
	];

	//场景
	protected $scene = [
		//创建接口
		'create'	=>	['title', 'url', 'projectId', 'categoryId'],
		//编辑接口
		'edit'	=>	['title','url','apiId', 'projectId', 'categoryId'],
		//获取接口列表
		'getlist'	=>	['projectId', 'categoryId']
	];
}