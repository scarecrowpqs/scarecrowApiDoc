<?php

namespace App\Http\Validators;

/**
 * 权限模块验证器
 * Class SystemMenu
 * @package app\back\validate
 */
class SystemAuth extends Validate
{
    // 验证规则
    protected $rule = [
		'id'	=>	'required|integer',
        'name' => 'required',
		'roleId'	=>	'required|integer',
		'roleName'	=>	'required',
		'userId'	=>	'required|integer'
    ];

    protected $message = [
		'id.required' => '权限ID不能为空',
		'id.integer' => '权限ID必须为数字',
        'name.required' => '权限名称不能为空',
		'roleId.required' => '角色ID不能为空',
		'roleId.integer' => '角色ID必须为数字',
		'roleName.required' => '角色名称不能为空',
		'userId.required' => '用户ID不能为空',
		'userId.integer' => '用户ID必须为数字',
    ];

    protected $scene = [
    	//创建权限
    	'createAuth'	=>	['name'],
		//编辑权限
		'editAuth'		=>	['id', 'name'],
		//删除权限
		'deleteAuth'	=>	['id'],
		//创建角色
		'createRole'	=>	['roleName'],
		//编辑角色
		'editRole'		=>	['roleId', 'roleName'],
		//删除角色
		'deleteRole'	=>	['roleId'],
		//用户绑定角色
		'userBindRole'	=>	['userId']
	];
}