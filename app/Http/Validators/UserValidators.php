<?php

namespace App\Http\Validators;

class UserValidators extends Validate{
	// 验证规则
	protected $rule = [
		'userName' => 'required|alpha_dash',
		'password' => 'required|min:6|max:16',
		'userId'   => 'required|integer'
	];

	//错误信息
	protected $message = [
		'userName.required' => '用户名不能为空',
		'userName.alpha_dash' => '用户名可能具有字母、数字、破折号(-)以及下划线(_)',
		'password.required' => '密码不能为空',
		'password.min' => '密码不能少于6位',
		'password.max' => '密码不能多于16位',
		'userId.required' => '用户ID不能为空',
		'userId.integer' => '用户ID只能为数字',
	];

	//场景
	protected $scene = [
		//创建用户
		'create'	=>	['userName', 'password'],
		//编辑用户
		'update'	=>	['password','userId'],
		//停用用户
		'stop'	=>	['userId'],
		//删除用户
		'delete'	=>	['userId'],
	];

}