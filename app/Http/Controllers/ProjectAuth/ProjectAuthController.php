<?php
namespace App\Http\Controllers\ProjectAuth;

use App\Http\Controllers\Controller;
use App\Models\ProjectAuth\ProjectAuthModel;
use Illuminate\Http\Request;

/**
 * 项目权限管理类
 * Class ProjectAuthController
 * @package App\Http\Controllers\ProjectAuth
 */
class ProjectAuthController extends Controller{
	/**
	 * 获取所有项目
	 * @return false|string
	 */
	public function getAllProject() {
		$m = new ProjectAuthModel();
		$relData = $m->getAllProject();
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 获取用户当前项目权限
	 * @param Request $request
	 * @return false|string
	 */
	public function getUserProjectAuth(Request $request) {
		$userId = (int)$request->input('userId', 0);
		$projectId = (int)$request->input('projectId', 0);

		$m = new ProjectAuthModel();
		$relData = $m->getUserProjectAuth($userId, $projectId);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

    /**
     * 获取当前项目所有接口列表
     * @param Request $request
     * @return false|string
     */
	public function getProjectAllApiList(Request $request) {
        $projectId = (int)$request->input('projectId', 0);

        $m = new ProjectAuthModel();
        $relData = $m->getProjectAllApiList($projectId);
        return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
    }

	/**
	 * 设置用户项目权限
	 * @param Request $request
	 * @return false|string
	 */
    public function setUserProjectAuth(Request $request) {
		$projectId = (int)$request->input('projectId', 0);
		$userId = (int)$request->input('userId', 0);
		$isAllProjectManage = (int)$request->input('isAllProjectManage', 0);
		$isAllProjectManageWrite = (int)$request->input('isAllProjectManageWrite', 0);
		$isProjectManage = (int)$request->input('isProjectManage', 0);
		$isProjectManageWrite = (int)$request->input('isProjectManageWrite', 0);
		$projectAllCanSeeApiListStr = $request->input('projectAllCanSeeApiList', '');

		$projectAllCanSeeApiList = [];
		if ($projectAllCanSeeApiListStr) {
			$projectAllCanSeeApiList = array_filter(explode(',', $projectAllCanSeeApiListStr));
		}

		$m = new ProjectAuthModel();
		$relData = $m->setUserProjectAuth($userId, $projectId, $isAllProjectManage, $isAllProjectManageWrite, $isProjectManage, $isProjectManageWrite, $projectAllCanSeeApiList);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}
}
