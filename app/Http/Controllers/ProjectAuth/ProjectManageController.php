<?php
namespace App\Http\Controllers\ProjectAuth;

use App\Facades\ScarecrowAuth;
use App\Http\Controllers\Controller;
use App\Http\Validators\ProjectManageValidators;
use App\Models\ProjectAuth\ProjectManageModel;
use Illuminate\Http\Request;

class ProjectManageController extends Controller{

    /**
     * 创建项目
     * @param Request $request
     * @return false|string
     */
	public function createProject(Request $request) {
        $allData = $request->input();
        $validateObj = new ProjectManageValidators();
        if (!$validateObj->scene('createProject')->check($allData)) {
            return ApiReturn('NO', implode(',', $validateObj->getError()));
        }

        $m = new ProjectManageModel();
        $relData = $m->createProject($allData);
        return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

    /**
     * 编辑项目
     * @param Request $request
     * @return false|string
     */
    public function editProject(Request $request) {
        $allData = $request->input();
        $validateObj = new ProjectManageValidators();
        if (!$validateObj->scene('updateProject')->check($allData)) {
            return ApiReturn('NO', implode(',', $validateObj->getError()));
        }

        $m = new ProjectManageModel();
        $relData = $m->editProject($allData);
        return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
    }

    /**
     * 改变项目状态
     * @param Request $request
     * @return false|string
     */
    public function changeProjectState(Request $request) {
        $allData = $request->input();
        $validateObj = new ProjectManageValidators();
        if (!$validateObj->scene('changeProject')->check($allData)) {
            return ApiReturn('NO', implode(',', $validateObj->getError()));
        }

        $m = new ProjectManageModel();
        $relData = $m->changeProjectState($allData);
        return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
    }

	/**
	 * 获取所有可以编辑项目
	 * @return false|string
	 */
    public function getAllCanEditProject(Request $request) {
    	$projectName = addslashes($request->input('projectName', ''));
		$m = new ProjectManageModel();
		$relData = $m->getAllCanEditProject($projectName);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

    /**
     * 获取所有可以查看项目
     * @return false|string
     */
    public function getAllCanViewProject() {
        $m = new ProjectManageModel();
        $relData = $m->getAllCanViewProject();
        return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
    }

    /**
     * 删除项目
     * @param Request $request
     * @return false|string
     */
    public function deleteProject(Request $request) {
        $allData = $request->input();
        $validateObj = new ProjectManageValidators();
        if (!$validateObj->scene('deleteProject')->check($allData)) {
            return ApiReturn('NO', implode(',', $validateObj->getError()));
        }

        $m = new ProjectManageModel();
        $relData = $m->deleteProject($allData);
        return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
    }

    /**
     * 创建分类
     * @param Request $request
     * @return false|string
     */
    public function createCategory(Request $request) {
        $allData = $request->input();
        $validateObj = new ProjectManageValidators();
        if (!$validateObj->scene('createCategory')->check($allData)) {
            return ApiReturn('NO', implode(',', $validateObj->getError()));
        }

        $m = new ProjectManageModel();
        $relData = $m->createCategory($allData);
        return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
    }

    /**
     * 编辑分类
     * @param Request $request
     * @return false|string
     */
    public function updateCategory(Request $request) {
        $allData = $request->input();
        $validateObj = new ProjectManageValidators();
        if (!$validateObj->scene('updateCategory')->check($allData)) {
            return ApiReturn('NO', implode(',', $validateObj->getError()));
        }

        $m = new ProjectManageModel();
        $relData = $m->updateCategory($allData);
        return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
    }

    /**
     * 删除分类
     * @param Request $request
     * @return false|string
     */
    public function deleteCategory(Request $request) {
        $allData = $request->input();
        $validateObj = new ProjectManageValidators();
        if (!$validateObj->scene('deleteCategory')->check($allData)) {
            return ApiReturn('NO', implode(',', $validateObj->getError()));
        }

        $m = new ProjectManageModel();
        $relData = $m->deleteCategory($allData);
        return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
    }

	/**
	 * 获取所有分类
	 * @return false|string
	 */
	public function getAllCategory(Request $request) {
	    $projectId = (int)$request->input('projectId', 0);
	    $categoryName = $request->input('categoryName', '');
		$m = new ProjectManageModel();
		$relData = $m->getAllCategory($projectId, $categoryName);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 获取项目信息
	 * @param Request $request
	 * @return false|string
	 */
	public function getProjectInfo(Request $request) {
		$projectId = (int)$request->input('projectId', 0);

		$m = new ProjectManageModel();
		$relData = $m->getProjectInfo($projectId);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 获取分类信息
	 * @param Request $request
	 * @return false|string
	 */
	public function getCategoryInfo(Request $request) {
		$projectId = $request->input('projectId', 0);
		$categoryId = (int)$request->input('categoryId', 0);

		$m = new ProjectManageModel();
		$relData = $m->getCategoryInfo($projectId, $categoryId);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

    /**
     * 下载项目
     * @param Request $request
     * @return false|string|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
	public function exportProject(Request $request) {
        $v = $request->input('version', 'postmanv2_1');
        $projectId = $request->input('projectId', 0);

        $m = new ProjectManageModel();
        switch ($v){
            case 'postmanv2_1':
                $fileInfo = $m->exportProjectPostmanV2_1($projectId);
                break;
            default:
                return ApiReturn("NO", "不存在导出类型");
        }
        if ($fileInfo['code']!=0) {
            return ApiReturn("NO", $fileInfo['msg']);
        }
        $fileInfo = $fileInfo['data'];
        return response()->download($fileInfo['path'],$fileInfo['name']);
    }
}