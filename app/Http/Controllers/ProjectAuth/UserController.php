<?php
namespace App\Http\Controllers\ProjectAuth;

use App\Http\Controllers\Controller;
use App\Http\Validators\UserValidators;
use App\Models\ProjectAuth\UserModel;
use Illuminate\Http\Request;

/**
 * 用户管理控制类
 * Class UserController
 * @package App\Http\Controllers\User
 */
class UserController extends Controller{
	/**
	 * 创建用户
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function createUser(Request $request) {
		$allData = $request->input();
		$validateObj = new UserValidators();
		if (!$validateObj->scene('create')->check($allData)) {
			return ApiReturn('NO', implode(',', $validateObj->getError()));
		}

		$m = new UserModel();
		$relData = $m->createUser($allData);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 更新用户密码
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function updateUserPwd(Request $request) {
		$allData = $request->input();
		$validateObj = new UserValidators();
		if (!$validateObj->scene('update')->check($allData)) {
			return ApiReturn('NO', implode(',', $validateObj->getError()));
		}

		$m = new UserModel();
		$relData = $m->updateUserPwd($allData);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 修改自己的密码
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function updateMyPwd(Request $request) {
		$oldPwd = $request->input('oldPwd', '');
		$newPwd = $request->input('newPwd', '');

		if (strlen($newPwd) < 6) {
			return ApiReturn('NO', '用户新密码不能小于6位');
		}

		$m = new UserModel();
		$relData = $m->updateMyPwd($oldPwd, $newPwd);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 修改用户状态
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function stopOrStartUser(Request $request) {
		$allData = $request->input();
		$validateObj = new UserValidators();
		if (!$validateObj->scene('stop')->check($allData)) {
			return ApiReturn('NO', implode(',', $validateObj->getError()));
		}

		$m = new UserModel();
		$relData = $m->stopOrStartUser($allData);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 删除用户
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function deleteUser(Request $request) {
		$allData = $request->input();
		$validateObj = new UserValidators();
		if (!$validateObj->scene('delete')->check($allData)) {
			return ApiReturn('NO', implode(',', $validateObj->getError()));
		}

		$m = new UserModel();
		$relData = $m->deleteUser($allData);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 获取用户列表
	 * @param Request $request
	 * @return false|string
	 */
	public function getUserList(Request $request) {
		$page = (int)$request->input('page', 1);
		$limit = (int)$request->input('limit', 10);
		$searchContent = addslashes($request->input('searchContent', ''));

		$m = new UserModel();
		$relData = $m->getUserList($searchContent, $page, $limit);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 获取所有用户列表
	 * @return false|string
	 */
	public function getAllUserList() {
		$m = new UserModel();
		$relData = $m->getAllUserList();
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}
}