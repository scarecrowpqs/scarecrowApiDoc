<?php
namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Home\SystemModel;

class SystemController extends Controller{
	/**
	 * 获取当前环境信息
	 * @return false|string
	 */
	public function getSystemInfo() {
		$m = new SystemModel();
		$relData = $m->getSystemInfo();
		return ApiReturn($relData['code'] == 0 ? 'YES' : 'NO', $relData['msg'], $relData['data']);
	}
}