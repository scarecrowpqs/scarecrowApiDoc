<?php

namespace App\Http\Controllers;

use App\Common\Config\AllUri;
use App\Http\Validators\SystemMenu;
use App\Rules\AuthorRule;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    public function index(Request $request) {
		return ApiReturn('YES', "");
    }

    public function test() {
		return ApiReturn('YES', "Test");
	}
}
