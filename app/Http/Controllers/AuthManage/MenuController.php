<?php
namespace App\Http\Controllers\AuthManage;

use App\Http\Validators\SystemMenu;
use App\Models\AuthManage\MenuModel;
use Illuminate\Http\Request;

/**
 * 菜单管理模块
 * Class MenuController
 * @package app\back\controller
 */
class MenuController{
	/**
	 * 创建菜单
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function addMenu(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemMenu();
		if (!$validateObj->scene('create')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new MenuModel();
		$relData = $m->addMenu($allData);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 编辑菜单
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function editMenu(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemMenu();
		if (!$validateObj->scene('edit')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new MenuModel();
		$relData = $m->editMenu($allData);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 删除菜单
	 * @input Request $request
	 * @return mixed
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 * @throws \think\exception\PDOException
	 */
	public function deleteMenu(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemMenu();
		if (!$validateObj->scene('delete')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new MenuModel();
		$relData = $m->deleteMenu($allData);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 获取菜单详情
	 * @input Request $request
	 * @return mixed
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 * @throws \think\exception\PDOException
	 */
	public function getMenu(Request $request) {
		$id = (int)$request->input('id');
		$m = new MenuModel();
		$relData = $m->getMenu($id);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 获取菜单列表
	 * @input Request $request
	 * @return mixed
	 */
	public function getMenuList(Request $request) {
		$name = addslashes($request->input('name', ''));
		$page = (int)$request->input('page', 1);
		$limit = (int)$request->input('limit', 10);
		$m = new MenuModel();
		$relData = $m->getMenuList($name, $page, $limit);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 获取菜单树状列表
	 * @return mixed
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function getMenuTree() {
		$m = new MenuModel();
		$treeData = $m->getMenuListTree();
		return ApiReturn("YES", "获取成功", $treeData);
	}

	/**
	 * 创建功能
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function addFunction(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemMenu();
		if (!$validateObj->scene('createFunction')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new MenuModel();
		$relData = $m->addFunction($allData);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 编辑功能
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function editFunction(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemMenu();
		if (!$validateObj->scene('editFunction')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new MenuModel();
		$relData = $m->editFunction($allData);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 删除功能
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function deleteFunction(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemMenu();
		if (!$validateObj->scene('deleteFunction')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new MenuModel();
		$relData = $m->deleteFunction($allData['functionId']);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 获取功能详情
	 * @input Request $request
	 * @return mixed
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 * @throws \think\exception\PDOException
	 */
	public function getFunction(Request $request) {
		$functionId = (int)$request->input('functionId', 0);

		$m = new MenuModel();
		$relData = $m->getFunction($functionId);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 获取功能列表
	 * @param Request $request
	 * @return false|string
	 */
	public function getFunctionList(Request $request) {
		$menuId = (int)$request->input('id', 0);
		$page = (int)$request->input('page', 1);
		$limit = (int)$request->input('limit', 10);
		$m = new MenuModel();
		$relData = $m->getFunctionList($menuId, $page, $limit);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 添加权限路由
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function addApiRoute(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemMenu();
		if (!$validateObj->scene('createApiRoute')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new MenuModel();
		$relData = $m->addApiRoute($allData);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 编辑权限路由
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function editApiRoute(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemMenu();
		if (!$validateObj->scene('editApiRoute')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new MenuModel();
		$relData = $m->editApiRoute($allData);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 删除权限路由
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function deleteApiRoute(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemMenu();
		if (!$validateObj->scene('deleteApiRoute')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new MenuModel();
		$relData = $m->deleteApiRoute($allData['apiRouteId']);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 获取权限路由详情
	 * @param Request $request
	 * @return false|string
	 */
	public function getApiRoute(Request $request) {
		$authId = (int)$request->input('authId', 0);
		$m = new MenuModel();
		$relData = $m->getApiRoute($authId);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 获取权限路由列表
	 * @param Request $request
	 * @return false|string
	 */
	public function getApiRouteList(Request $request) {
		$menuId = (int)$request->input('id', 0);
		$page = (int)$request->input('page', 1);
		$limit = (int)$request->input('limit', 10);
		$m = new MenuModel();
		$relData = $m->getApiRouteList($menuId, $page, $limit);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}
}