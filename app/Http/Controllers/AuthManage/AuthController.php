<?php
namespace App\Http\Controllers\AuthManage;

use App\Facades\ScarecrowAuth;
use App\Http\Validators\SystemAuth;
use App\Models\AuthManage\AuthModel;
use Illuminate\Http\Request;

/**
 * 权限管理模块
 * Class AuthManageController
 * @package app\back\controller
 */
class AuthController{
	/**
	 * 创建权限
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function createAuth(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemAuth();
		if (!$validateObj->scene('createAuth')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new AuthModel();
		$relData = $m->createAuth($allData);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 编辑权限
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function editAuth(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemAuth();
		if (!$validateObj->scene('editAuth')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new AuthModel();
		$relData = $m->editAuth($allData);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 删除权限
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function deleteAuth(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemAuth();
		if (!$validateObj->scene('deleteAuth')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new AuthModel();
		$relData = $m->deleteAuth($allData['id']);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 获取权限详情
	 * @param Request $request
	 * @return false|string
	 */
	public function getAuth(Request $request) {
		$id = (int)$request->input('id', 0);

		$m = new AuthModel();
		$relData = $m->getAuth($id);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 获取权限列表
	 * @param Request $request
	 * @return false|string
	 */
	public function getAuthList(Request $request) {
		$m = new AuthModel();
		$relData = $m->getAuthList();
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 获取权限列表分页
	 * @param Request $request
	 * @return false|string
	 */
	public function getAuthListPage(Request $request) {
		$page = (int)$request->input('page', 1);
		$limit = (int)$request->input('limit', 10);
		$m = new AuthModel();
		$relData = $m->getAuthListPage($page, $limit);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 创建角色
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function createRole(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemAuth();
		if (!$validateObj->scene('createRole')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new AuthModel();
		$relData = $m->createRole($allData);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 编辑角色
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function editRole(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemAuth();
		if (!$validateObj->scene('editRole')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new AuthModel();
		$relData = $m->editRole($allData);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 获取角色详情
	 * @param Request $request
	 * @return false|string
	 */
	public function getRole(Request $request) {
		$id = (int)$request->input('id', 0);

		$m = new AuthModel();
		$relData = $m->getRole($id);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 删除角色
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function deleteRole(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemAuth();
		if (!$validateObj->scene('deleteRole')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$m = new AuthModel();
		$relData = $m->deleteRole($allData['roleId']);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 获取角色列表分页
	 * @param Request $request
	 * @return false|string
	 */
	public function getRoleListPage(Request $request) {
		$page = (int)$request->input('page', 1);
		$limit = (int)$request->input('limit', 10);
		$m = new AuthModel();
		$relData = $m->getRoleListPage($page, $limit);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 获取角色列表
	 * @param Request $request
	 * @return false|string
	 */
	public function getRoleList(Request $request) {
		$m = new AuthModel();
		$relData = $m->getRoleList();
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 获取角色拥有的权限列表
	 * @param Request $request
	 * @return false|string
	 */
	public function getRoleAuthList(Request $request) {
		$roleId = $request->input('role_id', 0);
		$m = new AuthModel();
		$relData = $m->getRoleAuthList($roleId);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 用户绑定角色
	 * @param Request $request
	 * @return false|string
	 */
	public function roleBindUser(Request $request) {
		$allData = $request->input();
		$validateObj = new SystemAuth();
		if (!$validateObj->scene('userBindRole')->check($allData)) {
			return ApiReturn('NO', $validateObj->getError());
		}

		$roleList = $request->input('role_id', '');
		$allRoleId = [];
		if ($roleList) {
			$allRoleId = explode(',', $roleList);
			$allRoleIdObj = array_filter($allRoleId);
			$allRoleId = [];
			foreach ($allRoleIdObj as $item) {
				$allRoleId[] = (int)$item;
			}
		}
		$m = new AuthModel();
		$relData = $m->roleBindUser($allRoleId, $allData);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 获取当前用户拥有的所有菜单
	 * @return false|string
	 */
	public function getUserAllMenu() {
		$m = new AuthModel();
		$relData = $m->getUserAllMenu();
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 获取某个权限对应的所有功能
	 * @param Request $request
	 * @return false|string
	 */
	public function getAuthToFunctionList(Request $request) {
		$authId = (int)$request->input('auth_id', 0);
		$m = new AuthModel();

		$relData = $m->getAuthToFunctionList($authId);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 权限绑定功能
	 * @param Request $request
	 * @return false|string
	 */
	public function authBindFunction(Request $request) {
		$authId = (int)$request->input('auth_id', 0);
		$functionList = $request->input('func_id', '');
		$allFunctionId = [];
		if ($functionList) {
			$allFunctionId = explode(',', $functionList);
			$allFunctionIdObj = array_filter($allFunctionId);
			$allFunctionId = [];
			foreach ($allFunctionIdObj as $item) {
				$allFunctionId[] = (int)$item;
			}
		}

		$userId = ScarecrowAuth::id();
		$m = new AuthModel();
		$relData = $m->authBindFunction($authId, $allFunctionId, $userId);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}

	/**
	 * 角色绑定权限
	 * @input Request $request
	 * @return mixed
	 */
	public function roleBindAuth(Request $request) {
		$roleId = (int)$request->input('role_id', 0);
		$authList = $request->input('auth_id', '');
		$allAuthId = [];
		if ($authList) {
			$allAuthId = explode(',', $authList);
			$allFunctionIdObj = array_filter($allAuthId);
			$allAuthId = [];
			foreach ($allFunctionIdObj as $item) {
				$allAuthId[] = (int)$item;
			}
		}

		$m = new AuthModel();
		$relData = $m->roleBindAuth($roleId, $allAuthId);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg']);
	}
}