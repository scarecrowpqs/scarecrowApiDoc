<?php
namespace App\Http\Controllers\AuthManage;

use App\Http\Controllers\Controller;
use App\Models\AuthManage\LoginModel;
use Illuminate\Http\Request;

/**
 * 登录控制类
 * Class LoginController
 * @package App\Http\Controllers
 */
class LoginController extends Controller {
	/**
	 * 用户登录
	 * @param Request $request
	 * @return false|string
	 * @throws \Exception
	 */
	public function login(Request $request) {
		$userName = $request->input('userName', '');
		$userPwd = $request->input('userPwd', '');
		$relData = LoginModel::LoginUser($userName, $userPwd);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 更新当前TOKEN
	 * @return false|string
	 * @throws \Exception
	 */
	public function updateToken() {
		$relData = LoginModel::updateToken(APP_USER_TOKEN);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 退出登录
	 * @return false|string
	 */
	public function loginOut() {
		$relData = LoginModel::LoginOut(APP_USER_TOKEN);
		return ApiReturn($relData['code'] == 0 ? "YES" : "NO", $relData['msg'], $relData['data']);
	}

	/**
	 * 获取用户信息
	 * @return false|string
	 */
	public function getUserInfo() {
		$userInfoObj = LoginModel::getUserInfo(APP_USER_TOKEN);
		if ($userInfoObj['code'] != 0) {
			return ApiReturn("NO", $userInfoObj['msg']);
		}

		$userObj = $userInfoObj['data'];
		unset($userObj['authList']);
		return ApiReturn("YES", "获取成功", $userObj);
	}

	/**
	 * 获取用户菜单
	 * @return false|string
	 */
	public function getUserMenuList() {
		$menuList = LoginModel::getUserMenuList(APP_USER_TOKEN);
		return ApiReturn('YES', '获取成功', $menuList['data']);
	}
}
