<?php
namespace App\Http\Controllers\ApiDoc;

use App\Http\Controllers\Controller;
use App\Http\Validators\ApiValidators;
use App\Models\ApiDoc\ApiDocModel;
use Illuminate\Http\Request;

class ApiDocController extends Controller{

	/**
	 * 创建接口
	 * @param Request $request
	 * @return false|string
	 */
    public function createApi(Request $request) {
		$allData = $request->input();
		$validateObj = new ApiValidators();
		if (!$validateObj->scene('create')->check($allData)) {
			return ApiReturn('NO', implode(',', $validateObj->getError()));
		}

		$m = new ApiDocModel();
		$relData = $m->createApi($allData);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
    }

	/**
	 * 编辑接口
	 * @param Request $request
	 * @return false|string
	 */
    public function editApi(Request $request) {
		$allData = $request->input();
		$validateObj = new ApiValidators();
		if (!$validateObj->scene('edit')->check($allData)) {
			return ApiReturn('NO', implode(',', $validateObj->getError()));
		}

		$m = new ApiDocModel();
		$relData = $m->editApi($allData);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 删除接口
	 * @param Request $request
	 * @return false|string
	 */
    public function deleteApi(Request $request) {
		$apiId = (int)$request->input('apiId', 0);
		$m = new ApiDocModel();
		$relData = $m->deleteApi($apiId);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 获取接口文档列表分页
	 * @param Request $request
	 * @return false|string
	 */
	public function getApiDocListPage(Request $request) {
		$allData = $request->input();
		$validateObj = new ApiValidators();
		if (!$validateObj->scene('getlist')->check($allData)) {
			return ApiReturn('NO', implode(',', $validateObj->getError()));
		}

		if(!isset($allData['page']) || $allData['page'] < 1) {
			$allData['page'] = 1;
		}

		if(!isset($allData['limit'])) {
			$allData['limit'] = 10;
		}

		$m = new ApiDocModel();
		$relData = $m->getApiDocListPage($allData);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 获取接口文档列表
	 * @param Request $request
	 * @return false|string
	 */
	public function getApiDocList(Request $request) {
		$allData = $request->input();
		$validateObj = new ApiValidators();
		if (!$validateObj->scene('getlist')->check($allData)) {
			return ApiReturn('NO', implode(',', $validateObj->getError()));
		}

		$m = new ApiDocModel();
		$relData = $m->getApiDocList($allData);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 获取接口文档详情
	 * @param Request $request
	 * @return false|string
	 */
	public function getApiDocInfo(Request $request) {
		$apiId = (int)$request->input('apiId');
		$m = new ApiDocModel();
		$relData = $m->getApiDocInfo($apiId);
		return ApiReturn($relData['code'], $relData['msg'], $relData['data']);
	}

	/**
	 * 导出接口文档
	 * @param Request $request
	 * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
	 * @throws \Exception
	 */
	public function exportApiDoc(Request $request) {
		$projectId = $request->input('projectId', 0);
		$m = new ApiDocModel();
		$exportInfo = $m->exportProjectApiList($projectId);
		if ($exportInfo['code'] != 0) {
			return ApiReturn('NO', $exportInfo['msg']);
		}
		$exportData = $exportInfo['data'];
		$content = view('export_api_doc', ['projectInfo'=>$exportData])->toHtml();
		$relData = [
			'fileName'	=>	$exportData['projectName'] . '.doc',
			'fileData'	=>	$content
		];
		return ApiReturn('YES', '下载成功', $relData);
	}
}