<?php
namespace App\Http\Middleware;

use Closure;

/**
 * 允许跨域
 * Class checkAppKuaYuMiddleware
 * @package app\http\middleware
 */
class OpenKuaYuMiddleware{

	public function handle($request, Closure $next)
    {
        $this->allKuaYu($request, env('IS_ALLOW_ORIGIN', false));
        return $next($request);
    }

    protected function allKuaYu($request, $isOpen = false) {
        if ($isOpen) {
            header('Access-Control-Allow-Origin:*'); // *代表允许任何网址请求
            header('Access-Control-Allow-Methods:POST,GET,OPTIONS,DELETE,PUT'); // 允许请求的类型
            header('Access-Control-Allow-Credentials: true'); // 设置是否允许发送 cookies
            header('Access-Control-Allow-Headers: Content-Type,Content-Length,Accept-Encoding,X-Requested-with, Origin, SC-API-TOKEN');
			if ($request->method() == "OPTIONS") {
				die(200);
			}
        }
    }
}