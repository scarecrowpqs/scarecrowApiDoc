<?php

namespace App\Http\Middleware;

use Closure;

/**
 * 全局异常处理
 * Class HandleExceptionMiddleware
 * @package App\Http\Middleware
 */
class HandleExceptionMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        $response = $next($request);
        try {
            if (env('IS_FORMAT_EXCEPTION', true) && $response->exception) {
                $response->setContent(ApiReturn('NO', $response->exception->getMessage(), explode('#',$response->exception->getTraceAsString()), $response->status()));
            }
        } catch (\Throwable $e) {

        }

        return $response;
	}
}
