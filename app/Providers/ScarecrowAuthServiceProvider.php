<?php

namespace App\Providers;

use App\Common\ScarecrowProvider\ScarecrowAuthProvider;
use Illuminate\Support\ServiceProvider;

class ScarecrowAuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
		$this->app->singleton('ScarecrowAuth', function($app) {
			return new ScarecrowAuthProvider();
		});
    }
}
