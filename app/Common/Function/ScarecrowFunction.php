<?php
if (!function_exists('GetVersion')) {
    /**
     * 获取当前
     * @return string
     */
    function GetVersion() {
        return app()->version();
    }
}

if (!function_exists('DieDump')) {
	/**
	 * 断点输出
	 * @param $var
	 * @param bool $isJson
	 */
	function DieDump($var, $isJson = false) {
		if ($isJson) {
			die(json_encode($var, 256));
		} else {
			var_dump($var);
			die();
		}
	}
}

if (!function_exists('CreateToken')) {
	/**
	 * 创建用户TOKEN
	 * @param int $userId
	 * @return array
	 * @throws Exception
	 */
	function CreateToken($userId = 0) {
		$time = uniqid($userId) . time();
		$rand = random_int(0, 1000);
		$token = md5(md5($time) . md5($rand));
		$cryptToken = \Illuminate\Support\Facades\Crypt::encryptString($token);
		return compact('token', 'cryptToken');
	}
}

if (!function_exists('ApiReturn')) {
	/**
	 * 获取API返回固定格式
	 * @param string $status
	 * @param string $info
	 * @param array $data
	 * @param int $code
	 * @return false|string
	 */
	function ApiReturn($status='YES', $info='', $data=[], $code=API_SUCCESS) {
		if (is_int($status)) {
			$status = $status == 0 ? "YES" : "NO";
		}

		return json_encode(compact('status', 'info', 'data', 'code'), 256);
	}
}

if (!function_exists('ModelReturn')) {
	/**
	 * 模型内部返回固定格式
	 * @param int $code
	 * @param string $msg
	 * @param array $data
	 * @return array
	 */
	function ModelReturn($code = 0, $msg= '', $data=[]) {
		$code = (int)$code;
		return compact('code', 'msg', 'data');
	}
}

if (!function_exists('ArrToSql')) {
	/**
	 * 数组转换为SQL查询语句的in条件
	 * @param $arr
	 * @return string
	 */
	function ArrToSql($arr)
	{
		$str = sprintf("('%s')", implode("','", $arr));
		return $str;
	}
}

if (!function_exists('HandleLog')) {
	/**
	 * 获取处理所有日志信息log对象
	 * @return \App\Common\ScarecrowProvider\ScarecrowDoLogProvider|string
	 * @throws Exception
	 */
	function HandleLog()
	{
		return \App\Common\ScarecrowProvider\ScarecrowDoLogProvider::getExample();
	}
}

if (!function_exists('DbObjectToArr')) {
	/**
	 * DB返回的对象数据转为数组
	 * @param $data
	 * @return mixed
	 */
	function DbObjectToArr($data)
	{
		return json_decode(json_encode($data), true);
	}
}

if (!function_exists('ReadAllFiles')) {
	/**
	 * 获取文件夹下的所有文件名称（文件夹与文件有关联关系）
	 * @param $dirPath
	 * @return array
	 */
	function ReadAllFiles($dirPath) {
		$result = [];
		$handle = opendir($dirPath);
		if ( $handle )
		{
			while ( ( $file = readdir ( $handle ) ) !== false )
			{
				if ( $file != '.' && $file != '..')
				{
					$cur_path = $dirPath .'/'.$file;
					if ( is_dir ( $cur_path ) )
					{
						$result['dir'][$file] = ReadAllFiles ( $cur_path );
					}
					else
					{
						$result['file'][$file] = $cur_path;
					}
				}
			}
			closedir($handle);
		}
		$result['basePath'] = $dirPath;
		return $result;
	}
}

if (!function_exists('ReadAllDirAndFiles')) {
	/**
	 * 获取文件夹下的所有文件名称（文件夹与文件无关联关系）
	 * @param string $basePath
	 * @return array|bool
	 */
	function ReadAllDirAndFiles($basePath = '') {
		$allDir = [];
		$allFile = [];

		$basePath = realpath($basePath);
		if ($basePath === false) {
			return false;
		}

		$allDir[] = $basePath;
		$allHandleDir = $allDir;

		while (!empty($allHandleDir)) {
			$basePathTemp = array_pop($allHandleDir);
			$allObj = scandir($basePathTemp);

			foreach ($allObj as $item) {
				if ($item == '.' || $item == '..') {
					continue;
				}

				$path = $basePathTemp . '/' . $item;
				if (is_dir($path)) {
					$allDir[] = $path;
					$allHandleDir[] = $path;
				} else {
					$allFile[] = $path;
				}
			}
		}

		return [
			'dir'   =>  $allDir,
			'file'  =>  $allFile
		];
	}
}

if (!function_exists('SqlCollectToArr')) {
	/**
	 * 将SQL的结果集转为数组
	 * @param $data
	 * @return array
	 */
	function SqlCollectToArr($data) {
		$relData = [];
		foreach ($data as $key => $item) {
			if (is_object($item)) {
				$relData[$key] = (array)$item;
			} else {
				$relData[$key] = $item;
			}
		}
		return $relData;
	}
}

if (!function_exists('GetStaticHttpUrl')) {
    /**
     * 生产静态资源地址
     * @param $uri
     * @return string
     */
    function GetStaticHttpUrl($uri) {
        $baseUri = \Illuminate\Support\Env::get('STATIC_BASE_URI', '/');
        return $baseUri . trim($uri, '/');
    }
}

if (!function_exists('DataToHtmlShow')) {
	/**
	 * 将数据转为HTML格式展示
	 * @param $data
	 * @return string|string[]|null
	 */
	function DataToHtmlShow($data) {
		$data =  preg_replace('/[ ]/i', "&nbsp;", $data);
		$data =  preg_replace('/(\r\n|\n|\r)/i', "<br />", $data);
		return $data;
	}
}