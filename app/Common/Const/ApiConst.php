<?php
//全局API模块 常量定义表

//API 请求成功正常响应
defined('API_SUCCESS') OR define('API_SUCCESS', 200);
//API 签名验证失败
defined('API_CHECK_SIGN_FAIL') OR define('API_CHECK_SIGN_FAIL', -1);
//API TOKEN认证失败CODE
defined('API_CHECK_TOKEN_FAIL') OR define('API_CHECK_TOKEN_FAIL', -2);
