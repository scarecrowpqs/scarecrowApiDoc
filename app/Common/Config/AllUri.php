<?php
namespace App\Common\Config;

class AllUri {
	//生产环境URI
	private $onlineUri = [
		//示例 获取作者地址 正式环境
		'authorUri'	=>	'https://www.scarecrow.top'
	];

	//测试环境URI
	private $testUri = [
		//示例 获取作者地址 测试环境
		'authorUri'	=>	'https://www.scarecrow.top/home'
	];

	//实例对象
	private static $obj = null;

	protected function __construct(){}

	/**
	 * 获取实例对象
	 * @return AllUri|null
	 */
	public static function getExample() {
		if (self::$obj) {
			return self::$obj;
		}

		self::$obj = new self();
		return self::$obj;
	}

	/**
	 * 获取URI
	 * @param string $key
	 * @param string $default
	 * @return array|mixed|string
	 */
	public static function getUri($key='', $default='') {
		$obj = self::getExample();
		if (\Illuminate\Support\Env::get('APP_ENV', 'local') == 'production') {
			$uriList = $obj->onlineUri;
		} else {
			$uriList = $obj->testUri;
		}

		if (!$key) {
			return $uriList;
		}

		$keyList = explode('.', $key);
		$tempData = $uriList;
		foreach ($keyList as $k) {
			if (array_key_exists($k, $tempData)) {
				$tempData = $tempData[$k];
				continue;
			}

			return $default;
		}
		return $tempData;
	}
}