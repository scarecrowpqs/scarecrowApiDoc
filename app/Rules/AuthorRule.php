<?php
namespace App\Rules;


class AuthorRule implements BaseRule
{
	/**
	 * 判断验证规则是否通过
	 * @param $attribute
	 * @param $value
	 * @param $parameters
	 * @param $validator
	 * @return bool|mixed
	 */
	public function passes($attribute, $value, $parameters, $validator)
	{
		return strtolower($value) === "scarecrow";
	}

	/**
	 * 获取验证错误信息。
	 *
	 * @return string
	 */
	public function message()
	{
		return 'The :attribute must be is Scarecrow.';
	}
}