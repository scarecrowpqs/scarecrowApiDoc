<?php

namespace App\Rules;

interface BaseRule
{
	/**
	 * Determine if the validation rule passes.
	 * @param $attribute
	 * @param $value
	 * @param $parameters
	 * @param $validator
	 * @return mixed
	 */
	public function passes($attribute, $value, $parameters, $validator);

	/**
	 * Get the validation error message.
	 *
	 * @return string|array
	 */
	public function message();
}
