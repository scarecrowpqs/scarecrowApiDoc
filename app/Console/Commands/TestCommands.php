<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;

class TestCommands extends Command{

	/**
	 * 命令行执行命令
	 * @var string
	 */
	protected $signature = 'Test:run';

	/**
	 * 命令描述
	 *
	 * @var string
	 */
	protected $description = 'This Is Test Command description';


	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		//这里编写需要执行的动作
		echo "THIS IS TEST COMMANDS!";
	}
}