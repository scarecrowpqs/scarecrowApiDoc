<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static void id($token = '')
 * @method static void user($token = '')
 * @method static void logout($token = '')
 * @method static bool isAdmin($token = '')
 * @method static void CryptPassword($userPassword = '')
 * @method static void hasAuthEditProject($projectId = '', $token = '')
 * @method static void hasAuthViewProject($apiId = '', $token = '')
 * */
class ScarecrowAuth extends Facade{
	public static function getFacadeAccessor()
	{
		return 'ScarecrowAuth';
	}
}