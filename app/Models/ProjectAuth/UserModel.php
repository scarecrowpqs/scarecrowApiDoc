<?php
namespace App\Models\ProjectAuth;

use App\Facades\ScarecrowAuth;
use Illuminate\Support\Facades\DB;

class UserModel{
	/**
	 * 创建用户
	 * @param $allData
	 * @return array
	 * @throws \Exception
	 */
	public function createUser($allData) {
		$userName = $allData['userName'];
		$nickName = isset($allData['nikeName']) ? $allData['nikeName'] : "";
		$password = $allData['password'];

		$userObj = DB::table('sc_user')->where('username', $userName)->first();
		if ($userObj) {
			return ModelReturn(1, '用户账号已存在');
		}

		$userPassword = ScarecrowAuth::CryptPassword($password);
		if (!$nickName) {
			$nickName = $userName;
		}

		$insertData = [
			'nike_name'	=>	$nickName,
			'username'	=>	$userName,
			'password'	=>	$userPassword,
			'is_supper_admin'	=>	0
		];

		$userId = DB::table('sc_user')->insertGetId($insertData);
		if ($userId) {
			DB::table('sc_system_role_user')->insert([
				'role_id'	=>	1,
				'user_id'	=>	$userId
			]);
			HandleLog()->addLog('i', $allData, '创建了用户' . $userName);
			return ModelReturn(0, '创建成功');
		}
		return ModelReturn(1, '创建失败,请稍后再试');
	}

	/**
	 * 更新用户密码
	 * @param $allData
	 * @return array
	 * @throws \Exception
	 */
	public function updateUserPwd($allData) {
		$password = $allData['password'];
		$userId = $allData['userId'];
		$userPassword = ScarecrowAuth::CryptPassword($password);

		$userObj = DB::table('sc_user')->find($userId);
		if (!$userObj) {
			return ModelReturn(1, '用户不存在');
		}

		if ($userObj->is_supper_admin == 1) {
			return ModelReturn(1, '该账号为测试超级管理员，不能修改密码');
		}

		DB::table('sc_user')->where('id', $userId)->update([
			'password'	=>	$userPassword
		]);

		HandleLog()->addLog('u', $allData, '修改了用户密码');
		return ModelReturn(0, '修改成功');
	}

	/**
	 * 修改自己的密码
	 * @param $oldPwd
	 * @param $newPwd
	 * @return array
	 * @throws \Exception
	 */
	public function updateMyPwd($oldPwd, $newPwd)
	{
		$userId = ScarecrowAuth::id(APP_USER_TOKEN);
		$userOldPassword = ScarecrowAuth::CryptPassword($oldPwd);
		$userNewPassword = ScarecrowAuth::CryptPassword($newPwd);

		$userObj = DB::table('sc_user')->find($userId);
		if (!$userObj) {
			return ModelReturn(1, '用户不存在');
		}

		if ($userOldPassword !== $userObj->password) {
			return ModelReturn(1, '旧密码不正确');
		}

		DB::table('sc_user')->where('id', $userId)->update([
			'password'	=>	$userNewPassword
		]);

		HandleLog()->addLog('u', ['userId'=>$userId], '修改了用户密码');
		return ModelReturn(0, '修改成功');
	}

	/**
	 * 修改用户状态
	 * @param $allData
	 * @return array
	 * @throws \Exception
	 */
	public function stopOrStartUser($allData) {
		$userId = $allData['userId'];
		$userObj = DB::table('sc_user')->where('id', $userId)->whereIn('state', [1,2])->first();
		if (!$userObj) {
			return ModelReturn(1, '用户不存在');
		}

		if ($userObj->is_supper_admin == 1) {
			return ModelReturn(2, '超级管理员不允许改变状态');
		}

		DB::table('sc_user')->where('id', $userId)->update([
			'state'	=>	$userObj->state == 1 ? 2 : 1
		]);

		HandleLog()->addLog('u', $allData, '修改了用户状态');
		return ModelReturn(0, '修改成功');
	}

	/**
	 * 删除用户
	 * @param $allData
	 * @return array
	 * @throws \Exception
	 */
	public function deleteUser($allData) {
		$userId = $allData['userId'];
		$userObj = DB::table('sc_user')->where('id', $userId)->whereIn('state', [1,2])->first();
		if (!$userObj) {
			return ModelReturn(1, '用户不存在');
		}

		if ($userObj->is_supper_admin == 1) {
			return ModelReturn(2, '超级管理员不允许被删除');
		}

		DB::table('sc_user')->where('id', $userId)->update([
			'state'	=>	9
		]);

		HandleLog()->addLog('d', $allData, '删除了用户');
		return ModelReturn(0, '删除成功');
	}

	/**
	 * 获取用户列表
	 * @param $searchContent
	 * @param $page
	 * @param $limit
	 * @return array
	 */
	public function getUserList($searchContent, $page, $limit) {
		$userListObj = DB::table('sc_user')->whereRaw("(state != 9)");
		if ($searchContent) {
			$userListObj->whereRaw("(username like '%{$searchContent}%' OR nike_name like '%{$searchContent}%')");
		}

		$totalObj = clone $userListObj;
		$total = $totalObj->count('id');
		$index = ($page - 1) * $limit;
		$data = $userListObj->orderBy('id')->offset($index)->limit($limit)->get(['id','nike_name','username', 'state']);
		$tempData = SqlCollectToArr($data);
		$relData = [
			'total'	=>	$total,
			'limit'	=>	$limit,
			'page'	=>	$page,
			'list'	=>	$tempData
		];

		return ModelReturn(0, '获取成功', $relData);
	}

	/**
	 * 获取所有用户列表
	 * @return array
	 */
	public function getAllUserList() {
		$data = DB::table('sc_user')->whereRaw("(state != 9 AND is_supper_admin!=1)")->orderBy('id')->get(['id','nike_name','username']);
		$tempData = SqlCollectToArr($data);
		return ModelReturn(0, '获取成功', $tempData);
	}
}