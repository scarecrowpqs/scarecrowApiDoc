<?php
namespace App\Models\ProjectAuth;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ProjectAuthModel {
	/**
	 * 获取所有项目
	 * @return array
	 */
	public function getAllProject() {
		$allProjectList = DB::table('sc_api_doc_project')->whereRaw('state!=9')->orderBy('id')->get(['id', 'name']);
		$data = SqlCollectToArr($allProjectList);
		return ModelReturn(0, '获取成功', $data);
	}

	/**
	 * 获取用户当前项目权限
	 * @param $userId
	 * @param $projectId
	 * @return array
	 */
	public function getUserProjectAuth($userId, $projectId) {
		$relData = [
			'isAllProjectManage'		=>	0,
			'isAllProjectManageWrite'	=>	0,
			'isProjectManage'			=>	0,
			'isProjectManageWrite'		=>	0,
			'projectAllCanSeeApiList'	=>	[]
		];
		$isAllProjectManage = DB::table('sc_user_all_project_auth')->where("user_id", $userId)->first();
		if ($isAllProjectManage) {
			$isAllProjectManage = DbObjectToArr($isAllProjectManage);
			$relData['isAllProjectManage'] = 1;
			$relData['isAllProjectManageWrite'] = $isAllProjectManage['write'];
			return ModelReturn(0, '获取成功', $relData);
		}

		$isProjectManage = DB::table('sc_user_project_auth')->where(['user_id'=>$userId, 'project_id'=>$projectId])->first();
		if ($isProjectManage) {
			$isProjectManage = DbObjectToArr($isProjectManage);
			$relData['isProjectManage'] = 1;
			$relData['isProjectManageWrite'] = $isProjectManage['write'];
			return ModelReturn(0, '获取成功', $relData);
		}

		$allProjectApiList = DB::table('sc_user_read_api_doc as t1')->leftJoin('sc_api_doc as t2', 't1.api_doc_id',  '=', 't2.id')->whereRaw("t1.user_id=? AND t2.project_id=?", [$userId, $projectId])->pluck("t1.api_doc_id")->toArray();
		$relData['projectAllCanSeeApiList'] = $allProjectApiList;
		return ModelReturn(0, '获取成功', $relData);
	}

	/**
	 * 获取当前项目所有接口列表
	 * @param $projectId
	 * @return array
	 */
	public function getProjectAllApiList($projectId) {
        $allApiList = DB::table('sc_api_doc as t1')->leftJoin('sc_api_doc_category as t2', 't1.category_id', '=', 't2.id')->where('t1.project_id', $projectId)->whereRaw("status!=9")->select('t1.id,t1.title,t2.name,t1.url')->get();
        return ModelReturn(0, '获取成功', $allApiList);
    }

	/**
	 * 设置用户项目权限
	 * @param $userId
	 * @param $projectId
	 * @param $isAllProjectManage
	 * @param $isAllProjectManageWrite
	 * @param $isProjectManage
	 * @param $isProjectManageWrite
	 * @param $projectAllCanSeeApiList
	 * @return array
	 */
    public function setUserProjectAuth($userId, $projectId, $isAllProjectManage, $isAllProjectManageWrite, $isProjectManage, $isProjectManageWrite, $projectAllCanSeeApiList){
		try {
			Cache::forget('API_AUTH_VIEW_USER_' . $userId. '_' . $projectId);
			Cache::forget('API_AUTH_EDIT_USER_' . $userId. '_' . $projectId);
			DB::beginTransaction();
			if ($isAllProjectManage) {
				DB::table('sc_user_all_project_auth')->where('user_id', $userId)->delete();
				Db::table('sc_user_project_auth')->where('user_id', $userId)->delete();
				Db::table('sc_user_read_api_doc')->where('user_id', $userId)->delete();
				$allProjectManageInsert = [
					'user_id'	=>	$userId,
					'write'		=>	0
				];

				if ($isAllProjectManageWrite) {
					$allProjectManageInsert['write'] = 1;
				}

				DB::table('sc_user_all_project_auth')->insert($allProjectManageInsert);
				DB::commit();
				HandleLog()->addLog('i', $allProjectManageInsert, '设置了用户权限');
				return ModelReturn(0, '设置成功');
			}

			$projectObj = DB::table('sc_api_doc_project')->where('id', $projectId)->first();
			if (!$projectObj) {
				return ModelReturn(2, '设置失败,项目不存在');
			}

			$allApiList = DB::table('sc_api_doc')->where('project_id', $projectId)->pluck('id')->toArray();
			if ($isProjectManage) {
				DB::table('sc_user_all_project_auth')->where('user_id', $userId)->delete();
				Db::table('sc_user_project_auth')->where(['user_id'=> $userId, 'project_id'=>$projectId])->delete();
				Db::table('sc_user_read_api_doc')->where(['user_id'=>$userId])->whereIn('api_doc_id', $allApiList)->delete();

				$projectManageInsert = [
					'user_id'	=>	$userId,
					'project_id'=>	$projectId,
					'write'		=>	0
				];
				if ($isProjectManageWrite) {
					$projectManageInsert['write'] = 1;
				}

				DB::table('sc_user_project_auth')->insert($projectManageInsert);
				DB::commit();
				HandleLog()->addLog('i', $projectManageInsert, '设置了用户权限');
				return ModelReturn(0, '设置成功');
			}

			if ($projectAllCanSeeApiList) {
				DB::table('sc_user_all_project_auth')->where('user_id', $userId)->delete();
				Db::table('sc_user_project_auth')->where(['user_id'=> $userId, 'project_id'=>$projectId])->delete();
				Db::table('sc_user_read_api_doc')->where(['user_id'=>$userId])->whereIn('api_doc_id', $allApiList)->delete();
				$allCanInsertId = DB::table('sc_api_doc')->where('project_id', $projectId)->whereIn('id', $projectAllCanSeeApiList)->pluck('id')->toArray();

				$allInsertData = [];
				foreach ($allCanInsertId as $id) {
					$allInsertData[] = [
						'user_id'	=>	$userId,
						'api_doc_id'=>	$id
					];
				}
				if ($allInsertData) {
					DB::table('sc_user_read_api_doc')->insert($allInsertData);
				}
			}

			DB::commit();
			HandleLog()->addLog('i', [], '设置了用户权限');
			return ModelReturn(0, '设置成功');
		} catch (\Throwable $e) {
			DB::rollBack();
			return ModelReturn(1, '设置失败' . $e->getMessage());
		}
	}
}