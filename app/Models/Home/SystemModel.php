<?php
namespace App\Models\Home;

use App\Facades\ScarecrowAuth;
use Illuminate\Support\Facades\DB;

class SystemModel {
	/**
	 * 获取当前环境信息
	 * @return array
	 */
	public function getSystemInfo() {
		$userInfo = ScarecrowAuth::user(APP_USER_TOKEN);
		$userNum = DB::table('sc_user')->count('id');
		$projectNum = DB::table('sc_api_doc_project')->count('id');
		$apiNum = DB::table('sc_api_doc')->count('id');
		$relData = [
			'userName'	=>	$userInfo['username'],
			'nickName'	=>	$userInfo['nike_name'],
			'userNum'	=>	$userNum,
			'projectNum'=>	$projectNum,
			'apiNum'	=>	$apiNum,
			'gwUrl'		=>	env('APP_GW', 'http://www.scarecrow.top')
		];
		return ModelReturn(0, '获取成功', $relData);
	}

}