# lumen_xadmin

### API请求地址 http://host/
### 后端页面地址 http://host/admin/login.html
### 用户默认登录账号:18888888888  密码:123456

#### Description
lumen做API开发框架，x-admin做后端管理界面，一款基础的快速开发框架

# 后端lumen开发架构介绍
## 总纲
### 1、公共函数、常量、服务提供者实现类、工具类定义
    1、公共常量定义目录
    path:app/Common/Const
        1、ApiConst.php:全局常量定义文件

    2、公共函数定义目录
    path:app/Common/Function
        1、ScarecrowFunction.php:全局公共函数定义文件
        
    3、服务提供者类、工具类目录
    path:app/Common/ScarecrowProvider
        1、ScarecrowAuthProvider.php:权限管理服务提供者实现类
        2、ScarecrowDoLogProvider.php:全局操作日志处理类(该类主要用于对敏感操作主动进行日志记录)
        
    
### 2、服务提供者、门面定义
    1、服务提提供者类：
    自定义权限管理服务提供者
    path:app/Providers/ScarecrowAuthServiceProvider
    
    2、门面类:
    自定义权限管理门面(类似官方自带Auth)
    path:app/Facades/ScarecrowAuth
    
### 3、中间件(路由中间件:只对指定此中间件的路由生效;全局中间件:对所有的路由都生效)
    1、API鉴权中间件(路由中间件)
    path:app/Http/Middleware/ApiTokenCheckMiddleware
    介绍:该中间用于对API的登录判定，API路由权限判定，自动更新API的访问有效期、次数等
    
    2、验签中间件(路由中间件)
    path:app/Http/Middleware/CheckApiSignMiddleware
    介绍:该中间件用于对API的签名进行判定
    配置(在.env文件中配置即可生效):
        #API是否验签
        IS_CHECK_SIGN:true 
        #签名盐值,客户端签名的盐值必须与此值一致否则签名验证不会通过,如果没有配置则默认为 SCARECROW_API_SOCKET
        APP_SOCKET_KEY:KEY 
        
    3、开启跨域中间件(全局中间件)
    path:app/Http/Middleware/CheckApiSignMiddleware
    介绍:该中间件用于所有路由,允许接口跨域
    配置(在.env文件中配置即可生效):
        #是否允许API跨域
        IS_ALLOW_ORIGIN:true 
        
    4、异常接管中间件(全局中间件)
    path:app/Http/Middleware/HandleExceptionMiddleware
    介绍:该中间件用于格式化异常输出格式，使其与API正常响应输出格式一致
    配置(在.env文件中配置即可生效):
        #是否格式化异常输出
        IS_FORMAT_EXCEPTION=true    
     
## 基础内置模块
### 用户、登录、API有效期管理
    ptah:app/Http/Controllers/AuthManage/LoginController
    路由地址:routes/auth.php
    文档地址:doc/login_doc.md
    
### 菜单管理
    ptah:app/Http/Controllers/AuthManage/MenuController
    路由地址:routes/auth.php
    文档地址:doc/menu_doc.md
    
### 权限管理
    ptah:app/Http/Controllers/AuthManage/AuthController
    路由地址:routes/auth.php
    文档地址:doc/auth_doc.md
    
## 其他介绍
    1、.env配置:
        #操作日志存入引擎 该配置是配合全局操作日志处理类进行选择日志存储引擎的(file/mysql)
        ACTION_LOG_WRITE_TYPE=file
    
    2、全局函数:
        ApiReturn:该函数作为全局API统一返回格式
        ModelReturn:该函数作为全局MODEL统一返回格式
        HandleLog:该函数返回一个全局操作日志处理类，调用其中的addLog方法进行全局日志添加
        DieDump:全局断点输出方法
