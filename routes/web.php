<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
//用户、登录、权限管理路由模块，不想使用可以注释
require_once __DIR__ . '/auth.php';

$router->get('/', function (){
	return redirect('/admin');
});

//测试接口
$router->get('/test', "ExampleController@index");


$router->group(['prefix'=>'/api', 'middleware'=>['apiTokenCheck','checkApiSign']], function () use ($router) {
	//首页
	$router->group(['namespace'=>'Home'], function () use ($router) {
		//获取当前环境信息
		$router->post('system/getsysteminfo', 'SystemController@getSystemInfo');
	});

	//系统设置
	$router->group(['namespace'=>'ProjectAuth'], function () use ($router) {
		//创建用户
		$router->post('user/createuser', 'UserController@createUser');
		//更新用户密码
		$router->post('user/updateuserpwd', 'UserController@updateUserPwd');
		//修改自己的密码
		$router->post('user/updatemypwd', 'UserController@updateMyPwd');
		//修改用户状态
		$router->post('user/stoporstartuser', 'UserController@stopOrStartUser');
		//删除用户
		$router->post('user/deleteuser', 'UserController@deleteUser');
		//获取用户列表
		$router->post('user/getuserlist', 'UserController@getUserList');
		//获取所有用户列表
		$router->post('user/getalluserlist', 'UserController@getAllUserList');
	});

	//系统设置
	$router->group(['namespace'=>'ProjectAuth'], function () use ($router) {
		//获取所有项目
		$router->post('projectauth/getallproject', 'ProjectAuthController@getAllProject');
		//获取用户当前项目权限
		$router->post('projectauth/getuserprojectauth', 'ProjectAuthController@getUserProjectAuth');
		//设置用户项目权限
		$router->post('projectauth/setuserprojectauth', 'ProjectAuthController@setUserProjectAuth');
	});

    //系统设置
    $router->group(['namespace'=>'ProjectAuth'], function () use ($router) {
        //创建项目
        $router->post('projectmanage/createproject', 'ProjectManageController@createProject');
        //编辑项目
        $router->post('projectmanage/editproject', 'ProjectManageController@editProject');
        //改变项目状态
        $router->post('projectmanage/changeprojectstate', 'ProjectManageController@changeProjectState');
        //获取所有可以编辑项目
        $router->post('projectmanage/getallcaneditproject', 'ProjectManageController@getAllCanEditProject');
        //获取所有可以查看项目
        $router->post('projectmanage/getallcanviewproject', 'ProjectManageController@getAllCanViewProject');
        //删除项目
        $router->post('projectmanage/deleteproject', 'ProjectManageController@deleteProject');
        //创建分类
        $router->post('projectmanage/createcategory', 'ProjectManageController@createCategory');
        //编辑分类
        $router->post('projectmanage/updatecategory', 'ProjectManageController@updateCategory');
        //删除分类
        $router->post('projectmanage/deletecategory', 'ProjectManageController@deleteCategory');
        //获取所有分类
        $router->post('projectmanage/getallcategory', 'ProjectManageController@getAllCategory');
        //获取项目信息
        $router->post('projectmanage/getprojectinfo', 'ProjectManageController@getProjectInfo');
        //获取分类信息
        $router->post('projectmanage/getcategoryinfo', 'ProjectManageController@getCategoryInfo');
        //导出项目
        $router->post('projectmanage/exportproject', 'ProjectManageController@exportProject');
    });

	//文档设置
	$router->group(['namespace'=>'ApiDoc'], function () use ($router) {
		//创建接口
		$router->post('apidoc/createapi', 'ApiDocController@createApi');
		//编辑接口
		$router->post('apidoc/editapi', 'ApiDocController@editApi');
		//删除接口
		$router->post('apidoc/deleteapi', 'ApiDocController@deleteApi');
		//获取接口文档列表分页
		$router->post('apidoc/getapidoclistpage', 'ApiDocController@getApiDocListPage');
		//获取接口文档列表
		$router->post('apidoc/getapidoclist', 'ApiDocController@getApiDocList');
		//获取接口文档详情
		$router->post('apidoc/getapidocinfo', 'ApiDocController@getApiDocInfo');
		//导出文档
		$router->post('apidoc/exportapidoc', 'ApiDocController@exportApiDoc');
	});
});


