<?php
/** @var \Laravel\Lumen\Routing\Router $router */

//如果你不想使用登录模块,可以将此路由组删除，如果你想使用用户登录模块,请将false改为true
true && $router->group(['prefix'=>'/login', 'namespace'=>'AuthManage'], function () use ($router) {
	//用户登录
	$router->post('/', 'LoginController@login');

	$router->group(['middleware' => ['apiTokenCheck', 'checkApiSign']], function () use ($router) {
		//更新用户TOKEN
		$router->post('/update_token', 'LoginController@updateToken');
		//退出登录
		$router->post('/login_out', 'LoginController@loginOut');
		//获取当前用户信息
		$router->post('/get_user_info', 'LoginController@getUserInfo');
		//获取用户菜单
		$router->post('/getusermenulist', 'LoginController@getUserMenuList');
	});
});

//如果你需要使用签名功能，则请在该路由组下面进行路由添加，或者自行使用中间件checkApiSign
false && $router->group(['middleware' => 'checkApiSign'], function () use ($router) {

});

//如果你需要使用自带的权限管理功能，则请在打开该路由组即可使用,如果使用此组件则请打开用户登录模块
false && $router->group(['namespace'=>'AuthManage', 'middleware'=>['checkApiSign', 'apiTokenCheck']], function () use ($router) {
	/******************************菜单路由列表***************************************/
	//创建菜单
	$router->post('menu/addmenu', 'MenuController@addMenu');
	//编辑菜单
	$router->post('menu/editmenu', 'MenuController@editMenu');
	//删除菜单
	$router->post('menu/deletemenu', 'MenuController@deleteMenu');
	//获取菜单列表
	$router->post('menu/getmenulist', 'MenuController@getMenuList');
	//获取菜单树状列表
	$router->post('menu/getmenutree', 'MenuController@getMenuTree');
	//创建功能
	$router->post('menu/addfunction', 'MenuController@addFunction');
	//编辑功能
	$router->post('menu/editfunction', 'MenuController@editFunction');
	//删除功能
	$router->post('menu/deletefunction', 'MenuController@deleteFunction');
	//获取功能详情
	$router->post('menu/getfunction', 'MenuController@getFunction');
	//获取功能列表
	$router->post('menu/getfunctionlist', 'MenuController@getFunctionList');
	//添加权限路由
	$router->post('menu/addapiroute', 'MenuController@addApiRoute');
	//编辑权限路由
	$router->post('menu/editapiroute', 'MenuController@editApiRoute');
	//删除权限路由
	$router->post('menu/deleteapiroute', 'MenuController@deleteApiRoute');
	//获取权限路由详情
	$router->post('menu/getapiroute', 'MenuController@getApiRoute');
	//获取权限路由列表
	$router->post('menu/getapiroutelist', 'MenuController@getApiRouteList');
	//获取菜单详情
	$router->post('menu/getmenu', 'MenuController@getMenu');
	/******************************END*******************************************/


	/******************************权限、角色管理************************************/
	//创建权限
	$router->post('auth/createauth', 'AuthController@createAuth');
	//编辑权限V
	$router->post('auth/editauth', 'AuthController@editAuth');
	//删除权限
	$router->post('auth/deleteauth', 'AuthController@deleteAuth');
	//获取权限详情
	$router->post('auth/getauth', 'AuthController@getAuth');
	//获取权限列表
	$router->post('auth/getauthlist', 'AuthController@getAuthList');
	//获取权限列表分页
	$router->post('auth/getauthlistpage', 'AuthController@getAuthListPage');
	//创建角色
	$router->post('auth/createrole', 'AuthController@createRole');
	//编辑角色
	$router->post('auth/editrole', 'AuthController@editRole');
	//删除角色
	$router->post('auth/deleterole', 'AuthController@deleteRole');
	//获取角色详情
	$router->post('auth/getrole', 'AuthController@getRole');
	//获取角色列表
	$router->post('auth/getrolelist', 'AuthController@getRoleList');
	//获取角色列表分页
	$router->post('auth/getrolelistpage', 'AuthController@getRoleListPage');
	//获取角色拥有的权限列表
	$router->post('auth/getroleauthlist', 'AuthController@getRoleAuthList');
	//用户绑定角色
	$router->post('auth/rolebinduser', 'AuthController@roleBindUser');
	//获取当前用户拥有的所有菜单
	$router->post('auth/getuserallmenu', 'AuthController@getUserAllMenu');
	//获取某个权限对应的所有功能
	$router->post('auth/getauthtofunctionlist', 'AuthController@getAuthToFunctionList');
	//权限绑定功能
	$router->post('auth/authbindfunction', 'AuthController@authBindFunction');
	//角色绑定权限
	$router->post('auth/rolebindauth', 'AuthController@roleBindAuth');
	/**********************************END****************************************/
});