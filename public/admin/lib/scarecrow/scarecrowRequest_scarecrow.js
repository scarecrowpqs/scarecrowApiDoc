(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["scarecrowRequest"] = factory();
	else
		root["scarecrowRequest"] = factory();
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/scarecrowRequest/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/scarecrowRequest/index.js":
/*!***************************************!*\
  !*** ./src/scarecrowRequest/index.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var ScarecrowRequest = (function(window) {
    var ScarecrowRequest = function(params) {
        return  new ScarecrowRequest.fn.init(params);
    };

    ScarecrowRequest.fn = ScarecrowRequest.prototype = {
        constructor: ScarecrowRequest,
        //初始化
        init: function(parma = {}) {
            //请求前执行的回调函数
            this.__beforeRequestCallback = [];
            //请求响应后执行的回调函数
            this.__afterRequestCallback = [];
            //请求header
            this.__headerParams = [];

            this.method = parma['type'] ? parma['type'].toUpperCase() : "GET";
            this.dataType = parma['dataType'] ? parma['dataType'].toUpperCase() : "JSON";
            this.url = parma['url'] ?  parma['url'] : '';
            this.data = parma['data'] ?  parma['data'] : {};
            this.async  = parma['async'] ? true : false;
            this.successFunc = parma['success'] && (typeof parma['success'] == 'function') ?  parma['success'] : ()=>{};
            this.errorFunc = parma['error'] && (typeof parma['error'] == 'function') ?  parma['error'] : (e)=>{ throw e;};
            this.compileFunc = parma['compile'] && (typeof parma['compile'] == 'function') ?  parma['compile'] : ()=>{};
            parma['header'] = parma['header'] && Array.isArray(parma['header']) ? parma['header'] : [];
            
            for (let index in parma['header']) {
                if (typeof parma['header'][index] !== 'object') {
                    throw new Error("header arr item value format not right,it`s must object!");
                }
                
                if (!parma['header'][index].hasOwnProperty('key')) {
                    throw new Error("header arr item value format not right,it`s must object key key!");
                }
                
                if (!parma['header'][index].hasOwnProperty('value')) {
                    throw new Error("header arr item value format not right,it`s must object key value!");
                }
                
                this.setHeader(parma['header'][index]['key'], parma['header'][index]['value']);
            }
        },
        //作者
        author: "Scarecrow",
        //请求地址
        url:"",
        //请求类型
        method:"GET",
        //响应数据类型
        dataType:"JSON",
        //数据
        data:"",
        //是否异步
        async:true,
        //数据返回处理类型
        dataTypeFunctionList:{
            JSON:(d)=>{return JSON.parse(d);},
            TEXT:(d)=>{return d;}
        },
        //成功执行回调
        successFunc:()=>{},
        //失败执行回调
        errorFunc:(e)=>{ throw e; },
        //执行完成后执行回调
        compileFunc:()=>{}
    };

    //请求前过滤函数
    ScarecrowRequest.fn.beforeRequestCallback = function (callback) {
        if (typeof callback == "function") {
            this.__beforeRequestCallback.push(callback);
        } else {
            throw new Error("beforeRequestCallback parameter 1 must is function");
        }
    };

    //请求后过滤函数
    ScarecrowRequest.fn.afterRequestCallback = function (callback) {
        if (typeof callback == "function") {
            this.__afterRequestCallback.push(callback);
        } else {
            throw new Error("afterRequestCallback parameter 1 must is function");
        }
    };

    //设置DataType返回类型处理
    ScarecrowRequest.fn.setDataTypeFunction = function(key, callback) {
        key = key.toUpperCase();
        if (typeof callback == "function") {
            if (this.dataTypeFunctionList.hasOwnProperty(key)) {
                throw new Error('setDataTypeFunction parameter 1 is existence');
            }

            this.dataTypeFunctionList[key] = callback;
        } else {
            throw new Error("setDataTypeFunction parameter 2 must is function");
        }
    };
    
    //设置HEADER
    ScarecrowRequest.fn.setHeader = function(key, value) {
          this.__headerParams[key] = value;
    };

    ScarecrowRequest.fn.get = function (url, data={}, successCallback=()=>{}, errorCallback=()=>{}, compileCallback=()=>{}) {
        this.request({
            url:url,
            data:data,
            type:"GET",
            success:successCallback,
            error:errorCallback,
            compile:compileCallback
        });
    };

    ScarecrowRequest.fn.post = function (url, data={}, successCallback=()=>{}, errorCallback=()=>{}, compileCallback=()=>{}) {
        this.request({
            url:url,
            data:data,
            type:"POST",
            success:successCallback,
            error:errorCallback,
            compile:compileCallback
        });
    };

    ScarecrowRequest.fn.request = function (parma={}) {
        var xhr;
        let method = parma['type'] ? parma['type'].toUpperCase() : "GET";
        let dataType = parma['dataType'] ? parma['dataType'].toUpperCase() : "JSON";
        let url = parma['url'] ?  parma['url'] : '';
        let data = parma['data'] ?  parma['data'] : {};
        let async  = parma['async'] ? true : false;
        let successFunc = parma['success'] && (typeof parma['success'] == 'function') ?  parma['success'] : this.successFunc;
        let errorFunc = parma['error'] && (typeof parma['error'] == 'function') ?  parma['error'] : this.errorFunc;
        let compileFunc = parma['compile'] && (typeof parma['compile'] == 'function') ?  parma['compile'] : this.compileFunc;
        parma['header'] = parma['header'] && Array.isArray(parma['header']) ? parma['header'] : [];
            
        for (let index in parma['header']) {
            if (typeof parma['header'][index] !== 'object') {
                throw new Error("header arr item value format not right,it`s must object!");
            }
            
            if (!parma['header'][index].hasOwnProperty('key')) {
                throw new Error("header arr item value format not right,it`s must object key key!");
            }
            
            if (!parma['header'][index].hasOwnProperty('value')) {
                throw new Error("header arr item value format not right,it`s must object key value!");
            }
            
            this.setHeader(parma['header'][index]['key'], parma['header'][index]['value']);
        }
        
        
        if(window.XMLHttpRequest) {
          xhr = new XMLHttpRequest();
        } else {
          xhr = new ActiveXObject();
        }

        if(method == 'GET' && data) {
            url = url + '?';
            if (typeof data == 'object') {
                for (let i in data) {
                    url += i +"=" + data[i] + "&";
                }
            } else {
                url = url + data;
            }
        }
        
        for (let beforeCallbackIndex in this.__beforeRequestCallback) {
            let dataObj = this.__beforeRequestCallback[beforeCallbackIndex](data);
            if (typeof dataObj !== 'object') {
                throw new Error("beforeRequestCallback return value format not right,it`s must object!");
            }

            if (!dataObj.hasOwnProperty('state')) {
                throw new Error("beforeRequestCallback return value format not right,it`s must object key state!");
            }
            
            if (!dataObj.hasOwnProperty('data')) {
                throw new Error("beforeRequestCallback return value format not right,it`s must object key data!");
            }

            if (dataObj['state'] === false) {
                return false;
            }

            data = dataObj['data'];
        }

        xhr.open(method, url, async);
        
        for (let index in this.__headerParams) {
            xhr.setRequestHeader(index, this.__headerParams[index]);
        }
        
        if(method == 'GET') {
          xhr.send(null);
        } else {
          let datas = new URLSearchParams();
          for (let i in data) {
            datas.append(i, data[i]);
          }
          xhr.send(datas);
        }

        if (async) {               
            xhr.onreadystatechange=function(){
                if(xhr.readyState==4){
                    for (let afterCallbackIndex in this.__afterRequestCallback) {
                        let dataObj = this.__afterRequestCallback[afterCallbackIndex](xhr);
                        if (typeof dataObj !== 'object') {
                            throw new Error("afterRequestCallback return value format not right,it`s must object!");
                        }

                        if (!dataObj.hasOwnProperty('state')) {
                            throw new Error("afterRequestCallback return value format not right,it`s must object key state!");
                        }
                        
                        if (!dataObj.hasOwnProperty('data')) {
                            throw new Error("afterRequestCallback return value format not right,it`s must object key data!");
                        }

                        if (dataObj['state'] === false) {
                            return false;
                        }

                        xhr = dataObj['data'];
                    }

                    if(xhr.status==200){
                        if (!this.dataTypeFunctionList.hasOwnProperty(dataType)) {
                            throw new Error("dataType don`t existent:" + dataType);
                        }

                        successFunc(this.dataTypeFunctionList[dataType](xhr.responseText), xhr);
                    }else{
                        errorFunc(new Error(xhr.status + ":" + xhr.statusText),xhr);
                    }

                    compileFunc(xhr);
                }
            }
        } else {
            if(xhr.readyState==4){
                for (let afterCallbackIndex in this.__afterRequestCallback) {
                    let dataObj = this.__afterRequestCallback[afterCallbackIndex](xhr);
                    if (typeof dataObj !== 'object') {
                        throw new Error("afterRequestCallback return value format not right,it`s must object!");
                    }

                    if (!dataObj.hasOwnProperty('state')) {
                        throw new Error("afterRequestCallback return value format not right,it`s must object key state!");
                    }
                    
                    if (!dataObj.hasOwnProperty('data')) {
                        throw new Error("afterRequestCallback return value format not right,it`s must object key data!");
                    }

                    if (dataObj['state'] === false) {
                        return false;
                    }

                    xhr = dataObj['data'];
                }

                if(xhr.status==200){
                    if (!this.dataTypeFunctionList.hasOwnProperty(dataType)) {
                        throw new Error("dataType don`t existent:" + dataType);
                    }

                    successFunc(this.dataTypeFunctionList[dataType](xhr.responseText),xhr);
                }else{
                    errorFunc(new Error(xhr.status + ":" + xhr.statusText),xhr);
                }

                compileFunc(xhr);
            }
        }

    };

    ScarecrowRequest.fn.init.prototype = ScarecrowRequest.fn;
    return ScarecrowRequest;
})(window);
//挂在全局
module.exports = ScarecrowRequest;

/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9zY2FyZWNyb3dSZXF1ZXN0L3dlYnBhY2svdW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbiIsIndlYnBhY2s6Ly9zY2FyZWNyb3dSZXF1ZXN0L3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL3NjYXJlY3Jvd1JlcXVlc3QvLi9zcmMvc2NhcmVjcm93UmVxdWVzdC9pbmRleC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QsTztRQ1ZBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQztBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0dBQStHO0FBQy9HO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsc0JBQXNCO0FBQzdDLHVCQUF1QjtBQUN2QixTQUFTO0FBQ1Q7QUFDQSwwQkFBMEI7QUFDMUI7QUFDQSx3QkFBd0IsU0FBUyxFQUFFO0FBQ25DO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxvREFBb0Qsd0JBQXdCLHNCQUFzQix3QkFBd0I7QUFDMUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUEscURBQXFELHdCQUF3QixzQkFBc0Isd0JBQXdCO0FBQzNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBLG9EQUFvRDtBQUNwRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsb0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQSxrQyIsImZpbGUiOiJzY2FyZWNyb3dSZXF1ZXN0X3NjYXJlY3Jvdy5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiB3ZWJwYWNrVW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbihyb290LCBmYWN0b3J5KSB7XG5cdGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0Jylcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKTtcblx0ZWxzZSBpZih0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpXG5cdFx0ZGVmaW5lKFtdLCBmYWN0b3J5KTtcblx0ZWxzZSBpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpXG5cdFx0ZXhwb3J0c1tcInNjYXJlY3Jvd1JlcXVlc3RcIl0gPSBmYWN0b3J5KCk7XG5cdGVsc2Vcblx0XHRyb290W1wic2NhcmVjcm93UmVxdWVzdFwiXSA9IGZhY3RvcnkoKTtcbn0pKHdpbmRvdywgZnVuY3Rpb24oKSB7XG5yZXR1cm4gIiwiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvc2NhcmVjcm93UmVxdWVzdC9pbmRleC5qc1wiKTtcbiIsInZhciBTY2FyZWNyb3dSZXF1ZXN0ID0gKGZ1bmN0aW9uKHdpbmRvdykge1xyXG4gICAgdmFyIFNjYXJlY3Jvd1JlcXVlc3QgPSBmdW5jdGlvbihwYXJhbXMpIHtcclxuICAgICAgICByZXR1cm4gIG5ldyBTY2FyZWNyb3dSZXF1ZXN0LmZuLmluaXQocGFyYW1zKTtcclxuICAgIH07XHJcblxyXG4gICAgU2NhcmVjcm93UmVxdWVzdC5mbiA9IFNjYXJlY3Jvd1JlcXVlc3QucHJvdG90eXBlID0ge1xyXG4gICAgICAgIGNvbnN0cnVjdG9yOiBTY2FyZWNyb3dSZXF1ZXN0LFxyXG4gICAgICAgIC8v5Yid5aeL5YyWXHJcbiAgICAgICAgaW5pdDogZnVuY3Rpb24ocGFybWEgPSB7fSkge1xyXG4gICAgICAgICAgICAvL+ivt+axguWJjeaJp+ihjOeahOWbnuiwg+WHveaVsFxyXG4gICAgICAgICAgICB0aGlzLl9fYmVmb3JlUmVxdWVzdENhbGxiYWNrID0gW107XHJcbiAgICAgICAgICAgIC8v6K+35rGC5ZON5bqU5ZCO5omn6KGM55qE5Zue6LCD5Ye95pWwXHJcbiAgICAgICAgICAgIHRoaXMuX19hZnRlclJlcXVlc3RDYWxsYmFjayA9IFtdO1xyXG4gICAgICAgICAgICAvL+ivt+axgmhlYWRlclxyXG4gICAgICAgICAgICB0aGlzLl9faGVhZGVyUGFyYW1zID0gW107XHJcblxyXG4gICAgICAgICAgICB0aGlzLm1ldGhvZCA9IHBhcm1hWyd0eXBlJ10gPyBwYXJtYVsndHlwZSddLnRvVXBwZXJDYXNlKCkgOiBcIkdFVFwiO1xyXG4gICAgICAgICAgICB0aGlzLmRhdGFUeXBlID0gcGFybWFbJ2RhdGFUeXBlJ10gPyBwYXJtYVsnZGF0YVR5cGUnXS50b1VwcGVyQ2FzZSgpIDogXCJKU09OXCI7XHJcbiAgICAgICAgICAgIHRoaXMudXJsID0gcGFybWFbJ3VybCddID8gIHBhcm1hWyd1cmwnXSA6ICcnO1xyXG4gICAgICAgICAgICB0aGlzLmRhdGEgPSBwYXJtYVsnZGF0YSddID8gIHBhcm1hWydkYXRhJ10gOiB7fTtcclxuICAgICAgICAgICAgdGhpcy5hc3luYyAgPSBwYXJtYVsnYXN5bmMnXSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5zdWNjZXNzRnVuYyA9IHBhcm1hWydzdWNjZXNzJ10gJiYgKHR5cGVvZiBwYXJtYVsnc3VjY2VzcyddID09ICdmdW5jdGlvbicpID8gIHBhcm1hWydzdWNjZXNzJ10gOiAoKT0+e307XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JGdW5jID0gcGFybWFbJ2Vycm9yJ10gJiYgKHR5cGVvZiBwYXJtYVsnZXJyb3InXSA9PSAnZnVuY3Rpb24nKSA/ICBwYXJtYVsnZXJyb3InXSA6IChlKT0+eyB0aHJvdyBlO307XHJcbiAgICAgICAgICAgIHRoaXMuY29tcGlsZUZ1bmMgPSBwYXJtYVsnY29tcGlsZSddICYmICh0eXBlb2YgcGFybWFbJ2NvbXBpbGUnXSA9PSAnZnVuY3Rpb24nKSA/ICBwYXJtYVsnY29tcGlsZSddIDogKCk9Pnt9O1xyXG4gICAgICAgICAgICBwYXJtYVsnaGVhZGVyJ10gPSBwYXJtYVsnaGVhZGVyJ10gJiYgQXJyYXkuaXNBcnJheShwYXJtYVsnaGVhZGVyJ10pID8gcGFybWFbJ2hlYWRlciddIDogW107XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBmb3IgKGxldCBpbmRleCBpbiBwYXJtYVsnaGVhZGVyJ10pIHtcclxuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgcGFybWFbJ2hlYWRlciddW2luZGV4XSAhPT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJoZWFkZXIgYXJyIGl0ZW0gdmFsdWUgZm9ybWF0IG5vdCByaWdodCxpdGBzIG11c3Qgb2JqZWN0IVwiKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgaWYgKCFwYXJtYVsnaGVhZGVyJ11baW5kZXhdLmhhc093blByb3BlcnR5KCdrZXknKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcImhlYWRlciBhcnIgaXRlbSB2YWx1ZSBmb3JtYXQgbm90IHJpZ2h0LGl0YHMgbXVzdCBvYmplY3Qga2V5IGtleSFcIik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIGlmICghcGFybWFbJ2hlYWRlciddW2luZGV4XS5oYXNPd25Qcm9wZXJ0eSgndmFsdWUnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcImhlYWRlciBhcnIgaXRlbSB2YWx1ZSBmb3JtYXQgbm90IHJpZ2h0LGl0YHMgbXVzdCBvYmplY3Qga2V5IHZhbHVlIVwiKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRIZWFkZXIocGFybWFbJ2hlYWRlciddW2luZGV4XVsna2V5J10sIHBhcm1hWydoZWFkZXInXVtpbmRleF1bJ3ZhbHVlJ10pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICAvL+S9nOiAhVxyXG4gICAgICAgIGF1dGhvcjogXCJTY2FyZWNyb3dcIixcclxuICAgICAgICAvL+ivt+axguWcsOWdgFxyXG4gICAgICAgIHVybDpcIlwiLFxyXG4gICAgICAgIC8v6K+35rGC57G75Z6LXHJcbiAgICAgICAgbWV0aG9kOlwiR0VUXCIsXHJcbiAgICAgICAgLy/lk43lupTmlbDmja7nsbvlnotcclxuICAgICAgICBkYXRhVHlwZTpcIkpTT05cIixcclxuICAgICAgICAvL+aVsOaNrlxyXG4gICAgICAgIGRhdGE6XCJcIixcclxuICAgICAgICAvL+aYr+WQpuW8guatpVxyXG4gICAgICAgIGFzeW5jOnRydWUsXHJcbiAgICAgICAgLy/mlbDmja7ov5Tlm57lpITnkIbnsbvlnotcclxuICAgICAgICBkYXRhVHlwZUZ1bmN0aW9uTGlzdDp7XHJcbiAgICAgICAgICAgIEpTT046KGQpPT57cmV0dXJuIEpTT04ucGFyc2UoZCk7fSxcclxuICAgICAgICAgICAgVEVYVDooZCk9PntyZXR1cm4gZDt9XHJcbiAgICAgICAgfSxcclxuICAgICAgICAvL+aIkOWKn+aJp+ihjOWbnuiwg1xyXG4gICAgICAgIHN1Y2Nlc3NGdW5jOigpPT57fSxcclxuICAgICAgICAvL+Wksei0peaJp+ihjOWbnuiwg1xyXG4gICAgICAgIGVycm9yRnVuYzooZSk9PnsgdGhyb3cgZTsgfSxcclxuICAgICAgICAvL+aJp+ihjOWujOaIkOWQjuaJp+ihjOWbnuiwg1xyXG4gICAgICAgIGNvbXBpbGVGdW5jOigpPT57fVxyXG4gICAgfTtcclxuXHJcbiAgICAvL+ivt+axguWJjei/h+a7pOWHveaVsFxyXG4gICAgU2NhcmVjcm93UmVxdWVzdC5mbi5iZWZvcmVSZXF1ZXN0Q2FsbGJhY2sgPSBmdW5jdGlvbiAoY2FsbGJhY2spIHtcclxuICAgICAgICBpZiAodHlwZW9mIGNhbGxiYWNrID09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgICAgICB0aGlzLl9fYmVmb3JlUmVxdWVzdENhbGxiYWNrLnB1c2goY2FsbGJhY2spO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcImJlZm9yZVJlcXVlc3RDYWxsYmFjayBwYXJhbWV0ZXIgMSBtdXN0IGlzIGZ1bmN0aW9uXCIpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgLy/or7fmsYLlkI7ov4fmu6Tlh73mlbBcclxuICAgIFNjYXJlY3Jvd1JlcXVlc3QuZm4uYWZ0ZXJSZXF1ZXN0Q2FsbGJhY2sgPSBmdW5jdGlvbiAoY2FsbGJhY2spIHtcclxuICAgICAgICBpZiAodHlwZW9mIGNhbGxiYWNrID09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgICAgICB0aGlzLl9fYWZ0ZXJSZXF1ZXN0Q2FsbGJhY2sucHVzaChjYWxsYmFjayk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiYWZ0ZXJSZXF1ZXN0Q2FsbGJhY2sgcGFyYW1ldGVyIDEgbXVzdCBpcyBmdW5jdGlvblwiKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIC8v6K6+572uRGF0YVR5cGXov5Tlm57nsbvlnovlpITnkIZcclxuICAgIFNjYXJlY3Jvd1JlcXVlc3QuZm4uc2V0RGF0YVR5cGVGdW5jdGlvbiA9IGZ1bmN0aW9uKGtleSwgY2FsbGJhY2spIHtcclxuICAgICAgICBrZXkgPSBrZXkudG9VcHBlckNhc2UoKTtcclxuICAgICAgICBpZiAodHlwZW9mIGNhbGxiYWNrID09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kYXRhVHlwZUZ1bmN0aW9uTGlzdC5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XHJcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ3NldERhdGFUeXBlRnVuY3Rpb24gcGFyYW1ldGVyIDEgaXMgZXhpc3RlbmNlJyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuZGF0YVR5cGVGdW5jdGlvbkxpc3Rba2V5XSA9IGNhbGxiYWNrO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcInNldERhdGFUeXBlRnVuY3Rpb24gcGFyYW1ldGVyIDIgbXVzdCBpcyBmdW5jdGlvblwiKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICAvL+iuvue9rkhFQURFUlxyXG4gICAgU2NhcmVjcm93UmVxdWVzdC5mbi5zZXRIZWFkZXIgPSBmdW5jdGlvbihrZXksIHZhbHVlKSB7XHJcbiAgICAgICAgICB0aGlzLl9faGVhZGVyUGFyYW1zW2tleV0gPSB2YWx1ZTtcclxuICAgIH07XHJcblxyXG4gICAgU2NhcmVjcm93UmVxdWVzdC5mbi5nZXQgPSBmdW5jdGlvbiAodXJsLCBkYXRhPXt9LCBzdWNjZXNzQ2FsbGJhY2s9KCk9Pnt9LCBlcnJvckNhbGxiYWNrPSgpPT57fSwgY29tcGlsZUNhbGxiYWNrPSgpPT57fSkge1xyXG4gICAgICAgIHRoaXMucmVxdWVzdCh7XHJcbiAgICAgICAgICAgIHVybDp1cmwsXHJcbiAgICAgICAgICAgIGRhdGE6ZGF0YSxcclxuICAgICAgICAgICAgdHlwZTpcIkdFVFwiLFxyXG4gICAgICAgICAgICBzdWNjZXNzOnN1Y2Nlc3NDYWxsYmFjayxcclxuICAgICAgICAgICAgZXJyb3I6ZXJyb3JDYWxsYmFjayxcclxuICAgICAgICAgICAgY29tcGlsZTpjb21waWxlQ2FsbGJhY2tcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgU2NhcmVjcm93UmVxdWVzdC5mbi5wb3N0ID0gZnVuY3Rpb24gKHVybCwgZGF0YT17fSwgc3VjY2Vzc0NhbGxiYWNrPSgpPT57fSwgZXJyb3JDYWxsYmFjaz0oKT0+e30sIGNvbXBpbGVDYWxsYmFjaz0oKT0+e30pIHtcclxuICAgICAgICB0aGlzLnJlcXVlc3Qoe1xyXG4gICAgICAgICAgICB1cmw6dXJsLFxyXG4gICAgICAgICAgICBkYXRhOmRhdGEsXHJcbiAgICAgICAgICAgIHR5cGU6XCJQT1NUXCIsXHJcbiAgICAgICAgICAgIHN1Y2Nlc3M6c3VjY2Vzc0NhbGxiYWNrLFxyXG4gICAgICAgICAgICBlcnJvcjplcnJvckNhbGxiYWNrLFxyXG4gICAgICAgICAgICBjb21waWxlOmNvbXBpbGVDYWxsYmFja1xyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBTY2FyZWNyb3dSZXF1ZXN0LmZuLnJlcXVlc3QgPSBmdW5jdGlvbiAocGFybWE9e30pIHtcclxuICAgICAgICB2YXIgeGhyO1xyXG4gICAgICAgIGxldCBtZXRob2QgPSBwYXJtYVsndHlwZSddID8gcGFybWFbJ3R5cGUnXS50b1VwcGVyQ2FzZSgpIDogXCJHRVRcIjtcclxuICAgICAgICBsZXQgZGF0YVR5cGUgPSBwYXJtYVsnZGF0YVR5cGUnXSA/IHBhcm1hWydkYXRhVHlwZSddLnRvVXBwZXJDYXNlKCkgOiBcIkpTT05cIjtcclxuICAgICAgICBsZXQgdXJsID0gcGFybWFbJ3VybCddID8gIHBhcm1hWyd1cmwnXSA6ICcnO1xyXG4gICAgICAgIGxldCBkYXRhID0gcGFybWFbJ2RhdGEnXSA/ICBwYXJtYVsnZGF0YSddIDoge307XHJcbiAgICAgICAgbGV0IGFzeW5jICA9IHBhcm1hWydhc3luYyddID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIGxldCBzdWNjZXNzRnVuYyA9IHBhcm1hWydzdWNjZXNzJ10gJiYgKHR5cGVvZiBwYXJtYVsnc3VjY2VzcyddID09ICdmdW5jdGlvbicpID8gIHBhcm1hWydzdWNjZXNzJ10gOiB0aGlzLnN1Y2Nlc3NGdW5jO1xyXG4gICAgICAgIGxldCBlcnJvckZ1bmMgPSBwYXJtYVsnZXJyb3InXSAmJiAodHlwZW9mIHBhcm1hWydlcnJvciddID09ICdmdW5jdGlvbicpID8gIHBhcm1hWydlcnJvciddIDogdGhpcy5lcnJvckZ1bmM7XHJcbiAgICAgICAgbGV0IGNvbXBpbGVGdW5jID0gcGFybWFbJ2NvbXBpbGUnXSAmJiAodHlwZW9mIHBhcm1hWydjb21waWxlJ10gPT0gJ2Z1bmN0aW9uJykgPyAgcGFybWFbJ2NvbXBpbGUnXSA6IHRoaXMuY29tcGlsZUZ1bmM7XHJcbiAgICAgICAgcGFybWFbJ2hlYWRlciddID0gcGFybWFbJ2hlYWRlciddICYmIEFycmF5LmlzQXJyYXkocGFybWFbJ2hlYWRlciddKSA/IHBhcm1hWydoZWFkZXInXSA6IFtdO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICBmb3IgKGxldCBpbmRleCBpbiBwYXJtYVsnaGVhZGVyJ10pIHtcclxuICAgICAgICAgICAgaWYgKHR5cGVvZiBwYXJtYVsnaGVhZGVyJ11baW5kZXhdICE9PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiaGVhZGVyIGFyciBpdGVtIHZhbHVlIGZvcm1hdCBub3QgcmlnaHQsaXRgcyBtdXN0IG9iamVjdCFcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGlmICghcGFybWFbJ2hlYWRlciddW2luZGV4XS5oYXNPd25Qcm9wZXJ0eSgna2V5JykpIHtcclxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcImhlYWRlciBhcnIgaXRlbSB2YWx1ZSBmb3JtYXQgbm90IHJpZ2h0LGl0YHMgbXVzdCBvYmplY3Qga2V5IGtleSFcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGlmICghcGFybWFbJ2hlYWRlciddW2luZGV4XS5oYXNPd25Qcm9wZXJ0eSgndmFsdWUnKSkge1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiaGVhZGVyIGFyciBpdGVtIHZhbHVlIGZvcm1hdCBub3QgcmlnaHQsaXRgcyBtdXN0IG9iamVjdCBrZXkgdmFsdWUhXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLnNldEhlYWRlcihwYXJtYVsnaGVhZGVyJ11baW5kZXhdWydrZXknXSwgcGFybWFbJ2hlYWRlciddW2luZGV4XVsndmFsdWUnXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIFxyXG4gICAgICAgIGlmKHdpbmRvdy5YTUxIdHRwUmVxdWVzdCkge1xyXG4gICAgICAgICAgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHhociA9IG5ldyBBY3RpdmVYT2JqZWN0KCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZihtZXRob2QgPT0gJ0dFVCcgJiYgZGF0YSkge1xyXG4gICAgICAgICAgICB1cmwgPSB1cmwgKyAnPyc7XHJcbiAgICAgICAgICAgIGlmICh0eXBlb2YgZGF0YSA9PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgaSBpbiBkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsICs9IGkgK1wiPVwiICsgZGF0YVtpXSArIFwiJlwiO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdXJsID0gdXJsICsgZGF0YTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICBmb3IgKGxldCBiZWZvcmVDYWxsYmFja0luZGV4IGluIHRoaXMuX19iZWZvcmVSZXF1ZXN0Q2FsbGJhY2spIHtcclxuICAgICAgICAgICAgbGV0IGRhdGFPYmogPSB0aGlzLl9fYmVmb3JlUmVxdWVzdENhbGxiYWNrW2JlZm9yZUNhbGxiYWNrSW5kZXhdKGRhdGEpO1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIGRhdGFPYmogIT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJiZWZvcmVSZXF1ZXN0Q2FsbGJhY2sgcmV0dXJuIHZhbHVlIGZvcm1hdCBub3QgcmlnaHQsaXRgcyBtdXN0IG9iamVjdCFcIik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICghZGF0YU9iai5oYXNPd25Qcm9wZXJ0eSgnc3RhdGUnKSkge1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiYmVmb3JlUmVxdWVzdENhbGxiYWNrIHJldHVybiB2YWx1ZSBmb3JtYXQgbm90IHJpZ2h0LGl0YHMgbXVzdCBvYmplY3Qga2V5IHN0YXRlIVwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgaWYgKCFkYXRhT2JqLmhhc093blByb3BlcnR5KCdkYXRhJykpIHtcclxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcImJlZm9yZVJlcXVlc3RDYWxsYmFjayByZXR1cm4gdmFsdWUgZm9ybWF0IG5vdCByaWdodCxpdGBzIG11c3Qgb2JqZWN0IGtleSBkYXRhIVwiKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGRhdGFPYmpbJ3N0YXRlJ10gPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGRhdGEgPSBkYXRhT2JqWydkYXRhJ107XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB4aHIub3BlbihtZXRob2QsIHVybCwgYXN5bmMpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGZvciAobGV0IGluZGV4IGluIHRoaXMuX19oZWFkZXJQYXJhbXMpIHtcclxuICAgICAgICAgICAgeGhyLnNldFJlcXVlc3RIZWFkZXIoaW5kZXgsIHRoaXMuX19oZWFkZXJQYXJhbXNbaW5kZXhdKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgaWYobWV0aG9kID09ICdHRVQnKSB7XHJcbiAgICAgICAgICB4aHIuc2VuZChudWxsKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgbGV0IGRhdGFzID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgICAgICBmb3IgKGxldCBpIGluIGRhdGEpIHtcclxuICAgICAgICAgICAgZGF0YXMuYXBwZW5kKGksIGRhdGFbaV0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgeGhyLnNlbmQoZGF0YXMpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGFzeW5jKSB7ICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHhoci5vbnJlYWR5c3RhdGVjaGFuZ2U9ZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgIGlmKHhoci5yZWFkeVN0YXRlPT00KXtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBhZnRlckNhbGxiYWNrSW5kZXggaW4gdGhpcy5fX2FmdGVyUmVxdWVzdENhbGxiYWNrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBkYXRhT2JqID0gdGhpcy5fX2FmdGVyUmVxdWVzdENhbGxiYWNrW2FmdGVyQ2FsbGJhY2tJbmRleF0oeGhyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBkYXRhT2JqICE9PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiYWZ0ZXJSZXF1ZXN0Q2FsbGJhY2sgcmV0dXJuIHZhbHVlIGZvcm1hdCBub3QgcmlnaHQsaXRgcyBtdXN0IG9iamVjdCFcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghZGF0YU9iai5oYXNPd25Qcm9wZXJ0eSgnc3RhdGUnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiYWZ0ZXJSZXF1ZXN0Q2FsbGJhY2sgcmV0dXJuIHZhbHVlIGZvcm1hdCBub3QgcmlnaHQsaXRgcyBtdXN0IG9iamVjdCBrZXkgc3RhdGUhXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWRhdGFPYmouaGFzT3duUHJvcGVydHkoJ2RhdGEnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiYWZ0ZXJSZXF1ZXN0Q2FsbGJhY2sgcmV0dXJuIHZhbHVlIGZvcm1hdCBub3QgcmlnaHQsaXRgcyBtdXN0IG9iamVjdCBrZXkgZGF0YSFcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhT2JqWydzdGF0ZSddID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB4aHIgPSBkYXRhT2JqWydkYXRhJ107XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZih4aHIuc3RhdHVzPT0yMDApe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXRoaXMuZGF0YVR5cGVGdW5jdGlvbkxpc3QuaGFzT3duUHJvcGVydHkoZGF0YVR5cGUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJkYXRhVHlwZSBkb25gdCBleGlzdGVudDpcIiArIGRhdGFUeXBlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgc3VjY2Vzc0Z1bmModGhpcy5kYXRhVHlwZUZ1bmN0aW9uTGlzdFtkYXRhVHlwZV0oeGhyLnJlc3BvbnNlVGV4dCksIHhocik7XHJcbiAgICAgICAgICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yRnVuYyhuZXcgRXJyb3IoeGhyLnN0YXR1cyArIFwiOlwiICsgeGhyLnN0YXR1c1RleHQpLHhocik7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBjb21waWxlRnVuYyh4aHIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYoeGhyLnJlYWR5U3RhdGU9PTQpe1xyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgYWZ0ZXJDYWxsYmFja0luZGV4IGluIHRoaXMuX19hZnRlclJlcXVlc3RDYWxsYmFjaykge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkYXRhT2JqID0gdGhpcy5fX2FmdGVyUmVxdWVzdENhbGxiYWNrW2FmdGVyQ2FsbGJhY2tJbmRleF0oeGhyKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGRhdGFPYmogIT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcImFmdGVyUmVxdWVzdENhbGxiYWNrIHJldHVybiB2YWx1ZSBmb3JtYXQgbm90IHJpZ2h0LGl0YHMgbXVzdCBvYmplY3QhXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFkYXRhT2JqLmhhc093blByb3BlcnR5KCdzdGF0ZScpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcImFmdGVyUmVxdWVzdENhbGxiYWNrIHJldHVybiB2YWx1ZSBmb3JtYXQgbm90IHJpZ2h0LGl0YHMgbXVzdCBvYmplY3Qga2V5IHN0YXRlIVwiKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFkYXRhT2JqLmhhc093blByb3BlcnR5KCdkYXRhJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiYWZ0ZXJSZXF1ZXN0Q2FsbGJhY2sgcmV0dXJuIHZhbHVlIGZvcm1hdCBub3QgcmlnaHQsaXRgcyBtdXN0IG9iamVjdCBrZXkgZGF0YSFcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YU9ialsnc3RhdGUnXSA9PT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgeGhyID0gZGF0YU9ialsnZGF0YSddO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmKHhoci5zdGF0dXM9PTIwMCl7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCF0aGlzLmRhdGFUeXBlRnVuY3Rpb25MaXN0Lmhhc093blByb3BlcnR5KGRhdGFUeXBlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJkYXRhVHlwZSBkb25gdCBleGlzdGVudDpcIiArIGRhdGFUeXBlKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3NGdW5jKHRoaXMuZGF0YVR5cGVGdW5jdGlvbkxpc3RbZGF0YVR5cGVdKHhoci5yZXNwb25zZVRleHQpLHhocik7XHJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICBlcnJvckZ1bmMobmV3IEVycm9yKHhoci5zdGF0dXMgKyBcIjpcIiArIHhoci5zdGF0dXNUZXh0KSx4aHIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGNvbXBpbGVGdW5jKHhocik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTY2FyZWNyb3dSZXF1ZXN0LmZuLmluaXQucHJvdG90eXBlID0gU2NhcmVjcm93UmVxdWVzdC5mbjtcclxuICAgIHJldHVybiBTY2FyZWNyb3dSZXF1ZXN0O1xyXG59KSh3aW5kb3cpO1xyXG4vL+aMguWcqOWFqOWxgFxyXG5tb2R1bGUuZXhwb3J0cyA9IFNjYXJlY3Jvd1JlcXVlc3Q7Il0sInNvdXJjZVJvb3QiOiIifQ==