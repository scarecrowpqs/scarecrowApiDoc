(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["scarecrowCache"] = factory();
	else
		root["scarecrowCache"] = factory();
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/scarecrowCache/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/scarecrowCache/index.js":
/*!*************************************!*\
  !*** ./src/scarecrowCache/index.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var ScarecrowCache = (function(window) {
    var ScarecrowCache = function(params) {
        return  new ScarecrowCache.fn.init(params);
    };

    ScarecrowCache.fn = ScarecrowCache.prototype = {
        constructor: ScarecrowCache,
        //初始化
        init: function(parma = {}) {
            //不共享的方法再此添加，即每个类都会实例化自己的这些方法
        },
        //作者
        author: "Scarecrow",
        //缓存数据
        __cacheData:{},
        //过期默认时间
        __cacheExpireTime:600,
        //缓存KEY
        __cacheKey:"__SCARECROW_JS_CACHE__",
        //获取当前时间戳
        __getTime:function() {
            return parseInt(Date.parse(new Date()) / 1000);
        },
        //设置缓存底层数据
        __setData:function() {
            let nowTime = this.__getTime();
            for (let i in this.__cacheData) {
                if (this.__cacheData[i].expire < nowTime && this.__cacheData[i].expire != 0) {
                    delete this.__cacheData[i];
                }
            }

            let toData = JSON.stringify(this.__cacheData);
            window.localStorage.setItem(this.__cacheKey, toData);
        },
        //获取数据
        __getData:function() {
            let nowTime = this.__getTime();
            let toData = window.localStorage.getItem(this.__cacheKey);
            if (toData === null) {
                this.__cacheData = {};
                return {};
            }

            let data = JSON.parse(toData);
            for (let i in data) {
                if (data[i].expire < nowTime && data.expire != 0) {
                    delete data[i];
                }
            }

            this.__cacheData = data;
            return this.__cacheData;
        },
    };

    //获取KEY
    ScarecrowCache.fn.getKey = function (key, defaultValue="") {
        this.__getData();
        if (this.__cacheData.hasOwnProperty(key)) {
            let obj = this.__cacheData[key];
            if (obj.hasOwnProperty('data')) {
                return obj['data'];
            } else {
                return defaultValue;
            }
        }

        return defaultValue;
    },

    //删除某个KEY
    ScarecrowCache.fn.deleteKey = function (key) {
        if (this.__cacheData.hasOwnProperty(key)) {
            delete this.__cacheData[key];
        }
    },
        //设置KEY
    ScarecrowCache.fn.setKey = function (key, value, expire=-1) {
        expire = expire < 0 ? this.__cacheExpireTime : expire;
        expire = expire > 0 ? expire + this.__getTime() : expire;
        this.__cacheData[key] = {
            expire:expire,
            data:value
        };
        this.__setData();
    },
    //清空缓存
    ScarecrowCache.fn.removeCache = function () {
        this.__cacheData = {};
        this.__setData();
    }

    ScarecrowCache.fn.init.prototype = ScarecrowCache.fn;
    return ScarecrowCache;
})(window);
//挂在全局
module.exports = ScarecrowCache;

/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9zY2FyZWNyb3dDYWNoZS93ZWJwYWNrL3VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24iLCJ3ZWJwYWNrOi8vc2NhcmVjcm93Q2FjaGUvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vc2NhcmVjcm93Q2FjaGUvLi9zcmMvc2NhcmVjcm93Q2FjaGUvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNELE87UUNWQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUM7QUFDakM7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0EsZ0MiLCJmaWxlIjoic2NhcmVjcm93Q2FjaGVfc2NhcmVjcm93LmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIHdlYnBhY2tVbml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uKHJvb3QsIGZhY3RvcnkpIHtcblx0aWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgPT09ICdvYmplY3QnKVxuXHRcdG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeSgpO1xuXHRlbHNlIGlmKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZClcblx0XHRkZWZpbmUoW10sIGZhY3RvcnkpO1xuXHRlbHNlIGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0Jylcblx0XHRleHBvcnRzW1wic2NhcmVjcm93Q2FjaGVcIl0gPSBmYWN0b3J5KCk7XG5cdGVsc2Vcblx0XHRyb290W1wic2NhcmVjcm93Q2FjaGVcIl0gPSBmYWN0b3J5KCk7XG59KSh3aW5kb3csIGZ1bmN0aW9uKCkge1xucmV0dXJuICIsIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL3NjYXJlY3Jvd0NhY2hlL2luZGV4LmpzXCIpO1xuIiwidmFyIFNjYXJlY3Jvd0NhY2hlID0gKGZ1bmN0aW9uKHdpbmRvdykge1xyXG4gICAgdmFyIFNjYXJlY3Jvd0NhY2hlID0gZnVuY3Rpb24ocGFyYW1zKSB7XHJcbiAgICAgICAgcmV0dXJuICBuZXcgU2NhcmVjcm93Q2FjaGUuZm4uaW5pdChwYXJhbXMpO1xyXG4gICAgfTtcclxuXHJcbiAgICBTY2FyZWNyb3dDYWNoZS5mbiA9IFNjYXJlY3Jvd0NhY2hlLnByb3RvdHlwZSA9IHtcclxuICAgICAgICBjb25zdHJ1Y3RvcjogU2NhcmVjcm93Q2FjaGUsXHJcbiAgICAgICAgLy/liJ3lp4vljJZcclxuICAgICAgICBpbml0OiBmdW5jdGlvbihwYXJtYSA9IHt9KSB7XHJcbiAgICAgICAgICAgIC8v5LiN5YWx5Lqr55qE5pa55rOV5YaN5q2k5re75Yqg77yM5Y2z5q+P5Liq57G76YO95Lya5a6e5L6L5YyW6Ieq5bex55qE6L+Z5Lqb5pa55rOVXHJcbiAgICAgICAgfSxcclxuICAgICAgICAvL+S9nOiAhVxyXG4gICAgICAgIGF1dGhvcjogXCJTY2FyZWNyb3dcIixcclxuICAgICAgICAvL+e8k+WtmOaVsOaNrlxyXG4gICAgICAgIF9fY2FjaGVEYXRhOnt9LFxyXG4gICAgICAgIC8v6L+H5pyf6buY6K6k5pe26Ze0XHJcbiAgICAgICAgX19jYWNoZUV4cGlyZVRpbWU6NjAwLFxyXG4gICAgICAgIC8v57yT5a2YS0VZXHJcbiAgICAgICAgX19jYWNoZUtleTpcIl9fU0NBUkVDUk9XX0pTX0NBQ0hFX19cIixcclxuICAgICAgICAvL+iOt+WPluW9k+WJjeaXtumXtOaIs1xyXG4gICAgICAgIF9fZ2V0VGltZTpmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHBhcnNlSW50KERhdGUucGFyc2UobmV3IERhdGUoKSkgLyAxMDAwKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIC8v6K6+572u57yT5a2Y5bqV5bGC5pWw5o2uXHJcbiAgICAgICAgX19zZXREYXRhOmZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICBsZXQgbm93VGltZSA9IHRoaXMuX19nZXRUaW1lKCk7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgaW4gdGhpcy5fX2NhY2hlRGF0YSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuX19jYWNoZURhdGFbaV0uZXhwaXJlIDwgbm93VGltZSAmJiB0aGlzLl9fY2FjaGVEYXRhW2ldLmV4cGlyZSAhPSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVsZXRlIHRoaXMuX19jYWNoZURhdGFbaV07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxldCB0b0RhdGEgPSBKU09OLnN0cmluZ2lmeSh0aGlzLl9fY2FjaGVEYXRhKTtcclxuICAgICAgICAgICAgd2luZG93LmxvY2FsU3RvcmFnZS5zZXRJdGVtKHRoaXMuX19jYWNoZUtleSwgdG9EYXRhKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIC8v6I635Y+W5pWw5o2uXHJcbiAgICAgICAgX19nZXREYXRhOmZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICBsZXQgbm93VGltZSA9IHRoaXMuX19nZXRUaW1lKCk7XHJcbiAgICAgICAgICAgIGxldCB0b0RhdGEgPSB3aW5kb3cubG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy5fX2NhY2hlS2V5KTtcclxuICAgICAgICAgICAgaWYgKHRvRGF0YSA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fX2NhY2hlRGF0YSA9IHt9O1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHt9O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBsZXQgZGF0YSA9IEpTT04ucGFyc2UodG9EYXRhKTtcclxuICAgICAgICAgICAgZm9yIChsZXQgaSBpbiBkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YVtpXS5leHBpcmUgPCBub3dUaW1lICYmIGRhdGEuZXhwaXJlICE9IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBkZWxldGUgZGF0YVtpXTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5fX2NhY2hlRGF0YSA9IGRhdGE7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9fY2FjaGVEYXRhO1xyXG4gICAgICAgIH0sXHJcbiAgICB9O1xyXG5cclxuICAgIC8v6I635Y+WS0VZXHJcbiAgICBTY2FyZWNyb3dDYWNoZS5mbi5nZXRLZXkgPSBmdW5jdGlvbiAoa2V5LCBkZWZhdWx0VmFsdWU9XCJcIikge1xyXG4gICAgICAgIHRoaXMuX19nZXREYXRhKCk7XHJcbiAgICAgICAgaWYgKHRoaXMuX19jYWNoZURhdGEuaGFzT3duUHJvcGVydHkoa2V5KSkge1xyXG4gICAgICAgICAgICBsZXQgb2JqID0gdGhpcy5fX2NhY2hlRGF0YVtrZXldO1xyXG4gICAgICAgICAgICBpZiAob2JqLmhhc093blByb3BlcnR5KCdkYXRhJykpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBvYmpbJ2RhdGEnXTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkZWZhdWx0VmFsdWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBkZWZhdWx0VmFsdWU7XHJcbiAgICB9LFxyXG5cclxuICAgIC8v5Yig6Zmk5p+Q5LiqS0VZXHJcbiAgICBTY2FyZWNyb3dDYWNoZS5mbi5kZWxldGVLZXkgPSBmdW5jdGlvbiAoa2V5KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX19jYWNoZURhdGEuaGFzT3duUHJvcGVydHkoa2V5KSkge1xyXG4gICAgICAgICAgICBkZWxldGUgdGhpcy5fX2NhY2hlRGF0YVtrZXldO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICAgICAgLy/orr7nva5LRVlcclxuICAgIFNjYXJlY3Jvd0NhY2hlLmZuLnNldEtleSA9IGZ1bmN0aW9uIChrZXksIHZhbHVlLCBleHBpcmU9LTEpIHtcclxuICAgICAgICBleHBpcmUgPSBleHBpcmUgPCAwID8gdGhpcy5fX2NhY2hlRXhwaXJlVGltZSA6IGV4cGlyZTtcclxuICAgICAgICBleHBpcmUgPSBleHBpcmUgPiAwID8gZXhwaXJlICsgdGhpcy5fX2dldFRpbWUoKSA6IGV4cGlyZTtcclxuICAgICAgICB0aGlzLl9fY2FjaGVEYXRhW2tleV0gPSB7XHJcbiAgICAgICAgICAgIGV4cGlyZTpleHBpcmUsXHJcbiAgICAgICAgICAgIGRhdGE6dmFsdWVcclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuX19zZXREYXRhKCk7XHJcbiAgICB9LFxyXG4gICAgLy/muIXnqbrnvJPlrZhcclxuICAgIFNjYXJlY3Jvd0NhY2hlLmZuLnJlbW92ZUNhY2hlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMuX19jYWNoZURhdGEgPSB7fTtcclxuICAgICAgICB0aGlzLl9fc2V0RGF0YSgpO1xyXG4gICAgfVxyXG5cclxuICAgIFNjYXJlY3Jvd0NhY2hlLmZuLmluaXQucHJvdG90eXBlID0gU2NhcmVjcm93Q2FjaGUuZm47XHJcbiAgICByZXR1cm4gU2NhcmVjcm93Q2FjaGU7XHJcbn0pKHdpbmRvdyk7XHJcbi8v5oyC5Zyo5YWo5bGAXHJcbm1vZHVsZS5leHBvcnRzID0gU2NhcmVjcm93Q2FjaGU7Il0sInNvdXJjZVJvb3QiOiIifQ==