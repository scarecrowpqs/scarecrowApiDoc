layui.use(['layer','form','element'], function(){
    var layer = layui.layer;
    var form = layui.form;
    var element = layui.element;

    function resetPwdForm() {
        $("#newPwd").val('');
        $("#snewPwd").val('');
    }

    function resetEmailForm() {
        $("#email").val('');
    }

    //检查是否有特殊格式
    function checks(newName){
        var regEn = /[`!@#$%^&*()_+<>?:"{},\/;'[\]]/im,
            regCn = /[·！#￥（——）：；“”‘、，|《。》？、【】[\]]/im;
        if(regEn.test(newName) || regCn.test(newName)) {
            return true;
        }
        return false;
    }

    //检测邮箱格式
    function checkEmail(myemail){
        var myReg=/^[a-zA-Z0-9_-]+@([a-zA-Z0-9]+\.)+(com|cn|net|org)$/;
        if(myReg.test(myemail)){
            return true;
        }else{
            return false;
        }
    }


    $("#updatePwdBtn").click(function () {
        var newPwd = $("#newPwd").val();
        var snewPwd = $("#snewPwd").val();

        if (newPwd == "") {
            layer.msg("新密码不能为空");
            return ;
        }

        if (newPwd != snewPwd) {
            layer.msg("两次密码输入不一致");
            return ;
        }

        if (newPwd.length < 6) {
            layer.msg("密码不能小于6位");
            return ;
        }

        if (checks(newPwd)) {
            layer.msg("密码中不能有特殊字符");
            return ;
        }

        var data = {
            userId:parent.CHANGE_USER_STATE_ID,
            password:newPwd,
        };

        var index = layer.load(1);
        HttpRequest.request({
            url:getApiHttpUrl('api/user/updateuserpwd'),
            type:"POST",
            dataType:"json",
            data:data,
            success:function (datas) {
                layer.close(index);
                layer.msg(datas.info);
                resetPwdForm();
            },
            error:function () {
                layer.close(index);
                layer.msg("服务器开小差了~~~~");
            }
        });
    });
});
