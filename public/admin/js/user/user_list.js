layui.use(['element','form', 'layer', 'laypage'], function(){
    element = layui.element;
    form = layui.form;
    layer = layui.layer;
    laypage = layui.laypage;

    //绑定客户列表搜索按钮
    $("#userSearchBtn").click(function () {
        var searchData = $("#userSearch").val();
        HttpRequest.request({
            url:getApiHttpUrl('api/user/getuserlist'),
            type:"POST",
            cache:false,
            data:{
                searchContent:searchData
            },
            dataType:"json",
            success:function (datas) {
                if (datas.status == "YES") {
                    var data = datas.data;
                    XuanRanPaging('paging', data.total, data.limit);
                } else {
                    layer.msg(datas.info);
                }
            },
            error:function (e) {
                layer.msg("服务器开小差了~~~");
            }
        });
    });

    //渲染分页
    function XuanRanPaging(elem, count, limit) {
        laypage.render({
            elem: elem //注意，这里的 test1 是 ID，不用加 # 号
            ,count: count //数据总数，从服务端得到
            ,limit: limit
            ,jump: function(obj){
                var userSearch = $("#userSearch").val();
                var data = {
                    page:obj.curr,
                    limit:obj.limit,
                    searchContent:userSearch,
                };
                var loadIndex = layer.load(1);
                HttpRequest.request({
                    url:getApiHttpUrl('api/user/getuserlist'),
                    type:"POST",
                    cache:false,
                    data:data,
                    dataType:"json",
                    success:function (datas) {
                        layer.close(loadIndex);
                        if (datas.status == "YES") {
                            var data = datas.data;
                            XuanRanTable(data.list);
                        } else {
                            layer.msg(datas.info);
                        }

                    },
                    error:function (e) {
                        layer.close(loadIndex);
                        layer.msg("服务器开小差了~~~1");
                    }
                });
            }
        });
    }

    //渲染表格
    function XuanRanTable(data) {
        var str = "";
        for (i in data) {
            str += "<tr>";
            str += "<td>"+ data[i]['id'] +"</td>";
            str += "<td>"+ data[i]['username'] +"</td>";
            str += "<td>"+ data[i]['nike_name'] +"</td>";
            str += "<td><a href='javascript:void(0);updateUserPwd(\""+ data[i]['id'] +"\")' class='layui-btn layui-btn-sm'>修改密码</a><a href='javascript:void(0);changeUserState(\""+ data[i]['id'] +"\")' class='layui-btn layui-btn-sm layui-btn-danger'>"+ (data[i]['state'] == 1 ? "停用" : "启用") +"</a><a href='javascript:void(0);deleteUser(\""+ data[i]['id'] +"\")' class='layui-btn layui-btn-sm layui-btn-danger'>删除</a></td>";
            str += "</tr>";
        }

        $("#tableContent").empty().html(str);
    }

    //验证是否是手机号码
    function checkMobile(str) {
        var re = /^1\d{10}$/
        if (re.test(str)) {
            return true;
        } else {
            return false;
        }
    }

    function resetForm() {
        $("#name").val('');
        $("#idc").val('');
        $("#phone").val('');
    }

    $("#userSearchBtn").click();

    $("#addUserBtn").click(function () {
        layer.open({
            type: 2,
            title: "创建用户",
            area: ['1080px', '400px'],
            content: getHttpUrl('page/user/create_user.html'),
            end:function () {
                $("#userSearchBtn").click();
            }
        });
    });
});



/**
 * 修改用户状态
 */
function changeUserState(id) {
    HttpRequest.post(getApiHttpUrl('api/user/stoporstartuser'), {userId:id}, function (data) {
        if (data.status == "YES") {
            layer.msg(data.info, function () {
                window.location.reload();
            });
        } else {
            layer.msg(data.info);
        }
    }, function (e) {
        layer.msg("服务器开小差了~~~");
    });
}

/**
 * 删除用户
 */
function deleteUser(id) {
    HttpRequest.post(getApiHttpUrl('api/user/deleteuser'), {userId:id}, function (data) {
        if (data.status == "YES") {
            layer.msg(data.info, function () {
                window.location.reload();
            });
        } else {
            layer.msg(data.info);
        }
    }, function (e) {
        layer.msg("服务器开小差了~~~");
    });
}

CHANGE_USER_STATE_ID=0
/**
 * 显示修改账户密码页面
 * @param id
 * @param state
 */
function updateUserPwd(id) {
    CHANGE_USER_STATE_ID = id;
    layer.open({
        title:'修改账户密码',
        type: 2,
        content: getHttpUrl('page/user/update_user_info.html'),
        area: ['800px', '400px']
    });
}