layui.use(['layer','form'], function(){
    layer = layui.layer;
    form = layui.form;

    function resetForm() {
        $("#userName").val('');
        $("#nikeName").val('');
        $("#password").val('');
    }

    $("#createUserBtn").click(function () {
        var userName = $("#userName").val();
        var nikeName = $("#nikeName").val();
        var password = $("#password").val();

        if (userName == "") {
            layer.msg("账号不能为空");
            return ;
        }

        if (password == "") {
            layer.msg("密码不能为空");
            return ;
        }

        var data = {
            userName:userName,
            nikeName:nikeName,
            password:password
        };

        var index = layer.load(1);
        HttpRequest.request({
            url:getApiHttpUrl('api/user/createuser'),
            type:"POST",
            dataType:"json",
            data:data,
            success:function (datas) {
                layer.close(index);
                layer.msg(datas.info);
                resetForm();
            },
            error:function () {
                layer.close(index);
                layer.msg("服务器开小差了~~~~");
            }
        });
    });
});

//限制只能输入数字和小数点
function inputNum(obj) {
    $(obj).val($(obj).val().replace(/[^0-9.]/g,''));
}

//限制名字格式
function inputName(obj) {
    $(obj).val($(obj).val().replace(/[^\u4e00-\u9fa5A-Za-z]+/g,'').replace(/{}【】，。、？）（*&……%￥#@！~\| “：《》/g,''));
}

//只能输入数字和字母
function inputNumChar(obj) {
    $(obj).val($(obj).val().replace(/[^\a-\z\A-\Z0-9.]/g,''))
}
