//http 全局请求地址
var httpBaseUrl = '/admin/';

//API请求地址
var httpApiBaseUrl = '/';

//前端资源访问路径
function getHttpUrl(uri) {
    return httpBaseUrl + ScarecrowTool.trim(uri, '/');
}

//获取API请求地址
function getApiHttpUrl(uri) {
    return httpApiBaseUrl + ScarecrowTool.trim(uri, '/');
}