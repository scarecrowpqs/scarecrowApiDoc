layui.use(['form','layer'], function() {
    var form = layui.form;
    var layer = layui.layer;

    $("#createProjectBtn").click(function () {
        HttpRequest.request({
            url:getApiHttpUrl('api/projectmanage/createproject'),
            type:"POST",
            dataType:"JSON",
            data:{
                projectName:$("#projectName").val()
            },
            success:function (datas) {
                if (datas.status == "YES") {
                    layer.msg(datas.info, {anim:1}, () =>{
                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                        parent.document.getElementById('searchProjectBtn').click();
                        parent.layer.close(index);
                    });
                } else {
                    layer.msg(datas.info);
                }
            },
            error:function () {
                layer.msg("服务器错误，请稍后再试~");
            }
        });
    });
});

//限制名字格式
function inputName(obj) {
    $(obj).val($(obj).val().replace(/[^\u4e00-\u9fa5A-Za-z]+/g,'').replace(/{}【】，。、？）（*&……%￥#@！~\| “：《》/g,''));
}
