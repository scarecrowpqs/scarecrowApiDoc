layui.use(['form','layer'], function() {
    var form = layui.form;
    var layer = layui.layer;

    //获取所有项目并渲染
    function xrAllProjectList() {
        let url = getApiHttpUrl('api/projectmanage/getallcaneditproject');
        HttpRequest.request({
            url:url,
            type:'POST',
            dataType:'JSON',
            data:{},
            success:function (datas) {
                if (datas.status == "YES") {
                    xrProjectListForm(datas.data);
                } else {
                    layer.msg(datas.info);
                }
            },
            error:function (e) {
                layer.msg("服务器开小差了~");
            }
        });
    }

    //渲染项目列表
    function xrProjectListForm(data) {
        let htmlStr = "";
        for (let i in data) {
            htmlStr += '<option value="'+ data[i]['id'] +'">'+ data[i]['name'] +'</option>';
        }
        $("#allProjectList").append(htmlStr);
        form.render('select');
    }

    form.on('select(allProjectList)', function (data) {
        xrAllCategoryList();
    });


    //获取所有项目列表并渲染
    function xrAllCategoryList() {
        let url = getApiHttpUrl('api/projectmanage/getallcategory');
        HttpRequest.request({
            url:url,
            type:'POST',
            dataType:'JSON',
            async:false,
            data:{
                projectId:$("#allProjectList").val(),
                categoryName:""
            },
            success:function (datas) {
                if (datas.status == "YES") {
                    xrCategoryTable(datas.data);
                } else {
                    layer.msg(datas.info);
                }
            },
            error:function (e) {
                layer.msg("服务器开小差了~");
            }
        });
    }

    //渲染接口列表
    function xrCategoryTable(data) {
        let htmlStr = '<option value="0">请选择分类</option>';
        for (let i in data) {
            htmlStr += '<option value="'+ data[i]['id'] +'">'+ data[i]['name'] +'</option>';
        }
        $("#allCategoryList").empty().append(htmlStr);
        form.render('select');
    }

    $(".detail_add").on('click',function () {
        var html = '';
        html += '<tr>';
        html += '    <td><input type="text" autocomplete="off" placeholder="参数" class="layui-input paramName" value=""></td>';
        html += '    <td>';
        html += '       <select class="select_class paramType">';
        html += '           <option value="string">string</option>';
        html += '           <option value="int">int</option>';
        html += '           <option value="file">file</option>';
        html += '       </select>';
        html += '    </td>';
        html += '    <td>';
        html += '       <select class="select_class paramMust">';
        html += '           <option value="1">是</option>';
        html += '           <option value="0">否</option>';
        html += '       </select>';
        html += '    </td>';
        html += '    <td><input type="text" autocomplete="off" placeholder="参数说明" class="layui-input paramDesc"></td>';
        html += '    <td><button type="button" class="layui-btn layui-btn-danger detail_del  layui-btn-sm">删除</button></td>';
        html += '</tr>';
        $(".param_tr").append(html);
        form.render('select');
    });

    $(".param_tr").on('click','.detail_del',function () {
        $(this).parent().parent().remove();
    });

    $("#submitBtn").click(function () {
        let projectId = $("#allProjectList").val();
        let categoryId = $("#allCategoryList").val();
        if (projectId * categoryId == 0) {
            layer.msg("项目或分类必须选择!");
            return false;
        }

        let data = getFormData();
        if (data.title == "") {
            layer.msg("接口名称不能为空");
            return false;
        }

        if (data.url == "") {
            layer.msg("接口地址不能为空");
            return false;
        }

        HttpRequest.request({
            url:getApiHttpUrl('api/apidoc/createapi'),
            type:"POST",
            dataType:"JSON",
            data:data,
            success:function (datas) {
                layer.msg(datas.info);
                if (datas.status == "YES") {
                    resetForm();
                }
            },
            error:function () {
                layer.msg("服务器错误");
            }
        });
        //调用创建接口
    });

    function resetForm() {
        $("#title").val('');
        $("#url").val('');
        $("#result").html('');
        $("#result").val('');
        $(".param_tr").empty();
    }

    /**
     * 获取表单数据
     * @returns {{result: *, paramList: *, request_method: *, title: *, projectId: *, categoryId: *, url: *}}
     */
    function getFormData() {
        let projectId = $("#allProjectList").val();
        let categoryId = $("#allCategoryList").val();
        let title = $("#title").val();
        let url = $("#url").val();
        let request_method = $("#request_method").val();
        let result = $("#result").val();
        let paramList = [];
        $(".param_tr > tr").each(function (index, trObjs) {
            let trObj = $(trObjs);
            let paramName = trObj.find('.paramName').val();
            let paramType = trObj.find('.paramType').val();
            let paramMust = trObj.find('.paramMust').val();
            let paramDesc = trObj.find('.paramDesc').val();
            let data = {
                paramName:paramName,
                paramType:paramType,
                paramMust:paramMust,
                paramDesc:paramDesc
            };
            paramList.push(data);
        });

        let relData = {
            projectId:projectId,
            categoryId:categoryId,
            title:title,
            url:url,
            requestMethod:request_method,
            result:result,
            paramList:JSON.stringify(paramList)
        };

        return relData;
    }

    (()=>{
        xrAllProjectList();
    })();
});