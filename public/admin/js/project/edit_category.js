layui.use(['form','layer'], function() {
    var form = layui.form;
    var layer = layui.layer;

    function initCategory() {
        HttpRequest.request({
            url:getApiHttpUrl('api/projectmanage/getcategoryinfo'),
            type:'POST',
            dataType:'JSON',
            data:{
                projectId:parent.EDIT_NOW_PROJECT_ID,
                categoryId:parent.EDIT_NOW_CATEGORY_ID
            },
            success:function (datas) {
                if (datas.status == "YES") {
                    $("#categoryName").val(datas.data.name);
                } else {
                    layer.msg(datas.info);
                    $("#updateCategoryBtn").addClass('layui-btn-disabled');
                }
            },
            error:function (e) {
                $("#updateCategoryBtn").addClass('layui-btn-disabled');
                layer.msg("服务器开小差了~");
            }
        });
    }

    $("#updateCategoryBtn").click(function () {
        let categoryName = $("#categoryName").val();
        if (categoryName == "") {
            layer.msg("分类名称不能为空");
            return false;
        }
        HttpRequest.request({
            url:getApiHttpUrl('api/projectmanage/updatecategory'),
            type:'POST',
            dataType:'JSON',
            data:{
                categoryId:parent.EDIT_NOW_CATEGORY_ID,
                categoryName:$("#categoryName").val()
            },
            success:function (datas) {
                layer.msg(datas.info);
            },
            error:function (e) {
                layer.msg("服务器开小差了~");
            }
        });
    });
    initCategory();
});

//限制名字格式
function inputName(obj) {
    $(obj).val($(obj).val().replace(/[^\u4e00-\u9fa5A-Za-z]+/g,'').replace(/{}【】，。、？）（*&……%￥#@！~\| “：《》/g,''));
}