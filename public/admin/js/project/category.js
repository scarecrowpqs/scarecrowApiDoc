layui.use(['form', 'table'], function() {
    var form = layui.form;
    var table = layui.table


    //获取所有项目并渲染
    function xrAllProjectList() {
        let url = getApiHttpUrl('api/projectmanage/getallcaneditproject');
        HttpRequest.request({
            url:url,
            type:'POST',
            dataType:'JSON',
            data:{},
            success:function (datas) {
                if (datas.status == "YES") {
                    xrProjectListForm(datas.data);
                } else {
                    layer.msg(datas.info);
                }
            },
            error:function (e) {
                layer.msg("服务器开小差了~");
            }
        });
    }

    //渲染项目列表
    function xrProjectListForm(data) {
        let htmlStr = "";
        for (let i in data) {
            htmlStr += '<option value="'+ data[i]['id'] +'">'+ data[i]['name'] +'</option>';
        }
        $("#allProjectList").append(htmlStr);
        form.render('select');
    }

    form.on('select(allProjectList)', function (data) {
        $("#searchContent").val('');
        $("#searchCategoryBtn").click();
        form.render('select');
    });

    //获取所有分类列表并渲染
    function xrAllCategoryList() {
        let loadingIndex= layer.load(1);
        let url = getApiHttpUrl('api/projectmanage/getallcategory');
        HttpRequest.request({
            url:url,
            type:'POST',
            dataType:'JSON',
            data:{
                projectId:$("#allProjectList").val(),
                categoryName:$("#searchContent").val()
            },
            success:function (datas) {
                layer.close(loadingIndex);
                if (datas.status == "YES") {
                    xrCategoryTable(datas.data);
                } else {
                    layer.msg(datas.info);
                }
            },
            error:function (e) {
                layer.close(loadingIndex);
                layer.msg("服务器开小差了~");
            }
        });
    }

    //渲染接口列表
    function xrCategoryTable(data) {
        let htmlStr = "";
        $("#showAllCategoryTable > tbody").empty();
        for (let i in data) {
            htmlStr += '<tr>'+
                '<td>'+ data[i]['name'] +'</td>'+
                '<td>'+ data[i]['apiNum'] +'</td>'+
                '<td><a class="layui-btn layui-btn-xs" onclick="editCategory('+ data[i]['id'] +')">编辑</a><a class="layui-btn layui-btn-xs layui-btn-danger" onclick="deleteCategory('+ data[i]['id'] +')">删除</a></td>'+
                '</tr>';
        }

        $("#showAllCategoryTable > tbody").append(htmlStr);
    }

    $("#searchCategoryBtn").click(function () {
        if ($("#allProjectList").val() < 1) {
            layer.msg("请选择项目");
            return false;
        }
        xrAllCategoryList();
    });

    $("#addCategoryBtn").click(function () {
        EDIT_NOW_PROJECT_ID = $("#allProjectList").val();
        layer.open({
            title:'添加项目',
            type: 2,
            content: getHttpUrl('page/project/create_category.html'),
            area: ['500px', '200px'],
            end:function () {
                $("#searchCategoryBtn").click();
            }
        });
    });

    (()=>{
        $("#searchCategoryBtn").click();
        xrAllProjectList();
    })();
});

EDIT_NOW_PROJECT_ID=0;
EDIT_NOW_CATEGORY_ID=0;

/**
 * 编辑分类
 * @param projectId
 */
function editCategory(categoryId) {
    EDIT_NOW_PROJECT_ID = $("#allProjectList").val();
    EDIT_NOW_CATEGORY_ID = categoryId;
    layer.open({
        title:'编辑项目',
        type: 2,
        content: getHttpUrl('page/project/edit_category.html'),
        area: ['500px', '200px'],
        end: () => {
            $("#searchCategoryBtn").click();
        }
    });
}

/**
 * 删除项目
 * @param projectId
 */
function deleteCategory(categoryId) {
    let index = layer.open({
        type: 1
        ,title: false //不显示标题栏
        ,closeBtn: false
        ,area: '300px;'
        ,shade: 0.8
        ,id: categoryId
        ,btn: ['确定', '取消']
        ,btnAlign: 'c'
        ,content: '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;">你知道吗？亲！<br>你删除了该分类之后就不能再恢复了哟，请再次确认是否删除~</div>'
        ,yes: function(){
            //进行删除处理
            HttpRequest.request({
                url:getApiHttpUrl('api/projectmanage/deletecategory'),
                type:"POST",
                dataType:"JSON",
                data:{
                    categoryId:categoryId
                },
                success:function (datas) {
                    layer.close(index);
                    layer.msg(datas.info);
                },
                error:function () {
                    layer.close(index);
                    layer.msg("服务器错误，请稍后再试~");
                }
            });
        }
        ,end: () => {
            $("#searchCategoryBtn").click();
        }
    });
}