layui.use(['form', 'table'], function() {
    var form = layui.form;
    var table = layui.table;

    //获取所有项目列表并渲染
    function xrAllProjectList() {
        let loadingIndex= layer.load(1);
        let url = getApiHttpUrl('api/projectmanage/getallcaneditproject');
        let projectName = $("#searchContent").val();
        HttpRequest.request({
            url:url,
            type:'POST',
            dataType:'JSON',
            data:{
                projectName:projectName,
            },
            success:function (datas) {
                layer.close(loadingIndex);
                if (datas.status == "YES") {
                    xrProjectTable(datas.data);
                } else {
                    layer.msg(datas.info);
                }
            },
            error:function (e) {
                layer.close(loadingIndex);
                layer.msg("服务器开小差了~");
            }
        });
    }

    //渲染接口列表
    function xrProjectTable(data) {
        let htmlStr = "";
        $("#showAllApiTable > tbody").empty();
        for (let i in data) {
            htmlStr += '<tr>'+
                '<td>'+ data[i]['name'] +'</td>'+
                '<td>'+ data[i]['categoryNum'] +'</td>'+
                '<td>'+ data[i]['apiNum'] +'</td>'+
                '<td><a class="layui-btn layui-btn-xs" onclick="editProject('+ data[i]['id'] +')">编辑</a><a class="layui-btn layui-btn-xs '+ (data[i]['state'] == 1 ? "layui-btn-danger" : "") +'" onclick="changeProjectState('+ data[i]['id'] +')">'+ (data[i]['state'] == 1 ? "停用" : "启用") +'</a><a class="layui-btn layui-btn-xs layui-btn-warm" onclick="exportProject('+ data[i]['id'] +')">导出</a><a class="layui-btn layui-btn-xs layui-btn-danger" onclick="deleteProject('+ data[i]['id'] +')">删除</a></td>'+
                '</tr>';
        }

        $("#showAllApiTable > tbody").append(htmlStr);
    }

    $("#searchProjectBtn").click(function () {
        xrAllProjectList();
    });

    $("#addProjectBtn").click(function () {
        layer.open({
            title:'添加项目',
            type: 2,
            content: getHttpUrl('page/project/create_project.html'),
            area: ['500px', '200px'],
        });
    });

    

    (()=>{$("#searchProjectBtn").click()})();
});

EDIT_NOW_PROJECT_ID=0;

/**
 * 编辑项目
 * @param projectId
 */
function editProject(projectId) {
    EDIT_NOW_PROJECT_ID = projectId;
    layer.open({
        title:'编辑项目',
        type: 2,
        content: getHttpUrl('page/project/edit_project.html'),
        area: ['500px', '200px'],
        end: () => {
            $("#searchProjectBtn").click();
        }
    });
}

/**
 * 删除项目
 * @param projectId
 */
function deleteProject(projectId) {
    let index = layer.open({
        type: 1
        ,title: false //不显示标题栏
        ,closeBtn: false
        ,area: '300px;'
        ,shade: 0.8
        ,id: projectId
        ,btn: ['确定', '取消']
        ,btnAlign: 'c'
        ,content: '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;">你知道吗？亲！<br>你删除了该项目之后就不能再恢复了哟，请再次确认是否删除~</div>'
        ,yes: function(){
            //进行删除处理
            HttpRequest.request({
                url:getApiHttpUrl('api/projectmanage/deleteproject'),
                type:"POST",
                dataType:"JSON",
                data:{
                    projectId:projectId
                },
                success:function (datas) {
                    layer.close(index);
                    layer.msg(datas.info);
                },
                error:function () {
                    layer.close(index);
                    layer.msg("服务器错误，请稍后再试~");
                }
            });
        }
        ,end: () => {
            $("#searchProjectBtn").click();
        }
    });
}

/**
 * 改变项目状态
 * @param projectId
 */
function changeProjectState(projectId) {
    HttpRequest.request({
        url:getApiHttpUrl('api/projectmanage/changeprojectstate'),
        type:"POST",
        dataType:"JSON",
        data:{
            projectId:projectId
        },
        success:function (datas) {
            if (datas.status == "YES") {
                layer.msg(datas.info, {anim: 0}, () => {
                    $("#searchProjectBtn").click();
                });
            } else {
                layer.msg(datas.info);
            }

        },
        error:function () {
            layer.msg("服务器错误，请稍后再试~");
        }
    });
}

/**
 * 导出项目
 * @param projectId
 */
function exportProject(projectId) {
    let index = layer.open({
        type: 1
        ,title: false //不显示标题栏
        ,closeBtn: false
        ,area: '300px;'
        ,shade: 0.8
        ,id: projectId
        ,btn: ['确定', '取消']
        ,btnAlign: 'c'
        ,content: '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;">导出格式为 postman v2.1</div>'
        ,yes: function(){
            var loadeIndex = layer.load(1);
            HttpRequest.request({
                url:getApiHttpUrl('api/projectmanage/exportproject'),
                type:"POST",
                dataType:"TEXT",
                data:{
                    projectId:projectId
                },
                success:function (datas) {
                    data = new Blob([datas], {
                        type: 'text/plain'
                    });
                    var reader = new FileReader();
                    reader.readAsDataURL(data);
                    reader.onload = function (e) {
                        var a = document.createElement('a');
                        a.download = 'project.json';
                        a.href = e.target.result;
                        document.body.appendChild(a);
                        a.click();
                        document.body.removeChild(a);
                        layer.close(loadeIndex);
                        layer.msg("下载成功", {anim:1},function () {
                            layer.close(index);
                        })
                    }
                },
                error:function () {
                    layer.msg("服务器错误，请稍后再试~");
                }
            });

        }
        ,end: () => {
            $("#searchProjectBtn").click();
        }
    });
}