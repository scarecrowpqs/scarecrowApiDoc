layui.use(['form','layer'], function() {
    var form = layui.form;
    var layer = layui.layer;
    var apiInfo = {};
    var apiId = parent.EDIT_NOW_API_ID;

    /**
     * 初始化用户信息
     */
    function getApiInfo() {
        let url = getApiHttpUrl('api/apidoc/getapidocinfo');
        HttpRequest.request({
            url:url,
            type:'POST',
            dataType:'JSON',
            async:false,
            data:{
                apiId:apiId
            },
            success:function (datas) {
                if (datas.status == "YES") {
                    apiInfo = datas.data;
                    xrApiForm();
                    xrAllCategoryList();
                } else {
                    layer.msg(datas.info);
                }
            },
            error:function (e) {
                layer.msg("服务器开小差了~");
            }
        });
    }

    //渲染项目列表
    function xrApiForm() {
        let htmlStr = '<option value="'+ apiInfo['project_id'] +'">'+ apiInfo['projectName'] +'</option>';
        $("#allProjectList").append(htmlStr);

        $("#title").val(apiInfo['title']);
        $("#url").val(apiInfo['url']);
        $("#result").html(apiInfo['result']);
        $("#request_method option").each(function (index, item) {
            if ($(item).val() == apiInfo['request_method']) {
                $(item).prop('selected', true);
            }
        });

        let paramListData = JSON.parse(apiInfo['param']);
        var html = '';
        let tempData = {};
        for (let i in paramListData) {
            tempData = paramListData[i];
            html += '<tr>';
            html += '    <td><input type="text" autocomplete="off" placeholder="参数" class="layui-input paramName" value="'+ tempData['title'] +'"></td>';
            html += '    <td>';
            html += '       <select class="select_class paramType">';
            html += '           <option value="string" '+ (tempData['type'] == "string" ? "selected" : "") +'>string</option>';
            html += '           <option value="int" '+ (tempData['type'] == "int" ? "selected" : "") +'>int</option>';
            html += '           <option value="file" '+ (tempData['type'] == "file" ? "selected" : "") +'>file</option>';
            html += '       </select>';
            html += '    </td>';
            html += '    <td>';
            html += '       <select class="select_class paramMust">';
            html += '           <option value="1" '+ (tempData['must'] == 1 ? "selected" : "") +'>是</option>';
            html += '           <option value="0" '+ (tempData['must'] == 0 ? "selected" : "") +'>否</option>';
            html += '       </select>';
            html += '    </td>';
            html += '    <td><input type="text" autocomplete="off" placeholder="参数说明" class="layui-input paramDesc" value="'+ zy(tempData['desc']) +'"></td>';
            html += '    <td><button type="button" class="layui-btn layui-btn-danger detail_del  layui-btn-sm">删除</button></td>';
            html += '</tr>';
        }
        $(".param_tr").empty().append(html);


        form.render();
    }

    function zy(str) {
        return str.replace(/"/g, '\'');
    }

    //获取所有项目列表并渲染
    function xrAllCategoryList() {
        let url = getApiHttpUrl('api/projectmanage/getallcategory');
        HttpRequest.request({
            url:url,
            type:'POST',
            dataType:'JSON',
            async:false,
            data:{
                projectId:$("#allProjectList").val(),
                categoryName:""
            },
            success:function (datas) {
                if (datas.status == "YES") {
                    xrCategoryTable(datas.data);
                } else {
                    layer.msg(datas.info);
                }
            },
            error:function (e) {
                layer.msg("服务器开小差了~");
            }
        });
    }

    //渲染接口列表
    function xrCategoryTable(data) {
        let htmlStr = '<option value="0">请选择分类</option>';
        for (let i in data) {
            htmlStr += '<option value="'+ data[i]['id'] +'" '+ (data[i]['id'] == apiInfo['category_id'] ? 'selected' : "") +'>'+ data[i]['name'] +'</option>';
        }
        $("#allCategoryList").empty().append(htmlStr);
        form.render('select');
    }

    $(".detail_add").on('click',function () {
        var html = '';
        html += '<tr>';
        html += '    <td><input type="text" autocomplete="off" placeholder="参数" class="layui-input paramName" value=""></td>';
        html += '    <td>';
        html += '       <select class="select_class paramType">';
        html += '           <option value="string">string</option>';
        html += '           <option value="int">int</option>';
        html += '           <option value="file">file</option>';
        html += '       </select>';
        html += '    </td>';
        html += '    <td>';
        html += '       <select class="select_class paramMust">';
        html += '           <option value="1">是</option>';
        html += '           <option value="0">否</option>';
        html += '       </select>';
        html += '    </td>';
        html += '    <td><input type="text" autocomplete="off" placeholder="参数说明" class="layui-input paramDesc"></td>';
        html += '    <td><button type="button" class="layui-btn layui-btn-danger detail_del  layui-btn-sm">删除</button></td>';
        html += '</tr>';
        $(".param_tr").append(html);
        form.render('select');
    });

    $(".param_tr").on('click','.detail_del',function () {
        $(this).parent().parent().remove();
    });

    $("#submitBtn").click(function () {
        let projectId = $("#allProjectList").val();
        let categoryId = $("#allCategoryList").val();
        if (projectId * categoryId == 0) {
            layer.msg("项目或分类必须选择!");
            return false;
        }

        let data = getFormData();
        if (data.title == "") {
            layer.msg("接口名称不能为空");
            return false;
        }

        if (data.url == "") {
            layer.msg("接口地址不能为空");
            return false;
        }

        HttpRequest.request({
            url:getApiHttpUrl('api/apidoc/editapi'),
            type:"POST",
            dataType:"JSON",
            data:data,
            success:function (datas) {
                layer.msg(datas.info);
            },
            error:function () {
                layer.msg("服务器错误");
            }
        });
        //调用创建接口
    });

    /**
     * 获取表单数据
     * @returns {{result: *, paramList: *, request_method: *, title: *, projectId: *, categoryId: *, url: *}}
     */
    function getFormData() {
        let projectId = $("#allProjectList").val();
        let categoryId = $("#allCategoryList").val();
        let title = $("#title").val();
        let url = $("#url").val();
        let request_method = $("#request_method").val();
        let result = $("#result").val();
        let paramList = [];
        $(".param_tr > tr").each(function (index, trObjs) {
            let trObj = $(trObjs);
            let paramName = trObj.find('.paramName').val();
            let paramType = trObj.find('.paramType').val();
            let paramMust = trObj.find('.paramMust').val();
            let paramDesc = trObj.find('.paramDesc').val();
            let data = {
                paramName:paramName,
                paramType:paramType,
                paramMust:paramMust,
                paramDesc:paramDesc
            };
            paramList.push(data);
        });

        let relData = {
            projectId:projectId,
            categoryId:categoryId,
            apiId:apiId,
            title:title,
            url:url,
            requestMethod:request_method,
            result:result,
            paramList:JSON.stringify(paramList)
        };

        return relData;
    }

    (()=>{
        getApiInfo();
    })();
});