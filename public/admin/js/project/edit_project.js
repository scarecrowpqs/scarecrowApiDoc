layui.use(['form','layer'], function() {
    var form = layui.form;
    var layer = layui.layer;

    function initProject() {
        HttpRequest.request({
            url:getApiHttpUrl('api/projectmanage/getprojectinfo'),
            type:'POST',
            dataType:'JSON',
            data:{
                projectId:parent.EDIT_NOW_PROJECT_ID,
            },
            success:function (datas) {
                if (datas.status == "YES") {
                    $("#projectName").val(datas.data.name);
                } else {
                    layer.msg(datas.info);
                    $("#updateProjectBtn").addClass('layui-btn-disabled');
                }
            },
            error:function (e) {
                $("#updateProjectBtn").addClass('layui-btn-disabled');
                layer.msg("服务器开小差了~");
            }
        });
    }

    $("#updateProjectBtn").click(function () {
        let projectName = $("#projectName").val();
        if (projectName == "") {
            layer.msg("项目名称不能为空");
            return false;
        }
        HttpRequest.request({
            url:getApiHttpUrl('api/projectmanage/editproject'),
            type:'POST',
            dataType:'JSON',
            data:{
                projectId:parent.EDIT_NOW_PROJECT_ID,
                projectName:projectName
            },
            success:function (datas) {
                layer.msg(datas.info);
            },
            error:function (e) {
                layer.msg("服务器开小差了~");
            }
        });
    });
    initProject();
});

//限制名字格式
function inputName(obj) {
    $(obj).val($(obj).val().replace(/[^\u4e00-\u9fa5A-Za-z]+/g,'').replace(/{}【】，。、？）（*&……%￥#@！~\| “：《》/g,''));
}