HttpRequest = scarecrowRequest();

//退出登录
function logout() {
    let isCanLogout  = window.sessionStorage.getItem('SC-API-TOKEN-IS-CAN-LOGOUT');
    if (isCanLogout == 1) {
        window.sessionStorage.setItem('SC-API-TOKEN-IS-CAN-LOGOUT', 0);
        HttpRequest.post(getApiHttpUrl('login/login_out'));
    }

    HttpRequest.setHeader('SC-API-TOKEN', '');
    window.sessionStorage.removeItem('SC-API-TOKEN');
    window.sessionStorage.removeItem('SC-API-CACHE-INFO');
    window.top.location.href = getHttpUrl('login.html');
}

//设置头鉴权
function setToken(data) {
    let token  = window.sessionStorage.getItem('SC-API-TOKEN');
    HttpRequest.setHeader('SC-API-TOKEN', token);
    return {state:true, data:data};
}

//自动更新token机制
function updateToken(data) {
    if (!window.sessionStorage.getItem('SC-API-TOKEN') || window.sessionStorage.getItem('SC-API-API-DOC-UPDATE') == 1) {
        return {state:true, data:data};
    }

    let userInfo = window.sessionStorage.getItem('SC-API-CACHE-INFO');
    if (userInfo) {
        userInfo = JSON.parse(userInfo);
    }

    let nowTime = parseInt(new Date().getTime()/1000);
    userInfo.call_num = userInfo.call_num - 1;
    window.sessionStorage.setItem('SC-API-CACHE-INFO', JSON.stringify(userInfo));
    if (userInfo.call_num < 40 || nowTime + 600 > userInfo.expire_time) {
        window.sessionStorage.setItem('SC-API-API-DOC-UPDATE', 1);
        HttpRequest.request({
            url:getApiHttpUrl('login/update_token'),
            sync:false,
            type:'POST',
            dateType:'JSON',
            async:false,
            success:function (data) {
                window.sessionStorage.setItem('SC-API-API-DOC-UPDATE', 0);
                if (data.status == "YES") {
                    HttpRequest.setHeader('SC-API-TOKEN', data.data.token);
                    window.sessionStorage.setItem('SC-API-TOKEN', data.data.token);
                    window.sessionStorage.setItem('SC-API-CACHE-INFO', JSON.stringify(data.data));
                    return {state:true, data:data};
                } else {
                    logout();
                    return {state:false, data:data};
                }
            },
            error:function () {
                window.sessionStorage.setItem('SC-API-API-DOC-UPDATE', 0);
                logout();
                return {state:false, data:data};
            }
        });
    }
    return {state:true, data:data};
}

HttpRequest.beforeRequestCallback(setToken);

HttpRequest.beforeRequestCallback(updateToken);

HttpRequest.beforeRequestCallback(function(data) {
    data.platform = "pc";
    data.randstr = ScarecrowTool.createRandStr(6);
    data.timestamp = ScarecrowTool.time();
    let signObj = createSign(data);
    data.sign = signObj.sign;
    let rel = {state:true, data:data};
    return rel;
});

HttpRequest.afterRequestCallback((function(xhr) {
    try {
        if (this.dataType == "JSON") {
            var responseData = JSON.parse(xhr.responseText);
            if (responseData.code == -2 && (window.sessionStorage.getItem('SC-API-API-DOC-UPDATE') != 1 || window.sessionStorage.getItem('SC-API-TOKEN') == null)) {
                logout();
                return {state:false,data:xhr};
            }
        }

        return {state:true, data:xhr};
    } catch (e) {
        console.error(xhr.responseText);
        return {state:false, data:xhr};
    }
}).bind(HttpRequest));