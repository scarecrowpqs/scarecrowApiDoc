layui.use(['form','layer'], function() {
    var form = layui.form;
    var layer = layui.layer;
    var apiInfo = {};
    var apiId = parent.EDIT_NOW_API_ID;

    /**
     * 初始化用户信息
     */
    function getApiInfo() {
        let url = getApiHttpUrl('api/apidoc/getapidocinfo');
        HttpRequest.request({
            url:url,
            type:'POST',
            dataType:'JSON',
            async:false,
            data:{
                apiId:apiId
            },
            success:function (datas) {
                if (datas.status == "YES") {
                    apiInfo = datas.data;
                    xrApiForm();
                } else {
                    layer.msg(datas.info);
                }
            },
            error:function (e) {
                layer.msg("服务器开小差了~");
            }
        });
    }

    //渲染项目列表
    function xrApiForm() {
        let htmlStr = '<option value="">'+ apiInfo['projectName'] +'</option>';
        $("#allProjectList").append(htmlStr);

        let categoryStr = '<option value="">'+ apiInfo['categoryName'] +'</option>';
        $("#allCategoryList").append(categoryStr);

        let methodStr = '<option value="">'+ apiInfo['request_method'] +'</option>';
        $("#request_method").append(methodStr);

        $("#title").val(apiInfo['title']);
        $("#url").val(apiInfo['url']);
        $("#url").val(apiInfo['url']);
        $("#result").html(apiInfo['result']);

        let paramListData = JSON.parse(apiInfo['param']);
        var html = '';
        let tempData = {};
        for (let i in paramListData) {
            tempData = paramListData[i];
            html += '<tr>';
            html += '    <td><input type="text" autocomplete="off" placeholder="参数" class="layui-input paramName layui-disabled" value="'+ tempData['title'] +'" disabled></td>';
            html += '    <td>';
            html += '       <select class="select_class paramType layui-disabled" disabled>';
            html += '           <option value="">'+tempData['type']+'</option>';
            html += '       </select>';
            html += '    </td>';
            html += '    <td>';
            html += '       <select class="select_class paramMust layui-disabled" disabled>';
            html += '           <option value="">'+(tempData['must'] == 1 ? "是" : "否")+'</option>';
            html += '       </select>';
            html += '    </td>';
            html += '    <td><input type="text" autocomplete="off" placeholder="参数说明" class="layui-input paramDesc layui-disabled" value="'+ zy(tempData['desc']) +'" disabled></td>';
            html += '</tr>';
        }
        $(".param_tr").empty().append(html);
        form.render();
    }

    function zy(str) {
        return str.replace(/"/g, '\'');
    }

    (()=>{
        getApiInfo();
    })();
});