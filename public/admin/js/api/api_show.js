layui.use(['form', 'table'], function() {
    var form = layui.form;
    var table = layui.table


    //获取所有项目并渲染
    function xrAllProjectList() {
        let url = getApiHttpUrl('api/projectmanage/getallcanviewproject');
        HttpRequest.request({
            url:url,
            type:'POST',
            dataType:'JSON',
            data:{},
            success:function (datas) {
                if (datas.status == "YES") {
                    xrProjectListForm(datas.data);
                } else {
                    layer.msg(datas.info);
                }
            },
            error:function (e) {
                layer.msg("服务器开小差了~");
            }
        });
    }

    //渲染项目列表
    function xrProjectListForm(data) {
        let htmlStr = "";
        for (let i in data) {
            htmlStr += '<option value="'+ data[i]['id'] +'">'+ data[i]['name'] +'</option>';
        }
        $("#allProjectList").append(htmlStr);
        form.render('select');
    }

    form.on('select(allProjectList)', function (data) {
        xrAllCategoryList();
        $("#searchContent").val('');
        form.render('select');
        setTimeout(() => {
            $("#searchApiBtn").click();
        }, 10);
    });

    form.on('select(allCategoryList)', function (data) {
        form.render('select');
        setTimeout(() => {
            $("#searchApiBtn").click();
        }, 10);
    });

    //获取所有项目列表并渲染
    function xrAllCategoryList() {
        let Cache = scarecrowCache();
        let projectId = $("#allProjectList").val();
        let cacheKey = "PROJECT_CATEGORY_VIEW_" + projectId;
        let data = Cache.getKey(cacheKey, false);
        if (data === false) {
            let loadingIndex= layer.load(1);
            let url = getApiHttpUrl('api/projectmanage/getallcategory');
            HttpRequest.request({
                url:url,
                type:'POST',
                dataType:'JSON',
                async:false,
                data:{
                    projectId:$("#allProjectList").val()
                },
                success:function (datas) {
                    layer.close(loadingIndex);
                    if (datas.status == "YES") {
                        data = datas.data;
                        Cache.setKey(cacheKey, data, 14400);
                    } else {
                        layer.msg(datas.info);
                        return false;
                    }
                },
                error:function (e) {
                    layer.close(loadingIndex);
                    layer.msg("服务器开小差了~");
                    return false;
                }
            });
        } else {
        }
        xrCategoryTable(data);
    }

    //渲染接口列表
    function xrCategoryTable(data) {
        let htmlStr = '<option value="0">请选择分类</option>';
        for (let i in data) {
            htmlStr += '<option value="'+ data[i]['id'] +'">'+ data[i]['name'] +'</option>';
        }
        $("#allCategoryList").empty().append(htmlStr);
        form.render();
    }

    $("#searchApiBtn").click(function () {
        if ($("#allProjectList").val() < 1) {
            layer.msg("请选择项目");
            return false;
        }
        xrAllApiList();
    });


    //获取所有项目列表并渲染
    function xrAllApiList() {
        let url = getApiHttpUrl('api/apidoc/getapidoclist');
        let lodingIndex = layer.load(1);
        HttpRequest.request({
            url:url,
            type:'POST',
            dataType:'JSON',
            data:{
                projectId:$("#allProjectList").val(),
                categoryId:$("#allCategoryList").val(),
                apiName:$('#searchContent').val()
            },
            success:function (datas) {
                layer.close(lodingIndex);
                if (datas.status == "YES") {
                    xrApiTable(datas.data);
                } else {
                    layer.msg(datas.info);
                }
            },
            error:function (e) {
                layer.close(lodingIndex);
                layer.msg("服务器开小差了~");
            }
        });
    }

    //渲染接口列表
    function xrApiTable(data) {
        let htmlStr = "";
        $("#showAllApiTable > tbody").empty();
        for (let i in data) {
            htmlStr += '<tr>'+
                '<td>'+ data[i]['title'] +'</td>'+
                '<td>'+ data[i]['request_method'] +'</td>'+
                '<td>'+ data[i]['url'] +'</td>'+
                '<td>'+ data[i]['update_time'] +'</td>'+
                '<td><a class="layui-btn layui-btn-xs" onclick="viewApi('+ data[i]['id'] +')">详情</a>'+
                '</tr>';
        }

        $("#showAllApiTable > tbody").append(htmlStr);
    }

    //下载文件
    function downloadFile(fileName, fileData) {
        var urlObject = window.URL || window.webkitURL || window;
        var exportBlob = new Blob([fileData]);
        var a = document.createElement('a');

        a.download = fileName;
        a.href = urlObject.createObjectURL(exportBlob);;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }

    $("#exportProjectApiBtn").click(function () {
        let projectId = $("#allProjectList").val();
        if (projectId < 1) {
            layer.msg("请选择项目");
            return false;
        }
        let url = getApiHttpUrl('api/apidoc/exportapidoc');
        let lodingIndex = layer.load(1);
        HttpRequest.request({
            url:url,
            type:'POST',
            dataType:'JSON',
            data:{
                projectId:projectId,
            },
            success:function (datas,xhr) {
                layer.close(lodingIndex);
                if (datas.status == "YES") {
                    downloadFile(datas.data.fileName, datas.data.fileData);
                }else {
                    layer.msg(datas.info);
                }
            },
            error:function (e) {
                layer.close(lodingIndex);
                layer.msg("服务器开小差了~");
            }
        });
    });

    (()=>{
        $("#searchApiBtn").click();
        xrAllProjectList();
    })();
});

EDIT_NOW_PROJECT_ID = 0;
EDIT_NOW_API_ID = 0;
/**
 * 编辑接口
 * @param projectId
 */
function viewApi(apiId) {
    EDIT_NOW_API_ID = apiId;
    let index = layer.open({
        title:'查看接口',
        type: 2,
        content: getHttpUrl('page/api/view_api.html'),
        area: ['100%', '100%'],
        end:function () {
            $("#searchApiBtn").click();
        },
        success:function () {
            layer.full(index);
        }
    });
}
