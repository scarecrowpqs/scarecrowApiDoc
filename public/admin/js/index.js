$( () => {

});

window.onload = function () {
    var getMenuApiUrl = getApiHttpUrl('login/getusermenulist');

    // var menuList = [
    //     {name:'首页',icon:'&#xe6b8;',href:'',child:[
    //             // {name:'欢迎使用',icon:'&#xe6b8;',href:getHttpUrl('welcome.html')},
    //             {name:'接口文档',icon:'&#xe812;',href:getHttpUrl('index.html')},
    //         ]},
    //     {name:"系统管理", icon: '&#xe6ae;',href: '',child: [
    //             {name: '用户管理',icon: '&#xe6b8;',href: getHttpUrl('page/user/user_list.html')},
    //             {name: '权限管理',icon: '&#xe6e5;',href: ''},
    //             {name: '项目管理',icon: '&#xe839;',href: ''},
    //         ]},
    //     {
    //         name:"接口管理",icon: "&#xe6f6;", href:'', child:[
    //             {name:'编辑分类', icon: "&#xe6cb;", href: ""},
    //             {name:'编辑接口', icon: "&#xe74a;", href: ""},
    //         ]
    //     }
    // ];

    //设置左边菜单
    function getLeftMenu(datas, index=0) {
        if (datas.length <= index) {
            return '';
        }

        var data = datas[index];
        var htmlStr = "";
        if (data['child'] == undefined) {
            htmlStr = '<li><a onclick="xadmin.add_tab(\''+ data['name'] +'\',\''+ data['href'] +'\')"><i class="iconfont">'+ data['icon'] +'</i><cite>'+ data['name'] +'</cite></a></li>';
        } else {
            htmlStr = '<li><a href="javascript:;"><i class="iconfont left-nav-li" lay-tips="'+ data['name'] +'">'+data['icon']+'</i><cite>'+ data['name'] +'</cite>' +
                '<i class="iconfont nav_right">&#xe697;</i></a><ul class="sub-menu">' + getLeftMenu(data['child'], 0) + '</ul></li>';
        }
        return htmlStr + getLeftMenu(datas, index+1);
    }

    function setMenu() {
        if (getMenuApiUrl != "") {
            let menuCacheKey = "__SCARECROW_API_DOC_MENU__";
            let menuList = window.sessionStorage.getItem(menuCacheKey);
            if (menuList === null) {
                HttpRequest.request({
                    url:getMenuApiUrl,
                    type:'POST',
                    dataType:'JSON',
                    async:false,
                    data:{},
                    success:function (datas) {
                        if (datas.status == "YES") {
                            menuList = datas.data;
                            window.sessionStorage.setItem(menuCacheKey, JSON.stringify(menuList));
                        } else {
                            layer.msg(datas.info);
                            return false;
                        }
                    },
                    error:function (e) {
                        layer.msg("服务器开小差了~");
                        return false;
                    }
                });
            } else {
                menuList = JSON.parse(menuList);
            }

            var htmlStr = getLeftMenu(menuList);
            $("#nav").empty().html(htmlStr);
        } else {
            var htmlStr = getLeftMenu(menuList);
            $("#nav").empty().html(htmlStr);
        }
    }

    setMenu();
}
